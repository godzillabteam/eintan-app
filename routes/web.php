<?php

use App\Http\Controllers\ComponentController;
use Illuminate\Support\Facades\Route;
use App\Http\Livewire\TypeProposals;
use App\Http\Livewire\Roles;

use App\Http\Controllers\UsulanKegiatanController;
use App\Http\Controllers\KegiatanKelitbanganController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AbstraksiKajianController;
use App\Http\Controllers\RekomendasiHasilKelitbanganController;
use App\Http\Controllers\DataPenelitianController;
use App\Http\Controllers\RtlController;
use App\Http\Controllers\UserInputController;
use App\Http\Controllers\QuestionController;
use App\Http\Controllers\SurveyController;
use App\Http\Controllers\ActivityMasterController;
use App\Http\Controllers\ActivitySubMasterController;
use App\Http\Controllers\SectorController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth:sanctum', 'verified', 'role:admin,bidang,perangkat'])->group(function () {
    Route::get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');


    // Usulan Kegiatan

    Route::group(['prefix' => 'usulan-kegiatan', 'as' => 'usulanKegiatan.'], function () {
        Route::get('/', [UsulanKegiatanController::class, 'index']);
        Route::get('/create', [UsulanKegiatanController::class, 'create'])->name('create');
        Route::get('/data', [UsulanKegiatanController::class, 'data'])->name('data');
        Route::get('/edit/{id}', [UsulanKegiatanController::class, 'edit'])->name('edit');
        Route::get('/detail/{id}', [UsulanKegiatanController::class, 'detail'])->name('detail');
        Route::post('/store', [UsulanKegiatanController::class, 'store'])->name('store');
        Route::post('/update', [UsulanKegiatanController::class, 'update'])->name('update');
        Route::get('/delete/{id}', [UsulanKegiatanController::class, 'delete'])->name('delete');
    });

    // Kegiatan Kelitbangan

    Route::group(['prefix' => 'kegiatan-kelitbangan', 'as' => 'kegiatanKelitbangan.'], function () {
        Route::get('/', [KegiatanKelitbanganController::class, 'index'])->middleware(['role:admin,bidang']);
        Route::get('/data', [KegiatanKelitbanganController::class, 'data'])->name('data')->middleware(['role:admin,bidang']);
        Route::get('/create/{id?}', [KegiatanKelitbanganController::class, 'create'])->name('create')->middleware(['role:admin,bidang']);
        Route::get('/edit/{id}', [KegiatanKelitbanganController::class, 'edit'])->name('edit')->middleware(['role:admin,bidang']);
        Route::get('/detail/{id}', [KegiatanKelitbanganController::class, 'detail'])->name('detail')->middleware(['role:admin,bidang']);
        Route::post('/store', [KegiatanKelitbanganController::class, 'store'])->name('store')->middleware(['role:admin,bidang']);
        Route::get('/redirect/{id}', [KegiatanKelitbanganController::class, 'redirectForm'])->name('redirect')->middleware(['role:admin,bidang']);
        Route::post('/update', [KegiatanKelitbanganController::class, 'update'])->name('update')->middleware(['role:admin,bidang']);
    });

    // Data Penelitian

    Route::get('data-penelitian/abstraksi-kajian/data', [AbstraksiKajianController::class, 'data'])->middleware(['role:admin,bidang']);
    Route::resource('data-penelitian/abstraksi-kajian', AbstraksiKajianController::class)->middleware(['role:admin,bidang']);

    Route::get('data-penelitian/rekomendasi-hasil-kelitbangan/{id}/detail', [RekomendasiHasilKelitbanganController::class,'show'])->middleware(['role:admin,bidang,perangkat']);
    Route::get('data-penelitian/rekomendasi-hasil-kelitbangan/data', [RekomendasiHasilKelitbanganController::class,'data'])->middleware(['role:admin,bidang,perangkat']);
    Route::resource('data-penelitian/rekomendasi-hasil-kelitbangan', RekomendasiHasilKelitbanganController::class)->middleware(['role:admin,bidang,perangkat']);
   
    Route::group(['prefix' => 'data-penelitian', 'as' => 'dataPenelitian.'], function () {
        Route::get('/', [DataPenelitianController::class, 'index'])->middleware(['role:admin,bidang']);
        Route::get('/create/{type}/{id}', [DataPenelitianController::class, 'create'])->name('create')->middleware(['role:admin,bidang']);
        Route::post('/store', [DataPenelitianController::class, 'store'])->name('store')->middleware(['role:admin,bidang']);
        Route::post('/store_hasil', [DataPenelitianController::class, 'store_hasil'])->name('store_hasil')->middleware(['role:admin,bidang']);
    });

    // rtl

    Route::get('rtl/data', [RtlController::class, 'data'])->middleware(['role:admin,perangkat']);
    Route::resource('rtl', RtlController::class)->middleware(['role:admin,perangkat']);

    Route::get('master/user/data', [UserController::class, 'data'])->middleware(['role:admin']);
    Route::get('master/user/{id}/delete', [UserController::class, 'destroy'])->middleware(['role:admin']);
    Route::resource('master/user', UserController::class)->middleware(['role:admin']);


    // Master survey

    Route::get('master/question/data', [QuestionController::class, 'data'])->middleware(['role:admin']);
    Route::get('master/question/{id}/delete', [QuestionController::class, 'destroy'])->middleware(['role:admin']);
    Route::resource('master/question', QuestionController::class)->middleware(['role:admin']);


    Route::get('master/survey/data', [SurveyController::class, 'data'])->middleware(['role:admin']);
    Route::get('master/survey/{id}/delete', [SurveyController::class, 'destroy'])->middleware(['role:admin']);
    Route::resource('master/survey', SurveyController::class)->middleware(['role:admin']);

    Route::get('user-input/data/{id?}', [UserInputController::class, 'data'])->middleware(['role:perangkat,admin']);
    Route::resource('user-input', UserInputController::class)->middleware(['role:perangkat,admin']);
    Route::get('user-input/detail/{id?}', [UserInputController::class, 'index'])->middleware(['role:perangkat,admin']);


    Route::get('master/activity/data', [ActivityMasterController::class,'data'])->middleware(['role:admin']);
    Route::get('master/activity/{id}/delete', [ActivityMasterController::class,'destroy'])->middleware(['role:admin']);
    Route::resource('master/activity', ActivityMasterController::class)->middleware(['role:admin']);

    Route::get('master/activity-sub/data', [ActivitySubMasterController::class, 'data'])->middleware(['role:admin']);
    Route::get('master/activity-sub/{id}/delete', [ActivitySubMasterController::class, 'destroy'])->middleware(['role:admin']);
    Route::resource('master/activity-sub', ActivitySubMasterController::class)->middleware(['role:admin']);

    Route::get('master/sector/data', [SectorController::class, 'data'])->middleware(['role:admin']);
    Route::get('master/sector/{id}/delete', [SectorController::class, 'destroy'])->middleware(['role:admin']);
    Route::resource('master/sector', SectorController::class)->middleware(['role:admin']);


    Route::get('json/{type}/{id?}', [UsulanKegiatanController::class, 'jsonActivity']);



    // Route::get('/components', function () {
    //     return view('layouts.components');
    // })->name('components');

    // Master
    // Route::get('master/jenis-usulan-kelitbangan', TypeProposals::class);
    // Route::get('master/role', Roles::class);

});
