<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
                'name' => 'Administrator',
                'email' => 'admin@eintan.com',
                'password' => bcrypt('password'),
                'role_id' => 1
        ]);

        User::create([
                'name' => 'Admin Bidang',
                'email' => 'adminbidang@eintan.com',
                'password' => bcrypt('password'),
                'role_id' => 2
        ]);

        User::create([
                'name' => 'Perangkat Daerah',
                'email' => 'perangkatdaerah@eintan.com',
                'password' => bcrypt('password'),
                'role_id' => 3
        ]);
    }
}
