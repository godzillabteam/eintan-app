<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserInputQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_input_questions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_input_id');
            $table->unsignedBigInteger('question_id');
            $table->integer('score');
            $table->foreign('user_input_id')->references('id')->on('user_inputs')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('questions')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_input_questions');
    }
}
