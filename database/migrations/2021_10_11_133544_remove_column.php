<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RemoveColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('activity_masters', function (Blueprint $table) {
            $table->id();
            $table->text('name');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('activity_sub_masters', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('activity_master_id');
            $table->text('name');
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('activity_master_id')->references('id')->on('activity_masters')->onDelete('cascade');
        });

        //
        Schema::table('activity_proposals', function (Blueprint $table) {
            $table->softDeletes();
            $table->dropColumn('is_deleted');
        });

        Schema::table('activities', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('activity_subs', function (Blueprint $table) {
            $table->dropColumn('is_deleted');
        });

        Schema::table('rnd_activities', function (Blueprint $table) {
            $table->softDeletes();
            $table->dropColumn('is_deleted');
        });



    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('activity_proposals', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });

        Schema::table('rnd_activities', function (Blueprint $table) {
            $table->dropSoftDeletes();
        });
    }
}
