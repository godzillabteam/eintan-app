<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudyAbstract extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('study_abstracts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('activity_proposal_id');
            $table->string('year',10);
            $table->string('executor');
            $table->string('abstraction');
            $table->string('link');
            $table->timestamps();
            $table->boolean('is_deleted')->default(false);
            $table->foreign('activity_proposal_id')->references('id')->on('activity_proposals')->onDelete('cascade');
            $table->softDeletes();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('study_abstracts');
    }
}
