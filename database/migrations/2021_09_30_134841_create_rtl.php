<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rtls', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('rnd_result_id');
            $table->text('recommendation_implementation');
            $table->text('reason')->nullable();
            $table->text('activity')->nullable();
            $table->text('year')->nullable();
            $table->text('photo')->nullable();
            $table->foreign('rnd_result_id')->references('id')->on('rnd_results')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rtls');
    }
}
