<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRndActivityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rnd_activities', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('activity_proposal_id');
            $table->string('business_field');
            $table->string('program');
            $table->string('self_management');
            $table->string('provider');
            $table->string('activity_year');
            $table->string('status')->default('draft');
            $table->boolean('is_deleted')->default(false);
            $table->foreign('activity_proposal_id')->references('id')->on('activity_proposals')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rnd_activity');
    }
}
