<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnRndActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('rnd_activities', function (Blueprint $table) {
            $table->unsignedBigInteger('activity_master_id');
            $table->foreign('activity_master_id')->references('id')->on('activity_masters')->onDelete('cascade');

            $table->unsignedBigInteger('activity_sub_master_id');
            $table->foreign('activity_sub_master_id')->references('id')->on('activity_sub_masters')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('rnd_activities', function (Blueprint $table) {
            $table->dropColumn('activity_master_id');
            $table->dropColumn('activity_sub_master_id');

        });
    }
}
