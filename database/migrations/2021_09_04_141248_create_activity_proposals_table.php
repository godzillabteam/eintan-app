<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivityProposalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity_proposals', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('type_proposal_id');
            $table->string('type_proposal_title');
            $table->string('problem');
            $table->string('aim');
            $table->string('output');
            $table->string('outcome');
            $table->integer('proposed_year');
            $table->integer('estimated_budget');
            $table->timestamps();
            $table->index('user_id');
            $table->index('type_proposal_id');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('type_proposal_id')->references('id')->on('type_proposals')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $table->dropForeign('lists_user_id_foreign');
        $table->dropIndex('lists_user_id_index');
        $table->dropColumn('user_id');

        $table->dropForeign('lists_type_proposal_id_foreign');
        $table->dropIndex('lists_type_proposal_id_index');
        $table->dropColumn('type_proposal_id');

        Schema::dropIfExists('activity_proposals');
    }
}
