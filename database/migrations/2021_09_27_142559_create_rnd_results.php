<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRndResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rnd_results', function (Blueprint $table) {
            $table->id();

            $table->unsignedBigInteger('activity_proposal_id');
            $table->unsignedBigInteger('activity_sub_id');
            $table->text('recommendation_description');
            $table->unsignedBigInteger('user_id');
            $table->string('general_strategy');
            $table->timestamps();
            $table->softDeletes();
            $table->foreign('activity_proposal_id')->references('id')->on('activity_proposals')->onDelete('cascade');
            $table->foreign('activity_sub_id')->references('id')->on('activity_subs')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rnd_results');
    }
}
