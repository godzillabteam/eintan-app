// Alert Delete
let alertDelete = () => {
  swal({
    title: "Apakah Anda sudah yakin?",
    text: "Setelah dihapus, Anda tidak dapat memulihkan file ini!",
    icon: "warning",
    buttons: ["Batal", "Hapus"],
    dangerMode: true,
  }).then((willDelete) => {
    if (willDelete) {
      swal("File anda telah berhasil dihapus!", {
        icon: "success",
        button: {
          text: "Oke",
        },
      });
    }
  });
};
