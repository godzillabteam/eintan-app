// Modal
let modal = () => {
  var backdropModal = "modal-backdrop",
    createElement_Div,
    setTimerClose;

  var getElement_Modal = document.querySelectorAll("[data-modal]");
  var getElement_Dismiss = document.querySelectorAll("[data-modal-dismiss]");

  // Backdrop Modal
  createElement_Div = document.createElement("div");
  createElement_Div.setAttribute("id", "modalBackdrop");
  createElement_Div.classList.add(backdropModal);

  document.body.appendChild(createElement_Div);

  if (createElement_Div !== null) {
    var getElement_Backdrop;

    getElement_Backdrop = document.getElementById(
      createElement_Div.getAttribute("id")
    );

    if (getElement_Backdrop !== null) {
      getElement_Backdrop.addEventListener("click", (e) => {
        var setModal_Active = document.querySelectorAll(".modal.active");

        if (setModal_Active !== null) {
          setModal_Active.forEach((target) => {
            if (target.classList.contains("active") === true) {
              target.classList.remove("active");

              if (getElement_Backdrop.classList.contains("active") === true) {
                getElement_Backdrop.classList.remove("active");
              }

              setTimerClose = setTimeout(() => {
                document.body.classList.remove("modal-open");
              }, 200);
            }
          });
        }

        e.preventDefault();
      });
    }
  }

  // Close Modal
  if (getElement_Dismiss !== null) {
    getElement_Dismiss.forEach((target) => {
      target.addEventListener("click", (e) => {
        var setElement_Modal = document.querySelector(
          target.dataset.modalDismiss
        );

        if (setElement_Modal !== null) {
          if (setElement_Modal.classList.contains("active") === true) {
            setElement_Modal.classList.remove("active");

            if (getElement_Backdrop.classList.contains("active") === true) {
              getElement_Backdrop.classList.remove("active");
            }

            setTimerClose = setTimeout(() => {
              document.body.classList.remove("modal-open");
            }, 200);
          }
        }

        e.preventDefault();
      });
    });
  }

  // Open Modal
  if (getElement_Modal !== null) {
    getElement_Modal.forEach((target) => {
      target.addEventListener("click", (e) => {
        var setElement_Modal = document.querySelector(target.dataset.modal);

        if (setElement_Modal !== null) {
          if (setElement_Modal.classList.contains("active") !== true) {
            setElement_Modal.classList.add("active");

            document.body.classList.add("modal-open");

            if (getElement_Backdrop.classList.contains("active") !== true) {
              getElement_Backdrop.classList.add("active");
            }

            clearTimeout(setTimerClose);
          }
        }

        e.preventDefault();
      });
    });
  }
};

// Format Rupiah
let formatRupiah = (getData) => {
  var setElement = document.getElementsByClassName(getData);

  if (setElement != null) {
    for (let i = 0; i < setElement.length; i++) {
      var setAttr_Number = setElement[i].getAttribute("data-number");

      if (setAttr_Number != null) {
        var reverseData = setAttr_Number
          .toString()
          .split("")
          .reverse()
          .join("");
        var setFormat = reverseData.match(/\d{1,3}/g);

        setFormat = setFormat.join(".").split("").reverse().join("");

        setElement[i].innerHTML = "Rp." + setFormat + ",00";
      }
    }
  }
};

// Toggle Password (Show Hide)
let togglePassword = (getData) => {
  let setElement = document.getElementsByClassName(getData);

  if (setElement != null) {
    for (let i = 0; i < setElement.length; i++) {
      setElement[i].addEventListener("click", (e) => {
        let setAttr_Target = setElement[i].getAttribute("data-toggle");
        let setAttr_Size = setElement[i].getAttribute("data-size");

        if (setAttr_Target != null && setAttr_Size != null) {
          let getElement_Target = document.querySelector(setAttr_Target);

          if (getElement_Target != null) {
            if (setAttr_Size == "lg") {
              if (getElement_Target.type === "password") {
                setElement[i].innerHTML = `
                <svg class="fill-current h-6 w-6 duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M12 14.4C13.3255 14.4 14.4 13.3255 14.4 12C14.4 10.6745 13.3255 9.59998 12 9.59998C10.6745 9.59998 9.60001 10.6745 9.60001 12C9.60001 13.3255 10.6745 14.4 12 14.4Z" />
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0.549316 12C2.07842 7.13147 6.62678 3.59998 12 3.59998C17.3731 3.59998 21.9215 7.13143 23.4506 11.9999C21.9215 16.8685 17.3731 20.4 11.9999 20.4C6.62679 20.4 2.07845 16.8685 0.549316 12ZM16.8 12C16.8 14.6509 14.651 16.8 12 16.8C9.34904 16.8 7.20001 14.6509 7.20001 12C7.20001 9.34901 9.34904 7.19998 12 7.19998C14.651 7.19998 16.8 9.34901 16.8 12Z" />
                </svg>
              `;

                getElement_Target.type = "text";
              } else {
                setElement[i].innerHTML = `
                <svg class="fill-current h-6 w-6 duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M4.44852 2.7515C3.9799 2.28287 3.2201 2.28287 2.75147 2.7515C2.28284 3.22013 2.28284 3.97992 2.75147 4.44855L19.5515 21.2486C20.0201 21.7172 20.7799 21.7172 21.2485 21.2486C21.7172 20.7799 21.7172 20.0201 21.2485 19.5515L19.4807 17.7837C21.3258 16.3109 22.7268 14.3053 23.4509 12C21.9217 7.13148 17.3734 3.60002 12.0003 3.60002C10.052 3.60002 8.21213 4.06433 6.58534 4.88831L4.44852 2.7515ZM9.56175 7.86472L11.3784 9.68138C11.5768 9.62832 11.7852 9.60002 12.0003 9.60002C13.3258 9.60002 14.4003 10.6745 14.4003 12C14.4003 12.2151 14.372 12.4236 14.3189 12.6219L16.1356 14.4386C16.5579 13.7239 16.8003 12.8903 16.8003 12C16.8003 9.34906 14.6513 7.20002 12.0003 7.20002C11.1101 7.20002 10.2764 7.44238 9.56175 7.86472Z" />
                  <path d="M14.9449 20.0361L11.6996 16.7908C9.28815 16.6417 7.35863 14.7122 7.20957 12.3007L2.8019 7.89308C1.80075 9.0867 1.02764 10.478 0.549606 12.0001C2.07874 16.8686 6.62708 20.4 12.0002 20.4C13.0162 20.4 14.0027 20.2738 14.9449 20.0361Z" />
                </svg>
              `;

                getElement_Target.type = "password";
              }
            } else if (setAttr_Size == "md") {
              if (getElement_Target.type === "password") {
                setElement[i].innerHTML = `
                <svg class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M12 14.4C13.3255 14.4 14.4 13.3255 14.4 12C14.4 10.6745 13.3255 9.59998 12 9.59998C10.6745 9.59998 9.60001 10.6745 9.60001 12C9.60001 13.3255 10.6745 14.4 12 14.4Z" />
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0.549316 12C2.07842 7.13147 6.62678 3.59998 12 3.59998C17.3731 3.59998 21.9215 7.13143 23.4506 11.9999C21.9215 16.8685 17.3731 20.4 11.9999 20.4C6.62679 20.4 2.07845 16.8685 0.549316 12ZM16.8 12C16.8 14.6509 14.651 16.8 12 16.8C9.34904 16.8 7.20001 14.6509 7.20001 12C7.20001 9.34901 9.34904 7.19998 12 7.19998C14.651 7.19998 16.8 9.34901 16.8 12Z" />
                </svg>
              `;

                getElement_Target.type = "text";
              } else {
                setElement[i].innerHTML = `
                <svg class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M4.44852 2.7515C3.9799 2.28287 3.2201 2.28287 2.75147 2.7515C2.28284 3.22013 2.28284 3.97992 2.75147 4.44855L19.5515 21.2486C20.0201 21.7172 20.7799 21.7172 21.2485 21.2486C21.7172 20.7799 21.7172 20.0201 21.2485 19.5515L19.4807 17.7837C21.3258 16.3109 22.7268 14.3053 23.4509 12C21.9217 7.13148 17.3734 3.60002 12.0003 3.60002C10.052 3.60002 8.21213 4.06433 6.58534 4.88831L4.44852 2.7515ZM9.56175 7.86472L11.3784 9.68138C11.5768 9.62832 11.7852 9.60002 12.0003 9.60002C13.3258 9.60002 14.4003 10.6745 14.4003 12C14.4003 12.2151 14.372 12.4236 14.3189 12.6219L16.1356 14.4386C16.5579 13.7239 16.8003 12.8903 16.8003 12C16.8003 9.34906 14.6513 7.20002 12.0003 7.20002C11.1101 7.20002 10.2764 7.44238 9.56175 7.86472Z" />
                  <path d="M14.9449 20.0361L11.6996 16.7908C9.28815 16.6417 7.35863 14.7122 7.20957 12.3007L2.8019 7.89308C1.80075 9.0867 1.02764 10.478 0.549606 12.0001C2.07874 16.8686 6.62708 20.4 12.0002 20.4C13.0162 20.4 14.0027 20.2738 14.9449 20.0361Z" />
                </svg>
              `;

                getElement_Target.type = "password";
              }
            } else if (setAttr_Size == "sm") {
              if (getElement_Target.type === "password") {
                setElement[i].innerHTML = `
                <svg class="fill-current h-5 w-5 duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M12 14.4C13.3255 14.4 14.4 13.3255 14.4 12C14.4 10.6745 13.3255 9.59998 12 9.59998C10.6745 9.59998 9.60001 10.6745 9.60001 12C9.60001 13.3255 10.6745 14.4 12 14.4Z" />
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M0.549316 12C2.07842 7.13147 6.62678 3.59998 12 3.59998C17.3731 3.59998 21.9215 7.13143 23.4506 11.9999C21.9215 16.8685 17.3731 20.4 11.9999 20.4C6.62679 20.4 2.07845 16.8685 0.549316 12ZM16.8 12C16.8 14.6509 14.651 16.8 12 16.8C9.34904 16.8 7.20001 14.6509 7.20001 12C7.20001 9.34901 9.34904 7.19998 12 7.19998C14.651 7.19998 16.8 9.34901 16.8 12Z" />
                </svg>
              `;

                getElement_Target.type = "text";
              } else {
                setElement[i].innerHTML = `
                <svg class="fill-current h-5 w-5 duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M4.44852 2.7515C3.9799 2.28287 3.2201 2.28287 2.75147 2.7515C2.28284 3.22013 2.28284 3.97992 2.75147 4.44855L19.5515 21.2486C20.0201 21.7172 20.7799 21.7172 21.2485 21.2486C21.7172 20.7799 21.7172 20.0201 21.2485 19.5515L19.4807 17.7837C21.3258 16.3109 22.7268 14.3053 23.4509 12C21.9217 7.13148 17.3734 3.60002 12.0003 3.60002C10.052 3.60002 8.21213 4.06433 6.58534 4.88831L4.44852 2.7515ZM9.56175 7.86472L11.3784 9.68138C11.5768 9.62832 11.7852 9.60002 12.0003 9.60002C13.3258 9.60002 14.4003 10.6745 14.4003 12C14.4003 12.2151 14.372 12.4236 14.3189 12.6219L16.1356 14.4386C16.5579 13.7239 16.8003 12.8903 16.8003 12C16.8003 9.34906 14.6513 7.20002 12.0003 7.20002C11.1101 7.20002 10.2764 7.44238 9.56175 7.86472Z" />
                  <path d="M14.9449 20.0361L11.6996 16.7908C9.28815 16.6417 7.35863 14.7122 7.20957 12.3007L2.8019 7.89308C1.80075 9.0867 1.02764 10.478 0.549606 12.0001C2.07874 16.8686 6.62708 20.4 12.0002 20.4C13.0162 20.4 14.0027 20.2738 14.9449 20.0361Z" />
                </svg>
              `;

                getElement_Target.type = "password";
              }
            }
          }
        }

        e.preventDefault();
      });
    }
  }
};

// Sidebar Toggler
let sidebarToggler = (getData) => {
  let setElement = document.getElementById(getData);

  if (setElement != null) {
    setElement.addEventListener("click", (e) => {
      let setAttr_Target = setElement.getAttribute("data-target");
      let setAttr_Icons = setElement.getAttribute("data-icons");

      if (setAttr_Target != null && setAttr_Icons != null) {
        let getElement_Target = document.querySelector(setAttr_Target);
        let getElement_Icons = document.querySelector(setAttr_Icons);

        if (getElement_Target != null) {
          if (getElement_Target.classList.contains("-translate-x-full")) {
            getElement_Target.classList.remove("-translate-x-full");
            getElement_Target.classList.add("translate-x-0");
          } else {
            getElement_Target.classList.remove("translate-x-0");
            getElement_Target.classList.add("-translate-x-full");
          }
        }

        if (getElement_Icons != null) {
          if (getElement_Icons.classList.contains("active")) {
            getElement_Icons.classList.remove("active");
          } else {
            getElement_Icons.classList.add("active");
          }
        }
      }

      e.preventDefault();
    });
  }
};

// Sidebar Nav Dropdown
let sidebarDropdown = (getData) => {
  var setElement = document.getElementsByClassName(getData);

  if (setElement !== null) {
    for (let i = 0; i < setElement.length; i++) {
      setElement[i].addEventListener("click", (e) => {
        var setAttr_Toggle = setElement[i].getAttribute("data-toggle");

        if (setAttr_Toggle !== null) {
          var getElement_Toggle =
            document.getElementsByClassName(setAttr_Toggle);

          setElement[i].classList.toggle("active");

          if (getElement_Toggle !== null) {
            var getAttr_Collapse =
              getElement_Toggle[i].getAttribute("data-collapse");

            getElement_Toggle[i].classList.toggle(getAttr_Collapse);
          }
        }

        e.preventDefault();
      });
    }
  }
};
