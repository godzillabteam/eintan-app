const defaultTheme = require("tailwindcss/defaultTheme");

module.exports = {
  mode: "jit",
  purge: [
    "./vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php",
    "./vendor/laravel/jetstream/**/*.blade.php",
    "./storage/framework/views/*.php",
    "./resources/views/**/*.blade.php",
  ],

  theme: {
    extend: {
      colors: {
        transparent: "transparent",
        current: "currentColor",

        black: {
          DEFAULT: "#383838",
        },
        gray: {
          50: "#F8F8FF",
          400: "#B5BBD3",
        },
        green: {
          DEFAULT: "#3E9820",
        },
        lightgreen: {
          1: "#E0FDD5",
          2: "#53A538",
        },

        primary: "#3E9820",
        secondary: "#B5BBD3",
        success: "#2EAF4C",
        danger: "#FF5A50",
        warning: "#CF990E",
        info: "#6BA0D3",
        light: "#EFEFFF",
      },
      fontFamily: {
        sans: ["Nunito", ...defaultTheme.fontFamily.sans],
        poppins: ["Poppins", ...defaultTheme.fontFamily.sans],
        inter: ["Inter", ...defaultTheme.fontFamily.sans],
      },
      fontSize: {
        xxs: ["12px", "1.5"],
        xs: ["14px", "1.5"],
        sm: ["15px", "1.5"],
        base: ["16px", "1.5"],
      },
      boxShadow: {
        DEFAULT: "0px 6px 16px 0px rgb(56 56 56 / 10%)",
      },
      padding: {},
      zIndex: {
        normal: "1080",
        base: "1580",
        first1: "2080",
        first2: "2580",
      },
    },
  },

  plugins: [require("@tailwindcss/forms"), require("@tailwindcss/typography")],
};
