<x-guest-layout>

  <div class="grid grid-cols-12 gap-0 select-none">
    <div class="col-span-12 xl:col-span-7">
      <div class="flex items-center justify-center h-screen">
        <div class="w-[82%] sm:w-96">
          <!-- Brand Logo -->
          <div class="flex justify-center relative mb-14">
            <img class="w-auto h-20" src="{{ url('./img/e-intan.png') }}" alt="LOGO">
          </div>

          <x-jet-validation-errors class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 mb-6 duration-300 ease-in-out" />

          @if (session('status'))
          <div class="bg-green-100 border border-green-300 rounded-lg font-inter text-green-400 text-sm px-5 py-4 mb-8">
            {{ session('status') }}
          </div>
          @endif

          <!-- Form Login -->
          <form method="POST" action="{{ route('login') }}">
            @csrf

            <div class="text-gray-300 focus-within:text-primary relative mb-4">
              <x-ui.form.input id="emailLogin" type="email" name="email" aria-placeholder="Username atau email" placeholder="Username atau email" size="lg" parent="icons-r-lg" required autofocus />

              <span class="bg-transparent flex items-center justify-center absolute top-0 right-0 bottom-0 w-16">
                <svg class="fill-current h-6 w-6 duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path d="M2.404 7.06025L11.9999 11.8582L21.596 7.06019C21.5236 5.79977 20.4786 4.79999 19.2 4.79999H4.8C3.52142 4.79999 2.47636 5.7998 2.404 7.06025Z" />
                  <path d="M21.6 9.74147L11.9999 14.5415L2.4 9.74153V16.8C2.4 18.1255 3.47452 19.2 4.8 19.2H19.2C20.5255 19.2 21.6 18.1255 21.6 16.8V9.74147Z" />
                </svg>
              </span>
            </div>
            <div class="text-gray-300 focus-within:text-primary relative mb-12">
              <x-ui.form.input id="passwordLogin" type="password" name="password" aria-placeholder="Password" placeholder="Password" size="lg" parent="icons-r-lg" required autocomplete="current-password" />

              <span class="toggle-password bg-transparent flex items-center justify-center absolute top-0 right-0 bottom-0 w-16" data-toggle="#passwordLogin" data-size="lg">
                <svg class="fill-current h-6 w-6 duration-300 ease-in-out" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" clip-rule="evenodd" d="M4.44852 2.7515C3.9799 2.28287 3.2201 2.28287 2.75147 2.7515C2.28284 3.22013 2.28284 3.97992 2.75147 4.44855L19.5515 21.2486C20.0201 21.7172 20.7799 21.7172 21.2485 21.2486C21.7172 20.7799 21.7172 20.0201 21.2485 19.5515L19.4807 17.7837C21.3258 16.3109 22.7268 14.3053 23.4509 12C21.9217 7.13148 17.3734 3.60002 12.0003 3.60002C10.052 3.60002 8.21213 4.06433 6.58534 4.88831L4.44852 2.7515ZM9.56175 7.86472L11.3784 9.68138C11.5768 9.62832 11.7852 9.60002 12.0003 9.60002C13.3258 9.60002 14.4003 10.6745 14.4003 12C14.4003 12.2151 14.372 12.4236 14.3189 12.6219L16.1356 14.4386C16.5579 13.7239 16.8003 12.8903 16.8003 12C16.8003 9.34906 14.6513 7.20002 12.0003 7.20002C11.1101 7.20002 10.2764 7.44238 9.56175 7.86472Z" />
                  <path d="M14.9449 20.0361L11.6996 16.7908C9.28815 16.6417 7.35863 14.7122 7.20957 12.3007L2.8019 7.89308C1.80075 9.0867 1.02764 10.478 0.549606 12.0001C2.07874 16.8686 6.62708 20.4 12.0002 20.4C13.0162 20.4 14.0027 20.2738 14.9449 20.0361Z" />
                </svg>
              </span>
            </div>

            <div class="flex items-center justify-center relative">
              <x-jet-button class="bg-primary hover:bg-primary focus:bg-primary active:bg-primary focus:ring-0 rounded-md border-0 justify-center font-inter text-white text-base font-normal uppercase tracking-tight outline-none focus:outline-none py-4 w-full">
                {{ __('Masuk') }}
              </x-jet-button>
            </div>
          </form>

          <div class="flex justify-between relative mt-6 mb-0">
            @if (Route::has('password.request'))
            <a class="font-inter text-dark text-base font-semibold tracking-tight no-underline mb-0" href="{{ route('password.request') }}">Lupa Password?</a>
            @endif
            <a class="font-inter text-primary text-base font-semibold tracking-tight no-underline mb-0" href="">Hubungi
              Admin</a>
          </div>
        </div>
      </div>
    </div>
    <div class="hidden xl:flex xl:col-span-5">
      <div class="h-full">
        <img class="object-cover h-screen w-full" src="https://media.suara.com/pictures/970x544/2018/09/27/43335-gedung-pencakar-langit.jpg" alt="Image Banner">
      </div>
    </div>
  </div>

  <!-- Java Script -->
  <script src="{{ url('js/configs.js') }}"></script>
  <script>
    window.addEventListener('DOMContentLoaded', () => {
      const showPassword = togglePassword('toggle-password'); // Input (Show Password)
    }, false);
  </script>

  {{-- <x-jet-authentication-card>
        <x-slot name="logo">
            <x-jet-authentication-card-logo />
        </x-slot>

        
    </x-jet-authentication-card> --}}
</x-guest-layout>