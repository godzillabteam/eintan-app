<x-app-layout>
  <x-slot name="header">
    <div class="mb-6 sm:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Selamat Datang, '.Auth::user()->name) }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('E-intan') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <!-- Widget (information) -->
  <div class="grid grid-cols-12 gap-0 select-none mb-12">
    <div class="col-span-12 sm:col-span-4">
      <div class="hover:bg-lightgreen-1 border-l-[6px] border-l-transparent hover:border-l-primary border-r-2 border-gray-200 relative px-[30px] py-[25px] duration-500 duration ease-in-out">
        <div class="flex items-end mb-4">
          <h5 class="text-black font-poppins text-5xl font-normal tracking-tight mr-1 antialiased">100</h5>
          <div class="flex items-center text-green mb-1">
            <x-heroicon-o-arrow-up class="fill-current h-5 w-5" />
            <span class="text-current font-inter text-lg font-medium tracking-tight antialiased">16%</span>
          </div>
        </div>
        <p class="text-gray-400 font-inter text-xs font-medium tracking-tight capitalize antialiased">Transaksi Hari ini</p>
      </div>
    </div>
    <div class="col-span-12 sm:col-span-4">
      <div class="hover:bg-lightgreen-1 border-l-[6px] border-l-transparent hover:border-l-primary border-r-2 border-gray-200 relative px-[30px] py-[25px] duration-500 duration ease-in-out">
        <div class="flex items-end mb-4">
          <h5 class="text-black font-poppins text-5xl font-normal tracking-tight mr-1 antialiased">250</h5>
          <div class="flex items-center text-green mb-1">
            <x-heroicon-o-arrow-up class="fill-current h-5 w-5" />
            <span class="text-current font-inter text-lg font-medium tracking-tight antialiased">16%</span>
          </div>
        </div>
        <p class="text-gray-400 font-inter text-xs font-medium tracking-tight capitalize antialiased">Transaksi Bulan ini</p>
      </div>
    </div>
    <div class="col-span-12 sm:col-span-4">
      <div class="hover:bg-lightgreen-1 border-l-[6px] border-l-transparent hover:border-l-primary border-r-2 border-gray-200 relative px-[30px] py-[25px] duration-500 duration ease-in-out">
        <div class="flex items-end mb-4">
          <h5 class="text-black font-poppins text-5xl font-normal tracking-tight mr-1 antialiased">1000</h5>
          <div class="flex items-center text-green mb-1">
            <x-heroicon-o-arrow-up class="fill-current h-5 w-5" />
            <span class="text-current font-inter text-lg font-medium tracking-tight antialiased">16%</span>
          </div>
        </div>
        <p class="text-gray-400 font-inter text-xs font-medium tracking-tight capitalize antialiased">Total Semua Transaksi</p>
      </div>
    </div>
  </div>

  <!-- Tabel Information -->
  <div class="grid grid-cols-12 gap-5 select-none">
    <div class="col-span-12 lg:col-span-6">
      <div class="flex flex-col relative">
        <div class="mb-5">
          <h3 class="text-black font-inter text-lg font-semibold tracking-tight antialiased">Order Terakhir</h3>
        </div>
        <div class="whitespace-nowrap overflow-x-auto w-full">
          <table id="tableOrderTerakhir" class="bg-white table border-collapse text-black mb-2 w-full">
            <thead class="bg-lightgreen-1 border-collapse border-t border-b border-gray-200 text-green font-inter text-xs font-semibold capitalize tracking-tight">
              <tr>
                <td scope="col" class="p-4 pt-5">
                  <x-ui.form.checkbox id="checkAllLastOrder" name="checkbox-table" value="0" />
                </td>
                <td scope="col" class="p-4 pl-0" width="12%">Kode order</td>
                <td scope="col" class="p-4">Pelanggan</td>
                <td scope="col" class="p-4" width="18%">Kurir</td>
                <td scope="col" class="p-4">Status</td>
                <td scope="col" class="p-4">Total Transaksi</td>
              </tr>
            </thead>
            <tbody class="font-inter text-xs font-medium tracking-tight">
              <tr class="border-b border-gray-200">
                <td class="p-4 pt-5">
                  <x-ui.form.checkbox id="checkLastOrder1" name="checkbox-table" value="1" />
                </td>
                <td class="p-4 pl-0">KLS221413123</td>
                <td class="p-4">Lia Khusniawati</td>
                <td class="p-4">Bagus Prakoso</td>
                <td class="p-4">
                  <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="Batal" type="button" />
                </td>
                <td class="p-4 pr-1">
                  <div class="flex items-center justify-between">
                    <span class="format-rupiah" data-number="450000"></span>

                    <x-ui.form.dropdown align="drop-left">
                      <x-slot name="trigger">
                        <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                          <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                        </button>
                      </x-slot>

                      <x-slot name="content">
                        <div class="py-2.5 px-5 pr-8">
                          <x-ui.form.dropdown-link href="{{ url('master/user/${data}') }}/edit">
                            <x-heroicon-s-information-circle class="fill-current mr-2 h-4 w-4" />
                            {{ __('Detail') }}
                          </x-ui.form.dropdown-link>
                        </div>
                      </x-slot>
                    </x-ui.form.dropdown>
                  </div>
                </td>
              </tr>
              <tr>
                <td class="p-4 pt-5">
                  <x-ui.form.checkbox id="checkLastOrder2" name="checkbox-table" value="2" />
                </td>
                <td class="p-4 pl-0">KLS221413123</td>
                <td class="p-4">Lia Khusniawati</td>
                <td class="p-4">Bagus Prakoso</td>
                <td class="p-4">
                  <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="Selesai" type="button" />
                </td>
                <td class="p-4 pr-1">
                  <div class="flex items-center justify-between">
                    <span class="format-rupiah" data-number="450000"></span>

                    <x-ui.form.dropdown align="drop-left">
                      <x-slot name="trigger">
                        <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                          <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                        </button>
                      </x-slot>

                      <x-slot name="content">
                        <div class="py-2.5 px-5 pr-8">
                          <x-ui.form.dropdown-link href="{{ url('master/user/${data}') }}/edit">
                            <x-heroicon-s-information-circle class="fill-current mr-2 h-4 w-4" />
                            {{ __('Detail') }}
                          </x-ui.form.dropdown-link>
                        </div>
                      </x-slot>
                    </x-ui.form.dropdown>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
    <div class="col-span-12 lg:col-span-6 row-start-1 lg:row-start-auto mb-3 lg:mb-0">
      <div class="flex flex-col relative">
        <div class="mb-5">
          <h3 class="text-black font-inter text-lg font-semibold tracking-tight antialiased">Top 10 Pelanggan</h3>
        </div>
        <div class="whitespace-nowrap overflow-x-auto w-full">
          <table id="tableTopPelanggan" class="bg-white table border-collapse text-black mb-2 w-full">
            <thead class="bg-lightgreen-1 border-collapse border-t border-b border-gray-200 text-green font-inter text-xs font-semibold capitalize tracking-tight">
              <tr>
                <td scope="col" class="p-4 pt-5">
                  <x-ui.form.checkbox id="checkAllTopPelanggan" name="checkbox-table" value="0" />
                </td>
                <td scope="col" class="p-4 pl-0">Nama Pelanggan</td>
                <td scope="col" class="p-4" width="26%">Jumlah Order</td>
                <td scope="col" class="p-4">Total Transaksi</td>
              </tr>
            </thead>
            <tbody class="font-inter text-xs font-medium tracking-tight">
              <tr class="border-b border-gray-200">
                <td class="p-4 pr-0">
                  <x-ui.form.checkbox id="checkAllTopPelanggan1" name="checkbox-table" value="1" />
                </td>
                <td class="capitalize p-4 pl-0">Lia Khusniawati</td>
                <td class="text-center p-4">4</td>
                <td class="p-4 pr-2">
                  <div class="flex items-center justify-between">
                    <span class="format-rupiah" data-number="450000"></span>
                  </div>
                </td>
              </tr>
              <tr class="">
                <td class="p-4 pr-0">
                  <x-ui.form.checkbox id="checkAllTopPelanggan2" name="checkbox-table" value="2" />
                </td>
                <td class="capitalize p-4 pl-0">Lia Khusniawati</td>
                <td class="text-center p-4">4</td>
                <td class="p-4 pr-2">
                  <div class="flex items-center justify-between">
                    <span class="format-rupiah" data-number="450000"></span>
                  </div>
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

</x-app-layout>