<x-app-layout>
  <x-slot name="header">
    <div class="mb-6 sm:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Components') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <!-- Form Search -->
      <form class="mr-4 w-full" method="GET" action="./dashboard.php">
        <div class="text-gray-300 focus-within:text-primary relative mb-0">
          <span class="input-password bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-11" data-toggle="#passwordLogin">
            <x-heroicon-s-search class="fill-current h-5 w-5 duration-300 ease-in-out" />
          </span>
          <input id="searchDashboard" class="bg-white border-0 border-gray-200 focus:ring-0 placeholder-gray-300 rounded-lg focus:text-black font-inter text-sm tracking-tight outline-none py-3.5 px-4 pl-11 w-full" type="text" name="search" maxlength="80" aria-placeholder="Search..." placeholder="Search..." required autofocus>
        </div>
      </form>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <!-- Icons -->
  <div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
    <div class="flex flex-col pb-2 mb-4">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Icons</h3>
      <span class="text-gray-400 font-inter text-base font-normal tracking-tight">
        Pilihan/Pustaka icons heroikons.
      </span>
    </div>
    <div class="grid grid-cols-4 sm:grid-cols-8 lg:grid-cols-12 grid-rows-1 gap-4 select-none">
      @include('layouts.components.icons')
    </div>
  </div>

  <!-- Color's -->
  <div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
    <div class="flex flex-col pb-2 mb-4">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Colors</h3>
      <span class="text-gray-400 font-inter text-base font-normal tracking-tight">
        Pustaka warna, mempercepat dalam pemilihan warna.
      </span>
    </div>
    <div class="grid grid-cols-4 sm:grid-cols-8 lg:grid-cols-12 grid-rows-1 gap-4 select-none">
      @include('layouts.components.color')
    </div>
  </div>

  <!-- Button -->
  @include('layouts.components.button')

  <!-- Pagination -->
  @include('layouts.components.pagination')

  <!-- Alert -->
  @include('layouts.components.alert')

  <!-- Breadcrumb -->
  @include('layouts.components.breadcrumb')

  <!-- Dropdown -->
  @include('layouts.components.dropdown')

  <!-- Modal -->
  @include('layouts.components.modal')

  <!-- Form -->
  @include('layouts.components.form')

  <!-- Table -->
  @include('layouts.components.tabel')

  <!-- CKeditor -->
  @include('layouts.components.ckeditor')

</x-app-layout>