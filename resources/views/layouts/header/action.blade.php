<div class="hidden xl:flex relative">
  <x-ui.form.dropdown align="right">
    <x-slot name="trigger">
      <button class="bg-white inline-flex items-center border-0 rounded-lg text-primary font-inter text-sm font-normal tracking-tight outline-none py-3.5 px-4 duration-300 ease-in-out" type="button">
        <x-heroicon-o-user-circle class="stroke-current h-6 w-6 duration-300 ease-in-out" />
      </button>
    </x-slot>

    <x-slot name="content">
      <div class="py-2.5 px-5 pb-3">
        <div class="flex flex-col text-black font-inter text-sm font-semibold tracking-tight py-2 mb-1">
          {{ __('Pengaturan Akun') }}
        </div>

        <x-ui.form.dropdown-link href="{{ route('profile.show') }}">{{ __('Profil Pengguna') }}</x-ui.form.dropdown-link>
        <div class="border-t border-gray-200 my-2"></div>
        <form method="POST" action="{{ route('logout') }}">
          @csrf
          <x-ui.form.dropdown-link href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
            {{ __('LogOut') }}
          </x-ui.form.dropdown-link>
        </form>
      </div>
    </x-slot>
  </x-ui.form.dropdown>
</div>