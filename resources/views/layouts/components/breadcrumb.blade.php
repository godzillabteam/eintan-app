<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Breadcrumb</h3>
  </div>
  <div class="flex flex-col relative">
    <div class="mb-2">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-5 sm:pb-2 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center divide-x divide-gray-200 text-primary font-normal leading-none">
            <li class="pr-2.5">
              <a class="outline-none duration-300 ease-in-out" href="#">Dashboard</a>
            </li>
            <li class="px-2.5">
              <a class="outline-none duration-300 ease-in-out" href="#">Data Usulan</a>
            </li>
            <li class="px-2.5">
              <a class="outline-none duration-300 ease-in-out" href="#">Detail Data</a>
            </li>
            <li class="pl-2.5 text-gray-400" aria-current="page">Usulan Kegiatan 1</li>
          </ul>
        </nav>
      </div>
    </div>

    <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-5 sm:pb-2 w-full">
      <nav class="" aria-label="breadcrumb">
        <ul class="flex items-center text-gray-200 font-light leading-none">
          <li class="w-min">
            <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="#">Dashboard</a>
          </li>
          <li class="mx-2">/</li>
          <li class="w-min">
            <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="#">Data Usulan</a>
          </li>
          <li class="mx-2">/</li>
          <li class="w-min">
            <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="#">Detail Data</a>
          </li>
          <li class="mx-2">/</li>
          <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">Usulan Kegiatan 1</li>
        </ul>
      </nav>
    </div>
  </div>
</div>