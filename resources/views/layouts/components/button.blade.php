<!-- Solid Button's -->
<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Solid Button</h3>
  </div>
  <div class="inline-flex flex-wrap">
    <div class="mr-2">
      <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Primary" type="button" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="secondary" variant="default" size="base" rounded="base" title="Secondary" type="button" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="success" variant="default" size="base" rounded="base" title="Success" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="danger" variant="default" size="base" rounded="base" title="Danger" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="warning" variant="default" size="base" rounded="base" title="Warning" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="info" variant="default" size="base" rounded="base" title="Info" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="light" variant="default" size="base" rounded="base" title="Light" type="button" />
    </div>
    <div class="mt-2 lg:mt-0 mr-2">
      <x-ui.form.button background="dark" variant="default" size="base" rounded="base" title="Dark" type="button" />
    </div>
    <div class="mt-2 lg:mt-0 mr-2">
      <x-ui.form.button background="link" variant="default" size="base" rounded="base" title="Link" type="button" />
    </div>
  </div>
</div>

<!-- Outline Button's -->
<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Outline Button</h3>
  </div>
  <div class="inline-flex flex-wrap">
    <div class="mr-2">
      <x-ui.form.button background="outline-primary" variant="default" size="base" rounded="base" title="Primary" type="button" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="outline-secondary" variant="default" size="base" rounded="base" title="Secondary" type="button" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="outline-success" variant="default" size="base" rounded="base" title="Success" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="outline-danger" variant="default" size="base" rounded="base" title="Danger" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="outline-warning" variant="default" size="base" rounded="base" title="Warning" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="outline-info" variant="default" size="base" rounded="base" title="Info" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="outline-light" variant="default" size="base" rounded="base" title="Light" type="button" />
    </div>
    <div class="mt-2 lg:mt-0 mr-2">
      <x-ui.form.button background="outline-dark" variant="default" size="base" rounded="base" title="Dark" type="button" />
    </div>
    <div class="mt-2 lg:mt-0 mr-2">
      <x-ui.form.button background="outline-link" variant="default" size="base" rounded="base" title="Link" type="button" />
    </div>
  </div>
</div>

<!-- Status Button's -->
<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Status Button</h3>
  </div>
  <div class="inline-flex flex-wrap">
    <div class="mr-2">
      <x-ui.form.button background="status-primary" variant="default" size="base" rounded="base" title="Primary" type="button" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="status-secondary" variant="default" size="base" rounded="base" title="Secondary" type="button" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="status-success" variant="default" size="base" rounded="base" title="Success" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="status-danger" variant="default" size="base" rounded="base" title="Danger" type="button" />
    </div>
    <div class="mt-2 sm:mt-0 mr-2">
      <x-ui.form.button background="status-warning" variant="default" size="base" rounded="base" title="Warning" type="button" />
    </div>
  </div>
</div>

<!-- Button's With (Icons) -->
<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Button With Icons</h3>
  </div>
  <div class="divide-y divide-gray-200 divide-solid">
    <div class="inline-flex flex-wrap pb-8 w-full">
      <div class="mr-2">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Primary" icon-l="heroicon-s-collection" icon-type="fill" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="secondary" variant="default" size="base" rounded="base" title="Secondary" icon-l="heroicon-s-archive" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="success" variant="default" size="base" rounded="base" title="Success" icon-l="heroicon-s-paper-airplane" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="danger" variant="default" size="base" rounded="base" title="Danger" icon-l="heroicon-s-trash" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="warning" variant="default" size="base" rounded="base" title="Warning" icon-l="heroicon-s-document-text" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="info" variant="default" size="base" rounded="base" title="Info" icon-l="heroicon-o-speakerphone" icon-type="stroke" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="light" variant="default" size="base" rounded="base" title="Light" icon-l="heroicon-s-cog" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="dark" variant="default" size="base" rounded="base" title="Dark" icon-l="heroicon-s-cog" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 lg:mt-2 mr-2">
        <x-ui.form.button background="link" variant="default" size="base" rounded="base" title="Link" icon-l="heroicon-o-logout" icon-type="stroke" type="button" />
      </div>
    </div>
    <div class="inline-flex flex-wrap pt-8 w-full">
      <div class="mr-2">
        <x-ui.form.button background="outline-primary" variant="default" size="base" rounded="base" title="Primary" icon-l="heroicon-s-collection" icon-type="fill" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="outline-secondary" variant="default" size="base" rounded="base" title="Secondary" icon-l="heroicon-s-archive" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-success" variant="default" size="base" rounded="base" title="Success" icon-l="heroicon-s-paper-airplane" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-danger" variant="default" size="base" rounded="base" title="Danger" icon-l="heroicon-s-trash" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-warning" variant="default" size="base" rounded="base" title="Warning" icon-l="heroicon-s-document-text" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-info" variant="default" size="base" rounded="base" title="Info" icon-l="heroicon-o-speakerphone" icon-type="stroke" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-light" variant="default" size="base" rounded="base" title="Light" icon-l="heroicon-s-cog" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-dark" variant="default" size="base" rounded="base" title="Dark" icon-l="heroicon-s-cog" icon-type="fill" type="button" />
      </div>
      <div class="mt-2 lg:mt-2 mr-2">
        <x-ui.form.button background="outline-link" variant="default" size="base" rounded="base" title="Link" icon-l="heroicon-o-logout" icon-type="stroke" type="button" />
      </div>
    </div>
  </div>
</div>

<!-- Pill Button's -->
<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Pill Button</h3>
  </div>
  <div class="divide-y divide-gray-200 divide-solid">
    <div class="inline-flex flex-wrap pb-8 w-full">
      <div class="mr-2">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="pill" title="Primary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="secondary" variant="default" size="base" rounded="pill" title="Secondary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="success" variant="default" size="base" rounded="pill" title="Success" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="danger" variant="default" size="base" rounded="pill" title="Danger" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="warning" variant="default" size="base" rounded="pill" title="Warning" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="info" variant="default" size="base" rounded="pill" title="Info" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="light" variant="default" size="base" rounded="pill" title="Light" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="dark" variant="default" size="base" rounded="pill" title="Dark" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="link" variant="default" size="base" rounded="pill" title="Link" type="button" />
      </div>
    </div>
    <div class="inline-flex flex-wrap pt-8 w-full">
      <div class="mr-2">
        <x-ui.form.button background="outline-primary" variant="default" size="base" rounded="pill" title="Primary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="outline-secondary" variant="default" size="base" rounded="pill" title="Secondary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="outline-success" variant="default" size="base" rounded="pill" title="Success" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-danger" variant="default" size="base" rounded="pill" title="Danger" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-warning" variant="default" size="base" rounded="pill" title="Warning" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-info" variant="default" size="base" rounded="pill" title="Info" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-light" variant="default" size="base" rounded="pill" title="Light" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-dark" variant="default" size="base" rounded="pill" title="Dark" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-link" variant="default" size="base" rounded="pill" title="Link" type="button" />
      </div>
    </div>
  </div>
</div>

<!-- Square Button's -->
<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Square Button</h3>
  </div>
  <div class="divide-y divide-gray-200 divide-solid">
    <div class="inline-flex flex-wrap pb-8 w-full">
      <div class="mr-2">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="square" title="Primary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="secondary" variant="default" size="base" rounded="square" title="Secondary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="success" variant="default" size="base" rounded="square" title="Success" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="danger" variant="default" size="base" rounded="square" title="Danger" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="warning" variant="default" size="base" rounded="square" title="Warning" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="info" variant="default" size="base" rounded="square" title="Info" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="light" variant="default" size="base" rounded="square" title="Light" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="dark" variant="default" size="base" rounded="square" title="Dark" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="link" variant="default" size="base" rounded="square" title="Link" type="button" />
      </div>
    </div>
    <div class="inline-flex flex-wrap pt-8 w-full">
      <div class="mr-2">
        <x-ui.form.button background="outline-primary" variant="default" size="base" rounded="square" title="Primary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="outline-secondary" variant="default" size="base" rounded="square" title="Secondary" type="button" />
      </div>
      <div class="mr-2">
        <x-ui.form.button background="outline-success" variant="default" size="base" rounded="square" title="Success" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-danger" variant="default" size="base" rounded="square" title="Danger" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-warning" variant="default" size="base" rounded="square" title="Warning" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-info" variant="default" size="base" rounded="square" title="Info" type="button" />
      </div>
      <div class="mt-2 sm:mt-0 mr-2">
        <x-ui.form.button background="outline-light" variant="default" size="base" rounded="square" title="Light" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-dark" variant="default" size="base" rounded="square" title="Dark" type="button" />
      </div>
      <div class="mt-2 lg:mt-0 mr-2">
        <x-ui.form.button background="outline-link" variant="default" size="base" rounded="square" title="Link" type="button" />
      </div>
    </div>
  </div>
</div>

<!-- Block Level & Sizing Button's -->
<div class="grid grid-cols-12 gap-5 mb-12 select-none">
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Block Level Button</h3>
      </div>
      <div class="inline-flex flex-wrap w-full">
        <x-ui.form.button background="primary" variant="full" size="md" rounded="base" title="Block Level" type="button" />
      </div>
    </div>
  </div>
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Sizing Button</h3>
      </div>
      <div class="inline-flex flex-wrap items-end w-full">
        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="lg" rounded="base" title="Large" type="button" />
        </div>
        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Medium" type="button" />
        </div>
        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" rounded="base" title="Normal" type="button" />
        </div>
        <div class="mt-2 lg:mt-0 mr-2">
          <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Small" type="button" />
        </div>
      </div>
    </div>
  </div>
</div>