<!-- Form Control -->
<div class="grid grid-cols-12 gap-5 mb-12 select-none">
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Form Control</h3>
      </div>
      <div class="flex flex-col w-full">
        <div class="relative mb-4">
          <x-ui.form.label for="text_form_control" title="Nama Lengkap" />
          <x-ui.form.input id="namaLengkapFormControl" type="text" name="text_form_control" size="md" parent="" maxlength="80" aria-placeholder="Masukkan Nama Lengkap" placeholder="Masukkan Nama Lengkap" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="email_form_control" title="Email" />
          <x-ui.form.input id="emailFormControl" type="email" name="email_form_control" size="md" parent="" maxlength="80" aria-placeholder="Masukkan Email Address" placeholder="Masukkan Email Address" value="" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="password_form_control" title="Password" />
          <x-ui.form.input id="passwordFormControl" type="password" name="password_form_control" size="md" parent="" maxlength="80" aria-placeholder="Masukkan Password" placeholder="Masukkan Password" value="" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="date_form_control" title="Date" />
          <x-ui.form.input id="dateFormControl" type="date" name="date_form_control" size="md" parent="" maxlength="80" aria-placeholder="Tanggal" value="" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="time_form_control" title="Time" />
          <x-ui.form.input id="timeFormControl" type="time" name="time_form_control" size="md" parent="" maxlength="80" aria-placeholder="Tanggal" value="" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="notelp_form_control" title="No.Telp" />
          <x-ui.form.input id="noTelpFormControl" type="number" name="notelp_form_control" size="md" parent="" maxlength="13" aria-placeholder="Masukkan No.Telp" placeholder="Masukkan No.Telp" value="" required />
        </div>

        <div class="relative mb-4">
          <x-ui.form.label for="select_form_control" title="Kategori" />
          <select id="selectFormControl" class="bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg font-inter text-sm capitalize tracking-tight outline-none py-3.5 px-4 w-full appearance-none duration-300 ease-in-out" name="select_form_control" required>
            <option selected>Buka Pilihan Menu : </option>
            <option value="1">1</option>
            <option value="2">2</option>
            <option value="3">3</option>
            <option value="4">4</option>
            <option value="5">5</option>
          </select>
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="textarea_form_control" title="Pesan" />
          <x-ui.form.textarea id="textareaFormControl" type="text" name="textarea_form_control" size="md" rows="5" maxlength="255" aria-placeholder="Masukkan Pesan" placeholder="Masukkan Pesan" value="" message="" required />
        </div>
        <div class="relative mb-0">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Submit" type="submit" />
        </div>
      </div>
    </div>
  </div>
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Custom Control</h3>
      </div>
      <div class="flex flex-col divide-y divide-gray-200 divide-solid w-full">
        <div class="pb-8">
          <div class="relative mb-4">
            <x-ui.form.label for="left_icons_form" title="Left Icons" />

            <div class="text-gray-300 focus-within:text-primary relative">
              <input id="leftIconsForm" class="bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg focus:text-black font-inter text-sm tracking-tight outline-none py-3.5 px-4 pl-14 w-full duration-300 ease-in-out" type="email" name="left_icons_form" maxlength="80" aria-placeholder="Masukkan Email Address" placeholder="Masukkan Email Address" value="" required>
              <span class="bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-14">
                <x-heroicon-s-mail class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
              </span>
            </div>
          </div>
          <div class="relative mb-4">
            <x-ui.form.label for="right_icons_form" title="Right Icons" />

            <div class="text-gray-300 focus-within:text-primary relative">
              <input id="rightIconsForm" class="bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg focus:text-black font-inter text-sm tracking-tight outline-none py-3.5 px-4 pr-14 w-full duration-300 ease-in-out" type="email" name="right_icons_form" maxlength="80" aria-placeholder="Masukkan Email Address" placeholder="Masukkan Email Address" value="" required>
              <span class="bg-transparent flex items-center justify-center absolute top-0 right-0 bottom-0 w-14">
                <x-heroicon-s-mail class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
              </span>
            </div>
          </div>
          <div class="relative mb-4">
            <x-ui.form.label for="password_icons_form" title="Password" />

            <div class="text-gray-300 focus-within:text-primary relative">
              <x-ui.form.input id="passwordIconsForm" type="password" name="passwordIconsForm" size="md" parent="icons-r-md" maxlength="80" aria-placeholder="Masukkan Password" placeholder="Masukkan Password" required />
              <span class="toggle-passwrod bg-transparent flex items-center justify-center absolute top-0 right-0 bottom-0 w-14" data-toggle="#passwordIconsForm" data-size="md">
                <x-heroicon-s-eye-off class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
              </span>
            </div>
          </div>
          <div class="relative mb-3">
            <div class="flex flex-col position-relative mb-0">
              <x-ui.form.label for="select_search" title="Select Search" />

              <select id="selectSearch" class="custom-select custom-select2 rounded-lg shadow-none" name="select_search" required>
                <option selected>Pilih Perangkat Daerah : </option>
                <option value="1">Perangkat Daerah Timur</option>
                <option value="2">Perangkat Daerah Barat</option>
                <option value="3">Perangkat Daerah Selatan</option>
                <option value="4">Perangkat Daerah Utara</option>
                <option value="5">Perangkat Daerah I</option>
                <option value="5">Perangkat Daerah II</option>
                <option value="5">Perangkat Daerah III</option>
              </select>
            </div>
          </div>
        </div>
        <div class="py-8">
          <div class="relative mb-0">
            <x-ui.form.label for="checkbox_form" title="Checkbox" />

            <!-- With Event onclick (use 'click="namaFunction()"') -->
            <!-- <x-ui.form.checkbox id="checkbox1" name="checkbox_check" value="1" for="checkbox1" title="Check this custom checkbox" click="exampleClick()" /> -->

            <!-- With Event onchange (use 'change="namaFunction()"') -->
            <!-- <x-ui.form.checkbox id="checkbox1" name="checkbox_check" value="1" for="checkbox1" title="Check this custom checkbox" change="exampleChange()" /> -->

            <div class="relative">
              <x-ui.form.checkbox id="checkbox1" name="checkbox_check" value="1" for="checkbox1" title="Check this custom checkbox" checked="checked" />
            </div>
            <div class="relative">
              <x-ui.form.checkbox id="checkbox2" name="checkbox_check" value="2" for="checkbox2" title="This one custom checkbox" />
            </div>
            <div class="relative">
              <x-ui.form.checkbox id="checkbox3" name="checkbox_check" value="3" for="checkbox3" title="Disabled custom checkbox" disabled="disabled" />
            </div>
          </div>
        </div>
        <div class="py-8">
          <div class="relative mb-0">
            <x-ui.form.label for="radio_form" title="Radio" />

            <!-- With Event onclick (use 'click="namaFunction()"') -->
            <!-- <x-ui.form.radio id="radio1" name="radio_check" value="1" for="radio1" title="This checked custom radio" click="exampleClick()" /> -->

            <!-- With Event onchange (use 'change="namaFunction()"') -->
            <!-- <x-ui.form.radio id="radio1" name="radio_check" value="1" for="radio1" title="This checked custom radio" change="exampleChange()" /> -->

            <div class="relative">
              <x-ui.form.radio id="radio1" name="radio_check" value="1" for="radio1" title="Check this custom radio" checked="checked" />
            </div>
            <div class="relative">
              <x-ui.form.radio id="radio2" name="radio_check" value="2" for="radio2" title="This one custom radio" />
            </div>
            <div class="relative">
              <x-ui.form.radio id="radio3" name="radio_check" value="3" for="radio3" title="Disabled custom radio" disabled="disabled" />
            </div>
          </div>
        </div>
        <div class="pt-8">
          <div class="relative mb-0">
            <x-ui.form.label for="radio-inline" title="Radio Inline" />

            <div class="inline-flex flex-wrap">
              <div class="relative mr-5">
                <x-ui.form.radio id="radioInline1" name="radioinline_check" value="1" for="radio-inline1" title="Inline custom radio" checked="checked" />
              </div>
              <div class="relative">
                <x-ui.form.radio id="radioInline2" name="radioinline_check" value="2" for="radio-inline2" title="And another one" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Form Sizing -->
<div class="grid grid-cols-12 mb-12 gap-5 select-none">
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Form Control Sizing</h3>
      </div>
      <div class="flex flex-col w-full">
        <div class="relative mb-4">
          <x-ui.form.label for="large_form_control" title="Large" />
          <x-ui.form.input id="largeFormControl" type="text" name="large_form_control" size="lg" parent="" aria-placeholder="Large Form Control" placeholder="Large Form Control" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="medium_form_control" title="Medium" />
          <x-ui.form.input id="mediumFormControl" type="text" name="medium_form_control" size="md" parent="" aria-placeholder="Medium Form Control" placeholder="Medium Form Control" required />
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="normal_form_control" title="Normal" />
          <x-ui.form.input id="normalFormControl" type="text" name="normal_form_control" size="base" parent="" aria-placeholder="Normal Form Control" placeholder="Normal Form Control" required />
        </div>
        <div class="relative mb-0">
          <x-ui.form.label for="small_form_control" title="Small" />
          <x-ui.form.input id="smallFormControl" type="text" name="small_form_control" size="sm" parent="" aria-placeholder="Small Form Control" placeholder="Small Form Control" required />
        </div>
      </div>
    </div>
  </div>
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Form Control Sizing</h3>
      </div>
      <div class="flex flex-col w-full">
        <div class="relative mb-4">
          <x-ui.form.label for="large_form_control" title="Large" />

          <div class="text-gray-300 focus-within:text-primary relative">
            <x-ui.form.input id="iconLargeFormControl" type="text" name="large_form_control" size="lg" parent="icons-l-lg" aria-placeholder="Large Form Control" placeholder="Large Form Control" required />
            <span class="bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-14">
              <x-heroicon-s-mail class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
            </span>
          </div>
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="medium_form_control" title="Medium" />

          <div class="text-gray-300 focus-within:text-primary relative">
            <x-ui.form.input id="iconMediumFormControl" type="text" name="medium_form_control" size="md" parent="icons-l-md" aria-placeholder="Medium Form Control" placeholder="Medium Form Control" required />
            <span class="bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-14">
              <x-heroicon-s-mail class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
            </span>
          </div>
        </div>
        <div class="relative mb-4">
          <x-ui.form.label for="default_form_control" title="Normal" />

          <div class="text-gray-300 focus-within:text-primary relative">
            <x-ui.form.input id="iconDefaultFormControl" type="text" name="default_form_control" size="base" parent="icons-l-base" aria-placeholder="Default Form Control" placeholder="Default Form Control" required />
            <span class="bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-12">
              <x-heroicon-s-mail class="fill-current h-5 w-5 duration-300 ease-in-out" />
            </span>
          </div>
        </div>
        <div class="relative mb-0">
          <x-ui.form.label for="small_form_control" title="Small" />

          <div class="text-gray-300 focus-within:text-primary relative">
            <x-ui.form.input id="iconSmallFormControl" type="text" name="small_form_control" size="sm" parent="icons-l-sm" aria-placeholder="Small Form Control" placeholder="Small Form Control" required />
            <span class="bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-11">
              <x-heroicon-s-mail class="fill-current h-5 w-5 duration-300 ease-in-out" />
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>