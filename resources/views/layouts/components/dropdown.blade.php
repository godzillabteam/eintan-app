<div class="grid grid-cols-12 mb-12 gap-5">
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Dropdown</h3>
      </div>
      <div class="inline-flex lg:flex flex-wrap lg:justify-between relative w-full">
        <div class="mr-2">
          <x-ui.form.dropdown align="left">
            <x-slot name="trigger">
              <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Simple Dropdown" icon-r="heroicon-o-chevron-down" icon-type="fill" type="button" />
            </x-slot>

            <x-slot name="content">
              <div class="py-2.5 px-5 pb-3">
                <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                <div class="border-t border-gray-200 my-2"></div>
                <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
              </div>
            </x-slot>
          </x-ui.form.dropdown>
        </div>
        <div class="mt-2 lg:mt-0">
          <x-ui.form.dropdown align="right">
            <x-slot name="trigger">
              <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Header Dropdown" icon-r="heroicon-o-chevron-down" icon-type="fill" type="button" />
            </x-slot>

            <x-slot name="content">
              <div class="py-2.5 px-5 pb-3">
                <div class="flex flex-col text-black font-inter text-sm font-semibold tracking-tight py-2 mb-1">
                  {{ __('Manage Account') }}
                </div>

                <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                <div class="border-t border-gray-200 my-2"></div>
                <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
              </div>
            </x-slot>
          </x-ui.form.dropdown>
        </div>
      </div>
    </div>
  </div>
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Alignment Dropdown</h3>
      </div>
      <div class="flex sm:inline-flex lg:flex flex-wrap justify-between sm:justify-start lg:justify-between relative w-full">
        <div class="mr-2">
          <x-ui.form.dropdown align="left">
            <x-slot name="trigger">
              <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Left Dropdown" icon-r="heroicon-o-chevron-down" icon-type="fill" type="button" />
            </x-slot>

            <x-slot name="content">
              <div class="py-2.5 px-5">
                <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                <div class="border-t border-gray-200 my-2"></div>
                <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
              </div>
            </x-slot>
          </x-ui.form.dropdown>
        </div>
        <div class="sm:mt-2 lg:mt-0">
          <x-ui.form.dropdown align="right">
            <x-slot name="trigger">
              <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Right Dropdown" icon-r="heroicon-o-chevron-down" icon-type="fill" type="button" />
            </x-slot>

            <x-slot name="content">
              <div class="py-2.5 px-5">
                <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                <div class="border-t border-gray-200 my-2"></div>
                <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
              </div>
            </x-slot>
          </x-ui.form.dropdown>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Variation Dropdown</h3>
  </div>
  <div class="inline-flex flex-wrap relative w-full">
    <div class="mr-2">
      <x-ui.form.dropdown align="drop-right">
        <x-slot name="trigger">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Right Dropdown" icon-r="heroicon-o-chevron-right" icon-type="fill" type="button" />
        </x-slot>

        <x-slot name="content">
          <div class="py-2.5 px-5">
            <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
            <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
            <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
            <div class="border-t border-gray-200 my-2"></div>
            <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
          </div>
        </x-slot>
      </x-ui.form.dropdown>
    </div>
    <div class="mr-2">
      <x-ui.form.dropdown align="drop-up">
        <x-slot name="trigger">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Up Dropdown" icon-r="heroicon-o-chevron-up" icon-type="fill" type="button" />
        </x-slot>

        <x-slot name="content">
          <div class="py-2.5 px-5">
            <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
            <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
            <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
            <div class="border-t border-gray-200 my-2"></div>
            <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
          </div>
        </x-slot>
      </x-ui.form.dropdown>
    </div>
    <div class="mt-2 lg:mt-0">
      <x-ui.form.dropdown align="drop-left">
        <x-slot name="trigger">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Left Dropdown" icon-l="heroicon-o-chevron-left" icon-type="fill" type="button" />
        </x-slot>

        <x-slot name="content">
          <div class="py-2.5 px-5">
            <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
            <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
            <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
            <div class="border-t border-gray-200 my-2"></div>
            <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
          </div>
        </x-slot>
      </x-ui.form.dropdown>
    </div>
  </div>
</div>