<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Alert Modal</h3>
  </div>
  <div class="inline-flex flex-wrap">
    <div class="mr-2">
      <x-ui.form.button background="success" variant="default" size="base" rounded="base" title="Success" icon-l="heroicon-s-collection" icon-type="fill" type="button" onclick="alertSuccess()" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="info" variant="default" size="base" rounded="base" title="Info" icon-l="heroicon-o-speakerphone" icon-type="stroke" type="button" onclick="alertInfo()" />
    </div>
    <div class="mr-2">
      <x-ui.form.button background="warning" variant="default" size="base" rounded="base" title="Warning" icon-l="heroicon-s-document-text" icon-type="fill" type="button" onclick="alertWarning()" />
    </div>
    <div class="mr-2 mt-2 sm:mt-0">
      <x-ui.form.button background="danger" variant="default" size="base" rounded="base" title="Danger" icon-l="heroicon-s-trash" icon-type="fill" type="button" onclick="return alertDelete()" />
    </div>
  </div>
</div>

<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Simple Alert</h3>
  </div>
  <div class="flex flex-col relative">
    <div class="mb-3">
      <div class="bg-green-100 rounded-lg text-green-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Success)</p>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-gray-100 rounded-lg text-gray-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Secondary)</p>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-blue-100 rounded-lg text-blue-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Info)</p>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-yellow-100 rounded-lg text-yellow-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Warning)</p>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-red-100 rounded-lg text-red-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Danger)</p>
      </div>
    </div>
  </div>
</div>

<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Alert With Close Icons</h3>
  </div>
  <div class="flex flex-col relative">
    <div class="mb-3">
      <div class="bg-green-100 rounded-lg text-green-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Success)</p>
        <span class="flex items-center absolute inset-y-0 right-0 mr-4">
          <x-heroicon-o-x class="fill-current w-4 h-4" />
        </span>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-gray-100 rounded-lg text-gray-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Secondary)</p>
        <span class="flex items-center absolute inset-y-0 right-0 mr-4">
          <x-heroicon-o-x class="fill-current w-4 h-4" />
        </span>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-blue-100 rounded-lg text-blue-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Info)</p>
        <span class="flex items-center absolute inset-y-0 right-0 mr-4">
          <x-heroicon-o-x class="fill-current w-4 h-4" />
        </span>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-yellow-100 rounded-lg text-yellow-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Warning)</p>
        <span class="flex items-center absolute inset-y-0 right-0 mr-4">
          <x-heroicon-o-x class="fill-current w-4 h-4" />
        </span>
      </div>
    </div>
    <div class="mb-3">
      <div class="bg-red-100 rounded-lg text-red-600 font-inter text-sm font-normal leading-6 tracking-tight relative py-3 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <p>A simple alert with text and a right icon (Danger)</p>
        <span class="flex items-center absolute inset-y-0 right-0 mr-4">
          <x-heroicon-o-x class="fill-current w-4 h-4" />
        </span>
      </div>
    </div>
  </div>
</div>

<div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Custom Alert</h3>
  </div>
  <div class="grid grid-cols-12 gap-5">
    <div class="col-span-12 sm:col-span-6">
      <div class="bg-green-100 rounded-lg text-green-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5">Invoive Send successfully!</h3>
        <ul class="flex flex-col list-none pl-0">
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
        </ul>
      </div>
    </div>
    <div class="col-span-12 sm:col-span-6">
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5">Invoive Not Found</h3>
        <ul class="flex flex-col list-none pl-0">
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">A simple custom alert with title, example mode</li>
        </ul>
      </div>
    </div>
  </div>
</div>

<!-- Swal Alert Modal -->
<script>
  // Alert Success
  let alertSuccess = () => {
    swal({
      text: "File anda telah berhasil dikirimkan",
      icon: "success",
      button: {
        text: "Oke",
      },
    });
  };

  // Alert Info
  let alertInfo = () => {
    swal({
      title: "Pesan",
      text: "Anda mendapatkan pesan dari Admin",
      icon: "info",
      button: {
        text: "Oke",
      },
    });
  };

  // Alert Warning
  let alertWarning = () => {
    swal({
      title: "Peringatan",
      text: "Harap update versi terbaru, agar tidak terjadi bug",
      icon: "warning",
      button: {
        text: "Oke",
      },
    });
  };

  // Alert Delete
  let alertDelete = () => {
    swal({
      title: "Apakah Anda sudah yakin?",
      text: "Setelah dihapus, Anda tidak dapat memulihkan file ini!",
      icon: "warning",
      buttons: ["Batal", "Hapus"],
      dangerMode: true,
    }).then((willDelete) => {
      if (willDelete) {
        swal("File anda telah berhasil dihapus!", {
          icon: "success",
          button: {
            text: "Oke",
          },
        });
      }
    });
  };
</script>