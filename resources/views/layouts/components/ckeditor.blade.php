<div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">CKeditor</h3>
  </div>
  <div class="flex flex-col">
    <div class="relative">
      <x-ui.form.textarea id="exampleCKeditor" class="ckeditor" name="example_ckeditor" size="md" rows="5" maxlength="255" aria-placeholder="Masukkan Pesan" placeholder="Masukkan Pesan" value="" message="" required />
    </div>
  </div>
</div>