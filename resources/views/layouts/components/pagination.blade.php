<div class="grid grid-cols-12 gap-5 mb-12 select-none">
  <div class="col-span-12 lg:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Pagination</h3>
      </div>
      <div class="flex flex-col">
        <div class="mb-3">
          <nav aria-label="Page navigation example">
            <ul class="inline-flex list-none text-gray-400 font-inter text-base font-normal tracking-tight">
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 rounded-l-md hover:text-primary py-1.5 px-3.5 h-full duration-300 ease-in-out" href="#" tabindex="-1" aria-disabled="true">
                  Previous
                </a>
              </li>
              <li class="" aria-current="page">
                <a class="flex items-center border border-r-0 border-gray-200 py-1.5 px-3 h-full bg-primary text-white duration-300 ease-in-out" href="#">1</a>
              </li>
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 hover:text-primary py-1.5 px-3 h-full duration-300 ease-in-out" href="#">2</a>
              </li>
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 hover:text-primary py-1.5 px-3 h-full duration-300 ease-in-out" href="#">...</a>
              </li>
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 hover:text-primary py-1.5 px-3 h-full duration-300 ease-in-out" href="#">20</a>
              </li>
              <li class="">
                <a class="flex items-center border border-gray-200 rounded-r-md hover:text-primary py-1.5 px-3.5 h-full duration-300 ease-in-out" href="#">
                  Next
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="">
          <nav aria-label="Page navigation example">
            <ul class="inline-flex list-none text-gray-400 font-inter text-base font-normal tracking-tight">
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 rounded-l-md hover:text-primary py-1.5 px-2.5 h-full duration-300 ease-in-out" href="#" tabindex="-1" aria-disabled="true">
                  <x-heroicon-o-chevron-left class="stroke-current h-[18px] w-[18px]" />
                </a>
              </li>
              <li class="" aria-current="page">
                <a class="flex items-center border border-r-0 border-gray-200 py-1.5 px-3 h-full bg-primary text-white duration-300 ease-in-out" href="#">1</a>
              </li>
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 hover:text-primary py-1.5 px-3 h-full duration-300 ease-in-out" href="#">2</a>
              </li>
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 hover:text-primary py-1.5 px-3 h-full duration-300 ease-in-out" href="#">...</a>
              </li>
              <li class="">
                <a class="flex items-center border border-r-0 border-gray-200 hover:text-primary py-1.5 px-3 h-full duration-300 ease-in-out" href="#">20</a>
              </li>
              <li class="">
                <a class="flex items-center border border-gray-200 rounded-r-md hover:text-primary py-1.5 px-2.5 h-full duration-300 ease-in-out" href="#">
                  <x-heroicon-o-chevron-right class="stroke-current h-[18px] w-[18px]" />
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
  <div class="col-span-12 lg:col-span-6">
    <div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Pagination Customs</h3>
      </div>
      <div class="flex flex-col">
        <div class="mb-3">
          <nav aria-label="Page navigation example">
            <ul class="inline-flex list-none text-gray-400 font-inter text-base font-normal tracking-tight">
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-3.5 h-full duration-300 ease-in-out" href="#" tabindex="-1" aria-disabled="true">
                  Previous
                </a>
              </li>
              <li class="" aria-current="page">
                <a class="flex items-center text-primary hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">1</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">2</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">...</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">20</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-3.5 h-full duration-300 ease-in-out" href="#">
                  Next
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div class="">
          <nav aria-label="Page navigation example">
            <ul class="inline-flex list-none text-gray-400 font-inter text-base font-normal tracking-tight">
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2.5 h-full duration-300 ease-in-out" href="#" tabindex="-1" aria-disabled="true">
                  <x-heroicon-o-chevron-left class="stroke-current h-[18px] w-[18px]" />
                </a>
              </li>
              <li class="" aria-current="page">
                <a class="flex items-center text-primary hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">1</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">2</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">...</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2 h-full duration-300 ease-in-out" href="#">20</a>
              </li>
              <li class="">
                <a class="flex items-center hover:text-primary py-1.5 px-2.5 h-full duration-300 ease-in-out" href="#">
                  <x-heroicon-o-chevron-right class="stroke-current h-[18px] w-[18px]" />
                </a>
              </li>
            </ul>
          </nav>
        </div>
      </div>
    </div>
  </div>
</div>