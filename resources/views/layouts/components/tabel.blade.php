<div class="bg-white flex flex-col rounded py-5 px-[22px] pb-7 mb-12">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Table</h3>
  </div>
  <div class="flex flex-col w-full">
    <div class="whitespace-nowrap lg:whitespace-normal overflow-x-auto lg:overflow-visible w-full">
      <table id="tableOrderTerakhir" class="bg-white table border-collapse text-black relative mb-2 w-full">
        <thead class="bg-lightgreen-1 border-collapse border-t border-b border-gray-200 text-green font-inter text-xs font-semibold capitalize tracking-tight">
          <tr>
            <td scope="col" class="p-4 pt-5">
              <x-ui.form.checkbox id="checkAllLastOrder" name="checkbox-table" value="0" />
            </td>
            <td scope="col" class="p-4 pl-0" width="12%">Kode order</td>
            <td scope="col" class="p-4" width="18%">Kurir</td>
            <td scope="col" class="p-4">Alamat</td>
            <td scope="col" class="p-4" width="8%">Berat</td>
            <td scope="col" class="p-4 pr-0">Status</td>
          </tr>
        </thead>
        <tbody class="font-inter text-xs font-medium tracking-tight">
          <tr class="border-b border-gray-200">
            <td class="p-4 pt-5">
              <x-ui.form.checkbox id="checkLastOrder1" name="checkbox-table" value="1" />
            </td>
            <td class="p-4 pl-0">KLS221413123</td>
            <td class="p-4">Bagus Prakoso</td>
            <td class="p-4">Jalan Singgahan No 16, RT 07/RW 03, Desa Bungur Sari, Kec. Cibungur, Kab ...</td>
            <td class="p-4">4 Kg</td>
            <td class="p-4 pr-0">
              <div class="flex justify-between">
                <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="Batal" type="button" />

                <x-ui.form.dropdown align="drop-left">
                  <x-slot name="trigger">
                    <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-3.5 w-auto ml-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                    </button>
                  </x-slot>

                  <x-slot name="content">
                    <div class="py-2.5 px-5">
                      <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                      <div class="border-t border-gray-200 my-2"></div>
                      <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
                    </div>
                  </x-slot>
                </x-ui.form.dropdown>
              </div>
            </td>
          </tr>
          <tr>
            <td class="p-4 pt-5">
              <x-ui.form.checkbox id="checkLastOrder2" name="checkbox-table" value="2" />
            </td>
            <td class="p-4 pl-0">KLS221413123</td>
            <td class="p-4">Bagus Prakoso</td>
            <td class="p-4">Jalan Singgahan No 16, RT 07/RW 03, Desa Bungur Sari, Kec. Cibungur, Kab ...</td>
            <td class="p-4">4 Kg</td>
            <td class="p-4 pr-0">
              <div class="flex justify-between">
                <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="Selesai" type="button" />

                <x-ui.form.dropdown align="drop-left">
                  <x-slot name="trigger">
                    <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-3.5 w-auto ml-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                      <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                    </button>
                  </x-slot>

                  <x-slot name="content">
                    <div class="py-2.5 px-5">
                      <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                      <div class="border-t border-gray-200 my-2"></div>
                      <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
                    </div>
                  </x-slot>
                </x-ui.form.dropdown>
              </div>
            </td>
          </tr>
        </tbody>
      </table>
    </div>
  </div>
</div>

<div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8 mb-12">
  <div class="flex flex-col pb-2 mb-4">
    <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Data Tabel</h3>
  </div>
  <div class="flex flex-col relative w-full">
    <table id="exampleTables" class="table-data whitespace-nowrap lg:whitespace-normal">
      <thead>
        <tr>
          <td scope="col" class="first nosort">
            <x-ui.form.checkbox id="checkAllDataTable" name="checkbox-table" value="0" />
          </td>
          <td scope="col" width="12%">Kode order</td>
          <td scope="col" width="18%">Kurir</td>
          <td scope="col">Alamat</td>
          <td scope="col" width="8%">Berat</td>
          <td scope="col" class="nosort" width="8%">Status</td>
          <td scope="col" width="2%"></td>
        </tr>
      </thead>
      <tbody>
        <tr>
          <td class="first icons">
            <x-ui.form.checkbox id="checkDataTable1" name="checkbox-table" value="1" />
          </td>
          <td class="first-after">KLS221413123</td>
          <td>Bagus Prakoso</td>
          <td>Jalan Singgahan No 16, RT 07/RW 03, Desa Bungur Sari, Kec. Cibungur, Kab ...</td>
          <td>4 Kg</td>
          <td>
            <x-ui.form.button background="status-primary" variant="status" size="base" rounded="pill" title="Info" type="button" />
          </td>
          <td class="last">
            <x-ui.form.dropdown align="drop-left">
              <x-slot name="trigger">
                <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-3.5 w-auto ml-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                </button>
              </x-slot>

              <x-slot name="content">
                <div class="py-2.5 px-5">
                  <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                  <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                  <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                  <div class="border-t border-gray-200 my-2"></div>
                  <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
                </div>
              </x-slot>
            </x-ui.form.dropdown>
          </td>
        </tr>
        <tr>
          <td class="first icons">
            <x-ui.form.checkbox id="checkDataTable2" name="checkbox-table" value="2" />
          </td>
          <td class="first-after">KLS221413123</td>
          <td>Bagus Prakoso</td>
          <td>Jalan Singgahan No 16, RT 07/RW 03, Desa Bungur Sari, Kec. Cibungur, Kab ...</td>
          <td>4 Kg</td>
          <td>
            <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="Selesai" type="button" />
          </td>
          <td class="last">
            <x-ui.form.dropdown align="drop-left">
              <x-slot name="trigger">
                <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-3.5 w-auto ml-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                </button>
              </x-slot>

              <x-slot name="content">
                <div class="py-2.5 px-5">
                  <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                  <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                  <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                  <div class="border-t border-gray-200 my-2"></div>
                  <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
                </div>
              </x-slot>
            </x-ui.form.dropdown>
          </td>
        </tr>
        <tr>
          <td class="first icons">
            <x-ui.form.checkbox id="checkDataTable3" name="checkbox-table" value="3" />
          </td>
          <td class="first-after">KLS221413123</td>
          <td>Bagus Prakoso</td>
          <td>Jalan Singgahan No 16, RT 07/RW 03, Desa Bungur Sari, Kec. Cibungur, Kab ...</td>
          <td>4 Kg</td>
          <td>
            <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="Batal" type="button" />
          </td>
          <td class="last">
            <x-ui.form.dropdown align="drop-left">
              <x-slot name="trigger">
                <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-3.5 w-auto ml-2" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                </button>
              </x-slot>

              <x-slot name="content">
                <div class="py-2.5 px-5">
                  <x-ui.form.dropdown-link href="">{{ __('Action') }}</x-ui.form.dropdown-link>
                  <x-ui.form.dropdown-link href="">{{ __('Another action') }}</x-ui.form.dropdown-link>
                  <x-ui.form.dropdown-link href="">{{ __('Something else here') }}</x-ui.form.dropdown-link>
                  <div class="border-t border-gray-200 my-2"></div>
                  <x-ui.form.dropdown-link href="">{{ __('Separated link') }}</x-ui.form.dropdown-link>
                </div>
              </x-slot>
            </x-ui.form.dropdown>
          </td>
        </tr>

      </tbody>
    </table>
  </div>
</div>

<!-- JavaScript (Data Table) -->
<script>
  $(() => {
    $("#exampleTables").DataTable({
      scrollX: true,
      order: [
        [0, 'asc']
      ],
      "columnDefs": [{
          "targets": 0,
          orderable: false,
        },
        {
          "targets": 5,
          orderable: false,
        }
      ],
    });
  });
</script>