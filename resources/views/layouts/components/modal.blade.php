<div class="grid grid-cols-12 gap-5 select-none">
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px]">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Modal</h3>
      </div>
      <div class="inline-flex flex-wrap">
        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Demo Modal" type="button" data-modal="#demoModal" />
        </div>

        <!-- Demo Modal -->
        <div id="demoModal" class="modal" role="dialog">
          <div class="modal-dialog max-w-full sm:max-w-[500px]">
            <div class="modal-content">
              <div class="modal-heading">
                <div class="title">Demo Modal</div>
                <span class="text-gray-400" data-modal-dismiss="#demoModal">
                  <x-heroicon-o-x class="fill-current w-[18px] h-[18px]" />
                </span>
              </div>
              <div class="modal-body">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </p>
              </div>
              <div class="modal-footer">
                <div class="mr-2">
                  <x-ui.form.button background="secondary" variant="default" size="sm" rounded="base" title="Close" type="button" data-modal-dismiss="#demoModal" />
                </div>
                <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Save changes" type="button" />
              </div>
            </div>
          </div>
        </div>

        <!-- Button Static Modal -->
        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Static Modal" type="button" data-modal="#staticModal" />
        </div>

        <!-- Button Static Modal -->
        <div id="staticModal" class="modal" role="dialog">
          <div class="modal-dialog modal-dialog-scrollable max-w-full sm:max-w-[500px]">
            <div class="modal-content">
              <div class="modal-heading ">
                <div class="title">Demo Modal</div>
                <span class="text-gray-400" data-modal-dismiss="#staticModal">
                  <x-heroicon-o-x class="fill-current w-[18px] h-[18px]" />
                </span>
              </div>
              <div class="modal-body">
                <p>
                  What follows is just some placeholder text for this modal dialog. You just gotta ignite the light and let it shine!
                  Come just as you are to me. Just own the night like the 4th of July. Infect me with your love and fill me with your poison.
                  Come just as you are to me. End of the rainbow looking treasure. I can't sleep let's run away and don't ever look back,
                  don't ever look back. I can't sleep let's run away and don't ever look back, don't ever look back. Yes, we make angels cry,
                  raining down on earth from up above. I'm walking on air (tonight). Let you put your hands on me in my skin-tight jeans.
                  Stinging like a bee I earned my stripes. I went from zero, to my own hero. Even brighter than the moon, moon, moon.
                  Make 'em go, 'Aah, aah, aah' as you shoot across the sky-y-y! Why don't you let me stop by? Boom, boom, boom. Never made me
                  blink one time. Yeah, you're lucky if you're on her plane. Talk about our future like we had a clue. Oh my God no exaggeration.
                  You're original, cannot be replaced. The girl's a freak, she drive a jeep in Laguna Beach. It's no big deal, it's no big deal,
                  it's no big deal. In another life I would make you stay. I'm ma get your heart racing in my skin-tight jeans. I wanna walk on
                  your wave length and be there when you vibrate Never made me blink one time. We'd keep all our promises be us against the world.
                  In another life I would be your girl. We can dance, until we die, you and I, will be young forever. And on my 18th Birthday
                  we got matching tattoos. So open up your heart and just let it begin. 'Cause she's the muse and the artist. She eats your
                  heart out. Like Jeffrey Dahmer (woo). Pop your confetti. (This is how we do) I know one spark will shock the world, yeah
                  yeah. If you only knew what the future holds. Sipping on Rosé, Silver Lake sun, coming up all lazy. It’s in the palm of
                  your hand now baby. So we hit the boulevard. So make a wish, I'll make it like your birthday everyday. Do you ever feel
                  already buried deep six feet under? It's time to bring out the big balloons. You could've been the greatest. Passport stamps,
                  she's cosmopolitan. Your kiss is cosmic, every move is magic. We're living the life. We're doing it right. Open up your heart.
                  I was tryna hit it and quit it. Her love is like a drug. Always leaves a trail of stardust. The girl's a freak, she drive a
                  jeep in Laguna Beach. Fine, fresh, fierce, we got it on lock. All my girls vintage Chanel baby. Before you met me I was alright
                  but things were kinda heavy. Peach-pink lips, yeah, everybody stares. This is no big deal. Calling out my name. I could have
                  rewrite your addiction. She's got that, je ne sais quoi, you know it. Heavy is the head that wears the crown. 'Cause, baby,
                  you're a firework. Like thunder gonna shake the ground. Just own the night like the 4th of July! I’m gon’ put her in a coma.
                  What you're waiting for, it's time for you to show it off. Can't replace you with a million rings. You open my eyes and
                  I'm ready to go, lead me into the light. And here you are. I’m gon’ put her in a coma. Come on, let your colours burst.
                  So cover your eyes, I have a surprise. As I march alone to a different beat. Glitter all over the room pink flamingos
                  in the pool.
                </p>
              </div>
              <div class="modal-footer">
                <div class="mr-2">
                  <x-ui.form.button background="secondary" variant="default" size="sm" rounded="base" title="Close" type="button" data-modal-dismiss="#staticModal" />
                </div>
                <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Save changes" type="button" />
              </div>
            </div>
          </div>
        </div>

        <!-- Button Scrolling Content Modal -->
        <div class="mr-2 mt-2 lg:mt-0">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Scrolling Modal" type="button" data-modal="#scrollingModel" />
        </div>

        <!-- Scrolling Content Modal -->
        <div id="scrollingModel" class="modal" role="dialog">
          <div class="modal-dialog max-w-full sm:max-w-[500px]">
            <div class="modal-content">
              <div class="modal-heading">
                <div class="title">Demo Modal</div>
                <span class="text-gray-400" data-modal-dismiss="#scrollingModel">
                  <x-heroicon-o-x class="fill-current w-[18px] h-[18px]" />
                </span>
              </div>
              <div class="modal-body">
                <p>
                  What follows is just some placeholder text for this modal dialog. You just gotta ignite the light and let it shine!
                  Come just as you are to me. Just own the night like the 4th of July. Infect me with your love and fill me with your poison.
                  Come just as you are to me. End of the rainbow looking treasure. I can't sleep let's run away and don't ever look back,
                  don't ever look back. I can't sleep let's run away and don't ever look back, don't ever look back. Yes, we make angels cry,
                  raining down on earth from up above. I'm walking on air (tonight). Let you put your hands on me in my skin-tight jeans.
                  Stinging like a bee I earned my stripes. I went from zero, to my own hero. Even brighter than the moon, moon, moon.
                  Make 'em go, 'Aah, aah, aah' as you shoot across the sky-y-y! Why don't you let me stop by? Boom, boom, boom. Never made me
                  blink one time. Yeah, you're lucky if you're on her plane. Talk about our future like we had a clue. Oh my God no exaggeration.
                  You're original, cannot be replaced. The girl's a freak, she drive a jeep in Laguna Beach. It's no big deal, it's no big deal,
                  it's no big deal. In another life I would make you stay. I'm ma get your heart racing in my skin-tight jeans. I wanna walk on
                  your wave length and be there when you vibrate Never made me blink one time. We'd keep all our promises be us against the world.
                  In another life I would be your girl. We can dance, until we die, you and I, will be young forever. And on my 18th Birthday
                  we got matching tattoos. So open up your heart and just let it begin. 'Cause she's the muse and the artist. She eats your
                  heart out. Like Jeffrey Dahmer (woo). Pop your confetti. (This is how we do) I know one spark will shock the world, yeah
                  yeah. If you only knew what the future holds. Sipping on Rosé, Silver Lake sun, coming up all lazy. It’s in the palm of
                  your hand now baby. So we hit the boulevard. So make a wish, I'll make it like your birthday everyday. Do you ever feel
                  already buried deep six feet under? It's time to bring out the big balloons. You could've been the greatest. Passport stamps,
                  she's cosmopolitan. Your kiss is cosmic, every move is magic. We're living the life. We're doing it right. Open up your heart.
                  I was tryna hit it and quit it. Her love is like a drug. Always leaves a trail of stardust. The girl's a freak, she drive a
                  jeep in Laguna Beach. Fine, fresh, fierce, we got it on lock. All my girls vintage Chanel baby. Before you met me I was alright
                  but things were kinda heavy. Peach-pink lips, yeah, everybody stares. This is no big deal. Calling out my name. I could have
                  rewrite your addiction. She's got that, je ne sais quoi, you know it. Heavy is the head that wears the crown. 'Cause, baby,
                  you're a firework. Like thunder gonna shake the ground. Just own the night like the 4th of July! I’m gon’ put her in a coma.
                  What you're waiting for, it's time for you to show it off. Can't replace you with a million rings. You open my eyes and
                  I'm ready to go, lead me into the light. And here you are. I’m gon’ put her in a coma. Come on, let your colours burst.
                  So cover your eyes, I have a surprise. As I march alone to a different beat. Glitter all over the room pink flamingos
                  in the pool.
                </p>
              </div>
              <div class="modal-footer">
                <div class="mr-2">
                  <x-ui.form.button background="secondary" variant="default" size="sm" rounded="base" title="Close" type="button" data-modal-dismiss="#scrollingModel" />
                </div>
                <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Save changes" type="button" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-span-12 sm:col-span-6">
    <div class="bg-white flex flex-col rounded mb-12 py-5 px-[22px]">
      <div class="flex flex-col pb-2 mb-4">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight mb-1">Modal Sizing</h3>
      </div>
      <div class="inline-flex flex-wrap">
        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Large Modal" type="button" data-modal="#largeModal" />
        </div>

        <!-- Large Modal -->
        <div id="largeModal" class="modal" role="dialog">
          <div class="modal-dialog max-w-full sm:max-w-[500px] lg:max-w-[800px] xl:max-w-[1140px]">
            <div class="modal-content">
              <div class="modal-heading">
                <div class="title">Large Modal</div>
                <span class="text-gray-400" data-modal-dismiss="#largeModal">
                  <x-heroicon-o-x class="fill-current w-[18px] h-[18px]" />
                </span>
              </div>
              <div class="modal-body">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </p>
              </div>
              <div class="modal-footer">
                <div class="mr-2">
                  <x-ui.form.button background="secondary" variant="default" size="sm" rounded="base" title="Close" type="button" data-modal-dismiss="#largeModal" />
                </div>
                <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Save changes" type="button" />
              </div>
            </div>
          </div>
        </div>

        <div class="mr-2">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Medium Modal" type="button" data-modal="#mediumModal" />
        </div>

        <!-- Medium Modal -->
        <div id="mediumModal" class="modal" role="dialog">
          <div class="modal-dialog max-w-full sm:max-w-[500px] lg:max-w-[800px]">
            <div class="modal-content">
              <div class="modal-heading">
                <div class="title">Medium Modal</div>
                <span class="text-gray-400" data-modal-dismiss="#mediumModal">
                  <x-heroicon-o-x class="fill-current w-[18px] h-[18px]" />
                </span>
              </div>
              <div class="modal-body">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </p>
              </div>
              <div class="modal-footer">
                <div class="mr-2">
                  <x-ui.form.button background="secondary" variant="default" size="sm" rounded="base" title="Close" type="button" data-modal-dismiss="#mediumModal" />
                </div>
                <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Save changes" type="button" />
              </div>
            </div>
          </div>
        </div>

        <div class="mr-2 mt-2 lg:mt-0">
          <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Small Modal" type="button" data-modal="#smallModal" />
        </div>

        <!-- Small Modal -->
        <div id="smallModal" class="modal" role="dialog">
          <div class="modal-dialog max-w-full sm:max-w-[360px]">
            <div class="modal-content">
              <div class="modal-heading">
                <div class="title">Small Modal</div>
                <span class="text-gray-400" data-modal-dismiss="#smallModal">
                  <x-heroicon-o-x class="fill-current w-[18px] h-[18px]" />
                </span>
              </div>
              <div class="modal-body">
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna
                  aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
                  Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.
                </p>
              </div>
              <div class="modal-footer">
                <div class="mr-2">
                  <x-ui.form.button background="secondary" variant="default" size="sm" rounded="base" title="Close" type="button" data-modal-dismiss="#smallModal" />
                </div>
                <x-ui.form.button background="primary" variant="default" size="sm" rounded="base" title="Save changes" type="button" />
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>