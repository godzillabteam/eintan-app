<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <title>{{ config('app.name', 'Dashboard E-Intan') }}</title>
  <link rel="stylesheet" href="{{ url('./css/plugin/jquery.dataTables.min.css') }}">
  <link rel="stylesheet" href="{{ url('./css/plugin/select2.min.css') }}">
  <link rel="stylesheet" href="{{ url('./css/plugin/daterangepicker.min.css') }}">
  <link rel="stylesheet" href="{{ mix('css/app.css') }}">

  @livewireStyles

  <!-- Scripts -->
  <script src="{{ url('./js/plugin/jquery.min.js') }}"></script>
  <script src="{{ mix('js/app.js') }}" defer></script>
</head>

<body class="bg-gray-50 font-inter antialiased">
  <x-jet-banner />
  <!-- App -->
  <section class="flex flex-col relative">
    <!-- Sidebar -->
    @livewire('navigation-menu')

    <!-- Page Content -->
    <main class="relative pl-0 xl:pl-[340px] pt-[72px] xl:pt-0">
      <section class="pt-8 xl:pt-14 px-5 sm:px-12 pb-14">
        <!-- Header -->
        @if (isset($header))
        <header class="flex flex-col lg:flex-row lg:items-center justify-between mb-12">
          {{ $header }}
        </header>
        @endif

        {{ $slot }}
      </section>
    </main>
  </section>

  @stack('modals')

  <!-- JavaScript -->
  <script src="{{ url('./js/plugin/jquery.dataTables.min.js') }}"></script>
  <script src="{{ url('./js/plugin/select2.min.js') }}"></script>
  <script src="{{ url('./js/plugin/sweetalert.min.js') }}"></script>
  <script src="{{ url('./js/plugin/moment.min.js') }}"></script>
  <script src="{{ url('./js/plugin/daterangepicker.min.js') }}"></script>
  <script src="{{ url('./js/plugin/ckeditor/ckeditor.js') }}"></script>
  <script src="{{ url('./js/configs.js') }}"></script>
  <script>
    window.addEventListener('DOMContentLoaded', () => {
      const sidebarToggle = sidebarToggler("sidebarToggler"); // Toggle Sidebar
      const sidebarMenuDropdown = sidebarDropdown("sidebar-dropdown"); // Sidebar Menu Dropdown
      const showPassword = togglePassword('toggle-passwrod'); // Input (Show Password)
      const appModal = modal(); // Modal
      const formatUang = formatRupiah('format-rupiah'); // Format Rupiah
      const selectSearch = $('.custom-select2').select2(); // Select With Search

      // Data Table
      $.extend(true, $.fn.dataTable.defaults, {
        language: {
          searching: true,
          lengthMenu: "Tampilkan _MENU_ Data",
          zeroRecords: "Maaf, Tidak ada data yang ditemukan!",
          info: "Menampilkan _END_ dari _TOTAL_ data",
          infoEmpty: "Data tidak ditemukan",
          infoFiltered: "",
          paginate: {
            previous: "<svg class='stroke-current h-[18px] w-[18px]' xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' stroke='currentColor'><path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M15 19l-7-7 7-7'></path></svg>",
            next: "<svg class='stroke-current h-[18px] w-[18px]' xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' stroke='currentColor'><path stroke-linecap='round' stroke-linejoin='round' stroke-width='2' d='M9 5l7 7-7 7'></path></svg>",
          },
        },
      });
    }, false);
  </script>

  @livewireScripts
</body>

</html>