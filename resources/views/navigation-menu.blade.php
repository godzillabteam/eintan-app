<div x-data="{ open: false }">
  <!-- Navigation -->
  <nav class="bg-white shadow flex xl:hidden flex-wrap items-center fixed top-0 inset-x-0 px-5 sm:px-12 h-[72px] w-full z-base">
    <button id="sidebarToggler" class="hover:text-primary border-0 outline-none duration-300 ease-in-out" type="button" data-target="#sidebarLeft" data-icons="#toggleIconsButton">
      <div id="toggleIconsButton" class="hamburger pl-0">
        <span class="hamburger-items"></span>
        <span class="hamburger-items"></span>
        <span class="hamburger-items"></span>
      </div>
    </button>
    <a class="outline-none m-auto relative select-none" href="{{ route('dashboard') }}">
      <div class="h-9 w-auto">
        <img class="h-full w-auto" src="{{ url('./img/e-intan.png') }}" alt="Brand Logo">
      </div>
    </a>
    <div>
      <div class="flex items-center relative select-none">
        <x-ui.form.dropdown align="right">
          <x-slot name="trigger">
            @if (Laravel\Jetstream\Jetstream::managesProfilePhotos())
            <button type="button" class="bg-transparent shadow-none outline-none h-[72px]" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="rounded-full overflow-hidden relative h-11 w-11">
                <img class="bg-no-repeat bg-center bg-cover object-cover h-full w-full" src="{{ url('./img/wp-profiles.jpg') }}" alt="Images (Profiles)">
              </div>
            </button>
            @else
            <button type="button" class="bg-transparent shadow-none outline-none h-[72px]" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <div class="rounded-full overflow-hidden relative h-11 w-11">
                <img class="bg-no-repeat bg-center bg-cover object-cover h-full w-full" src="{{ url('./img/wp-profiles.jpg') }}" alt="Images (Profiles)">
              </div>
            </button>
            @endif
          </x-slot>

          <x-slot name="content">
            <div class="py-2.5 px-5 pb-3">
              <div class="flex flex-col text-black font-inter text-sm font-semibold tracking-tight py-2 mb-1">
                {{ __('Pengaturan Akun') }}
              </div>

              <x-ui.form.dropdown-link href="{{ route('profile.show') }}">{{ __('Profil Pengguna') }}</x-ui.form.dropdown-link>

              @if (Laravel\Jetstream\Jetstream::hasApiFeatures())
              <x-ui.form.dropdown-link href="{{ route('api-tokens.index') }}">{{ __('API Tokens') }}</x-ui.form.dropdown-link>
              @endif

              <div class="border-t border-gray-200 my-2"></div>

              <form method="POST" action="{{ route('logout') }}">
                @csrf
                <x-ui.form.dropdown-link href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                  {{ __('LogOut') }}
                </x-ui.form.dropdown-link>
              </form>
            </div>
          </x-slot>
        </x-ui.form.dropdown>
      </div>
    </div>
  </nav>

  <!-- Sidebar -->
  <section id="sidebarLeft" class="bg-primary flex flex-col flex-wrap fixed inset-y-0 left-0 pt-[72px] xl:pt-0 h-full w-[340px] duration-500 transform xl:transform-none -translate-x-full xl:translate-x-0 z-normal">
    <div class="sidebar-scroll flex flex-col overflow-y-auto pt-8 xl:pt-14 px-6 pb-0 h-full">
      <!-- Brand Logo -->
      <div class="hidden xl:flex px-3.5 mb-10 select-none h-[52px] w-auto">
        <img class="h-full w-auto" src="{{ url('./img/e-intan-white.png') }}" alt="Brand Logo">
      </div>

      <!-- Menu -->
      <div class="flex flex-col justify-between h-full">
        <ul class="flex flex-col list-none pl-0 mb-0 h-auto">
          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal ring-0 outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('dashboard') }}">
              <x-heroicon-s-view-grid class=" fill-current mr-2 h-6 w-6" />
              {{ __('Dashboard') }}
            </a>
          </li>

          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('usulanKegiatan.') }}">
              <x-heroicon-s-check-circle class="fill-current mr-2 h-6 w-6" />
              {{ __('Usulan Kegiatan') }}
            </a>
          </li>

          @if (Auth::user()->role->name=='admin' || Auth::user()->role->name=='bidang')
          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('kegiatanKelitbangan.') }}">
              <x-heroicon-s-clipboard-list class="fill-current mr-2 h-6 w-6" />
              {{ __('Kegiatan Kelitbangan') }}
            </a>
          </li>
          <li class="flex flex-col outline-none relative mb-1">
            <a class="sidebar-dropdown hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="#" data-toggle="sidebar-submenu">
              <x-heroicon-s-archive class="fill-current mr-2 h-6 w-6" />
              {{ __('Data Penelitian') }}
              <span class="flex items-center justify-center absolute inset-y-auto left-auto right-3.5 h-6 w-6">
                <x-heroicon-o-chevron-down class="parent-icons stroke-current h-[18px] w-[18px]" />
              </span>
            </a>

            <!-- Sub Menu Data Penelitian -->
            <ul class="sidebar-submenu sidebar-submenu-collapse flex-col border-l-2 border-solid border-gray-300 border-opacity-30 list-none pl-2 my-4 ml-[26px] h-auto" data-collapse="sidebar-submenu-collapse">
              <li class="flex flex-col outline-none relative mb-1">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('abstraksi-kajian.index') }}">
                  {{ __('Abstraksi Kajian') }}
                </a>
              </li>
              <li class="flex flex-col outline-none relative mb-0">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('rekomendasi-hasil-kelitbangan.index') }}">
                  {{ __('Rekomendasi Hasil') }}
                </a>
              </li>
            </ul>
          </li>
          @endif

          @if (Auth::user()->role->name=='perangkat')
          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('rekomendasi-hasil-kelitbangan.index') }}">
              <x-heroicon-s-archive class="fill-current mr-2 h-6 w-6" />
              {{ __('Rekomendasi Hasil') }}
            </a>
          </li>
          @endif

          @if (Auth::user()->role->name=='admin' || Auth::user()->role->name=='perangkat')
          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('rtl.index') }}">
              <x-heroicon-s-color-swatch class="fill-current mr-2 h-6 w-6" />
              {{ __('RTL') }}
            </a>
          </li>
          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('user-input.create') }}">
              <x-heroicon-s-chat-alt-2 class="fill-current mr-2 h-6 w-6" />
              {{ __('Saran & Masukkan') }}
            </a>
          </li>
          @endif

          @if (Auth::user()->role->name=='admin')
          <li class="flex flex-col outline-none relative mb-1">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('user-input.index') }}">
              <x-heroicon-s-annotation class="fill-current mr-2 h-6 w-6" />
              {{ __('Hasil Saran & Masukkan') }}
            </a>
          </li>

          <li class="flex flex-col outline-none relative mb-1">
            <a class="sidebar-dropdown hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="#" data-toggle="sidebar-submenu">
              <x-heroicon-s-database class="fill-current mr-2 h-6 w-6" />
              {{ __('Master Data') }}
              <span class="flex items-center justify-center absolute inset-y-auto left-auto right-3.5 h-6 w-6">
                <x-heroicon-o-chevron-down class="parent-icons stroke-current h-[18px] w-[18px]" />
              </span>
            </a>

            <!-- Sub Menu Data Penelitian -->
            <ul class="sidebar-submenu sidebar-submenu-collapse flex-col border-l-2 border-solid border-gray-300 border-opacity-30 list-none pl-2 my-4 ml-[26px] h-auto" data-collapse="sidebar-submenu-collapse">
              <li class="flex flex-col outline-none relative mb-0">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('sector.index') }}">
                  {{ __('Bidang') }}
                </a>
              </li>
              <li class="flex flex-col outline-none relative mb-0">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('question.index') }}">
                  {{ __('Pertanyaan') }}
                </a>
              </li>
              <li class="flex flex-col outline-none relative mb-1">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('survey.index') }}">
                  {{ __('Survei') }}
                </a>
              </li>
              <li class="flex flex-col outline-none relative mb-1">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('activity.index') }}">
                  {{ __('Aktivitas') }}
                </a>
              </li>
              <li class="flex flex-col outline-none relative mb-1">
                <a class="hover:bg-lightgreen-2 flex items-center rounded-[5px] font-inter text-white text-xs font-normal ring-0 outline-none relative py-2.5 pl-4 pr-0 duration-500 ease-in-out" href="{{ route('activity-sub.index') }}">
                  {{ __('Sub Aktivitas') }}
                </a>
              </li>
            </ul>
          </li>
          <li class="flex flex-col outline-none relative mb-0">
            <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('user.index') }}">
              <x-heroicon-s-cog class="fill-current mr-2 h-6 w-6" />
              {{ __('Pengguna Aplikasi') }}
            </a>
          </li>
          @endif
        </ul>

        <!-- LogOut -->
        <form method="POST" action="{{ route('logout') }}">
          @csrf
          <ul class="flex flex-col list-none pt-[152px] pl-0 pb-8 mb-0">
            <li class="flex flex-col outline-none relative mb-2">
              <a class="hover:bg-lightgreen-2 flex items-center rounded-lg font-inter text-white text-base font-normal outline-none relative py-4 px-4 duration-500 ease-in-out" href="{{ route('logout') }}" onclick="event.preventDefault(); this.closest('form').submit();">
                <x-heroicon-o-logout class="stroke-current mr-2 h-6 w-6" />
                {{ __('LogOut') }}
              </a>
            </li>
          </ul>
        </form>
      </div>
    </div>
  </section>
</div>