    <x-slot name="header">
      <div class="mb-6 sm:mb-0">
        <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Role') }}</h4>
      </div>
      <div class="flex items-center">
        <!-- Form Search -->
        <form class="mr-4 w-full" method="GET" action="./dashboard.php">
          <div class="text-gray-300 focus-within:text-primary relative mb-0">
            <span class="input-password bg-transparent flex items-center justify-center absolute top-0 left-0 bottom-0 w-11" data-toggle="#passwordLogin">
              <svg class="fill-current h-5 w-5 duration-300 ease-in-out" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" clip-rule="evenodd" d="M8 3.99997C6.93913 3.99997 5.92172 4.4214 5.17157 5.17155C4.42143 5.92169 4 6.93911 4 7.99997C4 9.06084 4.42143 10.0783 5.17157 10.8284C5.92172 11.5785 6.93913 12 8 12C9.06087 12 10.0783 11.5785 10.8284 10.8284C11.5786 10.0783 12 9.06084 12 7.99997C12 6.93911 11.5786 5.92169 10.8284 5.17155C10.0783 4.4214 9.06087 3.99997 8 3.99997ZM2 7.99997C1.99988 7.05568 2.22264 6.12468 2.65017 5.28271C3.0777 4.44074 3.69792 3.71157 4.4604 3.1545C5.22287 2.59743 6.10606 2.22819 7.03815 2.07681C7.97023 1.92543 8.92488 1.99618 9.82446 2.28332C10.724 2.57046 11.5432 3.06587 12.2152 3.72927C12.8872 4.39266 13.3931 5.20531 13.6919 6.10111C13.9906 6.9969 14.0737 7.95056 13.9343 8.88452C13.795 9.81848 13.4372 10.7064 12.89 11.476L17.707 16.293C17.8892 16.4816 17.99 16.7342 17.9877 16.9964C17.9854 17.2586 17.8802 17.5094 17.6948 17.6948C17.5094 17.8802 17.2586 17.9854 16.9964 17.9876C16.7342 17.9899 16.4816 17.8891 16.293 17.707L11.477 12.891C10.5794 13.5293 9.52335 13.9081 8.42468 13.9861C7.326 14.0641 6.22707 13.8381 5.2483 13.3329C4.26953 12.8278 3.44869 12.063 2.87572 11.1223C2.30276 10.1816 1.99979 9.10141 2 7.99997V7.99997Z">
              </svg>
            </span>
            <input id="searchDashboard" class="bg-white border-0 border-gray-200 focus:ring-0 placeholder-gray-300 rounded-lg focus:text-black font-inter text-sm tracking-tight outline-none py-3.5 px-4 pl-11 w-full" type="text" name="search" maxlength="80" aria-placeholder="Search..." placeholder="Search..." required autofocus>
          </div>
        </form>
        <div class="flex relative">
          <button class="bg-white inline-flex items-center border-0 rounded-lg text-primary font-inter text-sm font-normal tracking-tight outline-none py-3.5 px-4 duration-500 ease-in-out" type="button">
            <svg class="stroke-current h-6 w-6" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
              <path d="M15 17H20L18.5951 15.5951C18.2141 15.2141 18 14.6973 18 14.1585V11C18 8.38757 16.3304 6.16509 14 5.34142V5C14 3.89543 13.1046 3 12 3C10.8954 3 10 3.89543 10 5V5.34142C7.66962 6.16509 6 8.38757 6 11V14.1585C6 14.6973 5.78595 15.2141 5.40493 15.5951L4 17H9M15 17V18C15 19.6569 13.6569 21 12 21C10.3431 21 9 19.6569 9 18V17M15 17H9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path>
            </svg>
            <span class="bg-red-400 border border-white inline-flex rounded-full absolute top-1/3 right-1/3 w-2 h-2"></span>
          </button>
        </div>
      </div>
    </x-slot>
  
    <div class="py-12">
  
      <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
        @if (session()->has('message'))
            <div class="bg-teal-100 border-t-4 border-teal-500 rounded-b text-teal-900 px-4 py-3 shadow-md my-3" role="alert">
            <div class="flex">
                <div>
                <p class="text-sm">{{ session('message') }}</p>
                </div>
            </div>
            </div>
        @endif
        

        @if($isOpen)
            @include('livewire.create-roles')
        @endif
        <div class="whitespace-nowrap lg:whitespace-normal overflow-x-auto w-full">
          <table id="tableOrderTerakhir" class="bg-white table border-collapse text-black mb-2 w-full">
            <thead class="bg-lightgreen-1 border-collapse border-t border-b border-gray-200 text-green font-inter text-xs font-semibold capitalize tracking-tight">
              <tr>
                <td scope="col" class="p-4" width="5%">No</td>
                <td scope="col" class="p-4" width="80%">Role</td>
                <td scope="col" class="p-4 pr-0"></td>
              </tr>
              
            </thead>
            <tbody class="font-inter text-xs font-medium tracking-tight">
              @if (count($roles)==0)
              <tr class="border-b border-gray-200">
                <td class="p-4 text-center" colspan="6">
                  Data {{ __('Usulan Kegiatan') }} tidak ditemukan
                </td>
              </tr>
              @endif
              @foreach($roles as $index => $role)
                
              <tr class="border-b border-gray-200">
                <td class="p-4">
                    {{ $index + 1 }}
                </td>
                <td class="p-4">{{ $role->name }}</td>
                
                <td class="p-4 pr-0">
                  <div class="flex justify-between">
                    <x-ui.form.button background="status-success" wire:click="edit({{ $role->id }})" variant="status" size="base" rounded="pill" title="Edit" type="button" />                    
                    {{-- <x-ui.form.button background="status-danger" wire:click="delete({{ $role->id }})" variant="status" size="base" rounded="pill" title="Delete" type="button" />                     --}}
                  </div>
                </td>
              </tr>
              @endforeach
  
            </tbody>
          </table>
  
        </div>
      </div>
    </div>
