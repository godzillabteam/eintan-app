<button {{ $attributes }} @class([ 'font-inter font-normal capitalize tracking-tight outline-none select-none duration-300 ease-in-out' , 'rounded-lg'=> $rounded === "base" ,
  'rounded-full'=> $rounded === "pill" ,
  'rounded-0'=> $rounded === "square" ,

  'inline-flex items-center' => $iconL,
  'inline-flex items-center' => $iconR,

  'fill-current' => $iconType === "fill",
  'stroke-current' => $iconType === "stroke",

  'text-xxs py-2 px-3.5'=> $size === "sm" ,
  'text-sm py-[9px] px-3.5'=> $size === "base" ,
  'text-sm py-3.5 px-[18px]'=> $size === "md" ,
  'text-base px-6 py-5'=> $size === "lg" ,

  'inline-flex items-center'=> $variant === "default" ,
  'inline-flex items-center justify-center min-w-[7rem]'=> $variant === "status" ,
  'flex items-center justify-center w-full'=> $variant === "full" ,

  'bg-primary text-white'=> $background === "primary" ,
  'bg-secondary text-white'=> $background === "secondary" ,
  'bg-success text-white'=> $background === "success" ,
  'bg-danger text-white'=> $background === "danger" ,
  'bg-warning text-white'=> $background === "warning" ,
  'bg-info text-white'=> $background === "info" ,
  'bg-light text-black'=> $background === "light" ,
  'bg-black text-white'=> $background === "dark" ,
  'bg-transparent text-black'=> $background === "link" ,

  'bg-white hover:bg-primary border border-primary text-primary hover:text-white'=> $background === "outline-primary" ,
  'bg-white hover:bg-secondary border border-secondary text-secondary hover:text-white'=> $background === "outline-secondary" ,
  'bg-white hover:bg-success border border-success text-success hover:text-white'=> $background === "outline-success" ,
  'bg-white hover:bg-danger border border-danger text-danger hover:text-white'=> $background === "outline-danger" ,
  'bg-white hover:bg-warning border border-warning text-warning hover:text-white'=> $background === "outline-warning" ,
  'bg-white hover:bg-info border border-info text-info hover:text-white'=> $background === "outline-info" ,
  'bg-white hover:bg-light border border-light text-light hover:text-black'=> $background === "outline-light" ,
  'bg-white hover:bg-black border border-black text-black hover:text-white'=> $background === "outline-dark" ,
  'bg-white hover:bg-transparent border border-transparent text-black'=> $background === "outline-link" ,

  'bg-blue-100 text-blue-500'=> $background === "status-primary" ,
  'bg-gray-100 text-gray-500'=> $background === "status-secondary" ,
  'bg-green-100 text-green-500'=> $background === "status-success" ,
  'bg-red-100 text-red-500'=> $background === "status-danger" ,
  'bg-yellow-100 text-yellow-500'=> $background === "status-warning"
  ])>

  @if ($iconL & $iconType)

  @if($size === "sm")
  {{ svg($iconL, $iconType, ['class' => 'h-3.5 w-3.5 mr-1.5']) }}
  @elseif($size === "base")
  {{ svg($iconL, $iconType, ['class' => 'h-4 w-4 mr-1.5']) }}
  @elseif($size === "md")
  {{ svg($iconL, $iconType, ['class' => 'h-5 w-5 mr-2']) }}
  @elseif($size === "lg")
  {{ svg($iconL, $iconType, ['class' => 'h-5 w-5 mr-2']) }}
  @else
  {{ svg($iconL, $iconType, ['class' => 'h-4 w-4 mr-1.5']) }}
  @endif

  @endif

  {{ $title }}

  @if ($iconR)

  @if($size === "sm")
  {{ svg($iconR, $iconType, ['class' => 'h-3.5 w-3.5 ml-1.5']) }}
  @elseif($size === "base")
  {{ svg($iconR, $iconType, ['class' => 'h-4 w-4 ml-1.5']) }}
  @elseif($size === "md")
  {{ svg($iconR, $iconType, ['class' => 'h-5 w-5 ml-2']) }}
  @elseif($size === "lg")
  {{ svg($iconR, $iconType, ['class' => 'h-5 w-5 ml-2']) }}
  @else
  {{ svg($iconR, $iconType, ['class' => 'h-4 w-4 ml-1.5']) }}
  @endif

  @endif
</button>