<div class="flex items-center relative">
  <input id={{ $id }} @class([ 'text-primary border-gray-400 rounded focus:ring focus:ring-offset-0 focus:ring-primary focus:ring-opacity-30 outline-none focus:outline-none select-none duration-300 ease-in-out',
  'disabled:bg-gray-200 disabled:border-gray-400'
  ]) type="checkbox" name={{ $name }} value={{ $value }} 
  @if($click) onclick="{{$click}}" @endif 
  @if($change) onchange="{{$change}}" @endif 
  @if($checked==="checked" ) checked @endif 
  @if($disabled==="disabled" ) disabled @endif 
  >

  @if($for & $title)
  <label @if($disabled==="disabled" ) @class([ 'text-gray-400 font-inter text-base font-normal tracking-tight ml-3' ]) @else @class([ 'text-black font-inter text-base font-normal tracking-tight ml-3' ]) @endif for={{ $for }}>{{ $title }}</label>
  @endif
</div> 