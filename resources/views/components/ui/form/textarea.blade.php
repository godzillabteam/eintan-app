<textarea {{ $attributes }} @class([ 'bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg focus:text-black font-inter tracking-tight outline-none w-full duration-300 ease-in-out' , 
'text-xs py-2 px-3.5'=> $size === "sm" ,
'text-sm py-3 px-3.5'=> $size === "base" ,
'text-sm py-3.5 px-4'=> $size === "md" ,
'px-5 py-4'=> $size === "lg" ,
])>{{ $message }}</textarea>