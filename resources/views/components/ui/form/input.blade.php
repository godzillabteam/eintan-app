<input @class([ 'bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 focus:placeholder-black rounded-lg text-black focus:text-black font-inter tracking-tight outline-none w-full duration-300 ease-in-out' , 'text-xs py-2 px-3.5'=> $size === "sm" ,
'text-sm py-3 px-3.5'=> $size === "base" ,
'text-sm py-3.5 px-4'=> $size === "md" ,
'text-sm py-5 px-6'=> $size === "lg" ,

'pl-11'=> $parent === "icons-l-sm" ,
'pr-11'=> $parent === "icons-r-sm" ,
'pl-12'=> $parent === "icons-l-base" ,
'pr-12'=> $parent === "icons-r-base" ,
'pl-14'=> $parent === "icons-l-md" ,
'pr-14'=> $parent === "icons-r-md" ,
'pl-16'=> $parent === "icons-l-lg" ,
'pr-16'=> $parent === "icons-r-lg" ,
]) {{ $attributes }} />