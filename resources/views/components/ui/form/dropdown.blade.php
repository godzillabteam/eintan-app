@props(['align' => 'right', 'width' => 'max-content', 'contentClasses' => 'bg-white py-0', 'dropdownClasses' => ''])

@php
switch ($align) {
case 'left':
$alignmentClasses = 'origin-top-left left-0';
break;
case 'drop-left':
$alignmentClasses = 'origin-top-right top-0 left-auto right-full bottom-auto';
break;
case 'top':
$alignmentClasses = 'origin-top';
break;
case 'drop-up':
$alignmentClasses = 'origin-bottom top-auto left-auto right-0 bottom-full';
break;
case 'none':
case 'false':
$alignmentClasses = '';
break;
case 'drop-right':
$alignmentClasses = 'origin-top-left top-0 left-full right-auto bottom-auto';
break;
case 'right':
default:
$alignmentClasses = 'origin-top-right top-full left-auto right-0 bottom-auto';
break;
}

switch ($width) {
case '48':
$width = 'w-48';
break;
case 'max-content':
$width = 'w-max';
break;
}
@endphp

<div class="relative" x-data="{ open: false }" @click.away="open = false" @close.stop="open = false">
  <div @click="open = ! open">
    {{ $trigger }}
  </div>

  <div x-show="open" x-transition:enter="transition ease-out duration-200" x-transition:enter-start="transform opacity-0 scale-95" x-transition:enter-end="transform opacity-100 scale-100" x-transition:leave="transition ease-in duration-75" x-transition:leave-start="transform opacity-100 scale-100" x-transition:leave-end="transform opacity-0 scale-95" class="dropdown absolute {{ $width }} rounded-md shadow-lg {{ $alignmentClasses }} {{ $dropdownClasses }} z-[580]" style="display: none;" @click="open = false">
    <div class="rounded-lg border border-gray-200 ring-0 {{ $contentClasses }}">
      {{ $content }}
    </div>
  </div>
</div>