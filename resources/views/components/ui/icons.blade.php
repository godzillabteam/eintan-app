<div {{ $attributes }} @class([ 'border border-gray-300 hover:border-primary rounded-lg text-gray-300 hover:text-primary outline-none select-none h-16 w-16 duration-300 ease-in-out' , 
  'flex items-center justify-center' => $icon, 
  'fill-current' => $iconType === "fill",
  'stroke-current' => $iconType === "stroke", 
  ])>

  @if ($icon & $iconType)
  {{ svg($icon, $iconType, ['class' => 'h-[22px] w-[22px]']) }}
  @endif
</div>