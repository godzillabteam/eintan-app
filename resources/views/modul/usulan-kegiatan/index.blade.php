<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Usulan Kegiatan') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Usulan Kegiatan') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Data Usulan Kegiatan</h3>
      <a href="{{ route('usulanKegiatan.create') }}">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Tambah Data" type="button" />
      </a>
    </div>
    <div class="flex flex-col relative w-full">
      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      @endif

      <div class="pt-8 overflow-hidden">
        <form action="">
          <div class="flex overflow-y-hidden overflow-x-auto pb-4 mb-5">
            {{-- <div class="relative mr-3 min-w-max w-[17rem]">
              <x-ui.form.label for="filterDateUsulan" title="Tanggal" />
              <x-ui.form.input id="filterDateUsulan" type="text" name="filter_date_usulan" size="md" parent="" maxlength="80" aria-placeholder="Tanggal" value=""  />
            </div> --}}
            <div class="relative mr-3 min-w-max w-[17rem]">
              <x-ui.form.label for="proposed_year" title="Tahun" />
              <select id="proposed_year" class="custom-select custom-select2 rounded-lg shadow-none" name="proposed_year[]" multiple>
                @foreach ($years as $year)
                @if (in_array($year->year, $proposed_year))
                <option value="{{ $year->year }}" selected="selected">{{ $year->year }}</option>
                @else
                <option value="{{ $year->year }}">{{ $year->year }}</option>
                @endif
                @endforeach

              </select>
            </div>
            @if(Auth::user()->role->name!='perangkat')
            <div class="relative mr-3 min-w-max w-[17rem]">
              <x-ui.form.label for="user_id" title="Perangkat Daerah" />
              <select id="user_id" class="custom-select custom-select2 rounded-lg shadow-none" name="user_id[]" multiple>
                @foreach ($users as $user)
                @if (in_array($user->id, $user_id))
                <option value="{{ $user->id }}" selected="selected">{{ $user->name }}</option>
                @else
                <option value="{{ $user->id }}">{{ $user->name }}</option>
                @endif
                @endforeach
              </select>
            </div>
            @endif
            <div class="relative mr-3 min-w-max w-[17rem]">
              <x-ui.form.label for="type_proposal_id" title="Jenis Usulan" />
              <select id="type_proposal_id" class="custom-select custom-select2 rounded-lg shadow-none" name="type_proposal_id[]" multiple>
                @foreach ($type_proposals as $type_proposal)
                @if (in_array($type_proposal->id, $type_proposal_id))
                <option value="{{ $type_proposal->id }}" selected="selected">{{ $type_proposal->name }}</option>
                @else
                <option value="{{ $type_proposal->id }}">{{ $type_proposal->name }}</option>
                @endif
                @endforeach

              </select>
            </div>

            <div class="relative mr-3 min-w-max w-[17rem] ">
              <div class="mt-10"></div>
              <x-ui.form.button background="light" variant="default" size="base" rounded="base" title="Filter" icon-l="heroicon-s-filter" icon-type="fill" type="submit" />
            </div>


          </div>
        </form>

        <table id="tableUsulanKegiatan" class="table-data whitespace-nowrap xl:whitespace-normal">
          <thead>
            <tr>
              <td scope="col" width="18%">Judul Usulan</td>
              <td scope="col" width="18%">Jenis Usulan</td>
              <td scope="col" width="6%">Tahun</td>
              <td scope="col" width="14%">Perkiraan Anggaran</td>
              <td scope="col" width="14%">Perangkat Daerah</td>
              <td scope="col" width="5%">Status</td>
              <td scope="col" width="2%"></td>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <script>
    $(() => {
      $('#tableUsulanKegiatan').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ url("usulan-kegiatan/data") }}' + ('{{ $filter }}'),
        columns: [{
            "data": "type_proposal_title",
            "className": "first text-left"
          },
          {
            "data": "type_proposal.name"
          },
          {
            "data": "proposed_year"
          },
          {
            "data": "estimated_budget"
          },
          {
            "data": "user.name",
          },
          {
            "data": "status",
          },
          {
            "data": "id",
            "className": "last"
          },
        ],
        order: [
          [0, 'asc']
        ],
        "columnDefs": [{
            "targets": 0,
            "data": "type_proposal_title",
            "render": (data, type, row, meta) => {
              return '<a class="text-gray-600 hover:text-primary underline duration-300 ease-in-out" href="{{ url("usulan-kegiatan/detail") }}' + "/" + row.id + '">' + data + '</a>';
            }
          },
          {
            "targets": 3,
            "data": "estimated_budget",
            "render": (data, type, row, meta) => {
              let idr = numberWithCommas(data)
              return `Rp. ${idr}`;
            }
          },
          {
            "targets": 5,
            orderable: false,
            "data": "status",
            "render": (data, type, row, meta) => {
              if (data == 'draft') {
                return `
                  <div class="whitespace-nowrap">
                    <x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="Draft" type="button" />
                  </div>
                `;
              } else if (data == 'canceled') {
                return `
                  <div class="whitespace-nowrap">
                    <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="Dibatalkan" type="button" />
                  </div>
                `;
              } else {
                return `
                  <div class="whitespace-nowrap">
                    <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="Diproses" type="button" />
                  </div>
                `;
              }
            }
          },
          {
            "targets": 6,
            "data": "id",
            "render": (data, type, row, meta) => {
              if (row['status'] != 'canceled' && row['status'] != 'process') {
                var usulKegiatan_Detail = "{{ url('usulan-kegiatan/detail') }}" + "/" + data;
                var usulKegiatan_Delete = "{{ url('usulan-kegiatan/delete') }}/";
                var kegKelitbangan_Create = "{{ url('kegiatan-kelitbangan/create/') }}" + "/" + data;

                return `
                  <x-ui.form.dropdown align="drop-left">
                    <x-slot name="trigger">
                      <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                        <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                      </button> 
                    </x-slot>

                    <x-slot name="content">
                      <div class="py-2.5 px-5 pr-8">
                        <x-ui.form.dropdown-link href="` + usulKegiatan_Detail + `">
                          <x-heroicon-s-pencil-alt class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Perbarui') }}
                        </x-ui.form.dropdown-link>
                        <x-ui.form.dropdown-link href="` + kegKelitbangan_Create + `">
                          <x-heroicon-s-clipboard-list class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Buat Kegiatan') }}
                        </x-ui.form.dropdown-link>
                        <x-ui.form.dropdown-link class="text-danger hover:text-danger focus:text-danger" href="javascript:void(0);" onclick="return usulanDelete('` + data + `','` + usulKegiatan_Delete + `')">
                          <x-heroicon-s-trash class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Hapus') }}
                        </x-ui.form.dropdown-link>
                      </div>
                    </x-slot>
                  </x-ui.form.dropdown>
                `;
              } else {
                var usulKegiatan_Detail = "{{ url('usulan-kegiatan/detail') }}" + "/" + data;
                var kegKelitbangan_View = "{{ url('kegiatan-kelitbangan/redirect/') }}" + "/" + data;

                return `
                  <x-ui.form.dropdown align="drop-left">
                    <x-slot name="trigger">
                      <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                        <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                      </button> 
                    </x-slot>

                    <x-slot name="content">
                      <div class="py-2.5 px-5 pr-8">
                        <x-ui.form.dropdown-link href="` + usulKegiatan_Detail + `">
                          <x-heroicon-s-pencil-alt class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Detail') }}
                        </x-ui.form.dropdown-link>
                        <x-ui.form.dropdown-link href="` + kegKelitbangan_View + `">
                          <x-heroicon-s-clipboard-list class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Lihat Kegiatan') }}
                        </x-ui.form.dropdown-link>
                      </div>
                    </x-slot>
                  </x-ui.form.dropdown>
                `;
              }
            }
          },
        ],
      });

      // Format Uang
      let numberWithCommas = (x) => {
        return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
      }
      $('#filterDateUsulan').daterangepicker();
    });

    // Alert Delete (Ususlan Kegiatan)
    let usulanDelete = (getId, getURL) => {
      swal({
        title: "Apakah Anda sudah yakin?",
        text: "Setelah dihapus, Anda tidak dapat memulihkan Usulan Kegiatan ini!",
        icon: "warning",
        buttons: ["Batal", "Hapus"],
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {

          // swal("File anda telah berhasil dihapus!", {
          //   icon: "success",
          //   button: {
          //     text: "Oke",
          //   },
          // });

          $.ajax({
            type: 'GET',
            url: getURL + getId,
            // data: data,
            success: (data) => {
              swal("File anda telah berhasil dihapus!", {
                icon: "success",
                button: {
                  text: "Oke",
                },
              });
              setTimeout(() => {
                location.reload()
              }, 2000);
            },
            error: (data) => {
              swal("File anda tidak berhasil dihapus! ada masalah pada server", {
                icon: "warning",
                button: {
                  text: "Oke",
                },
              });
            }
          });
        }
      });
    };
  </script>
</x-app-layout>