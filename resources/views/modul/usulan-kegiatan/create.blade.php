<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Usulan Kegiatan') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('usulanKegiatan.') }}">{{ __('Usulan Kegiatan') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Tambah') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Tambah Data : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ __('Usulan Kegiatan') }}</p>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('usulanKegiatan.store') }}">
          @csrf

          <div class="grid grid-cols-12 gap-5 select-none">
            <div class="col-span-12 sm:col-span-6">
              <div class="flex flex-col relative">
                <x-ui.form.label for="user_id" title="Nama Perangkat Daerah" />

                <select id="user_id" class="custom-select custom-select2 rounded-lg shadow-none" name="user_id" required>
                  @foreach ($users as $user)
                  <option value="{{ $user->id }}">
                    {{ $user->name }}
                  </option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="type_proposal_id" title="Jenis Usulan Kelitbangan" />

                <select id="type_proposal_id" class="custom-select custom-select2 rounded-lg shadow-none" name="type_proposal_id" required>
                  @foreach ($typeProposals as $typeProposal)
                  <option value="{{ $typeProposal->id }}">{{ $typeProposal->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="type_proposal_title" title="Judul Usulan Kelitbangan" />
                <x-ui.form.input id="type_proposal_title" type="text" name="type_proposal_title" size="md" parent="" aria-placeholder="Masukkan judul usulan kelitbangan" placeholder="Masukkan judul usulan kelitbangan" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="proposed_year" title="Tahun Usulan" />
                <x-ui.form.input id="proposed_year" type="number" name="proposed_year" size="md" parent="" aria-placeholder="Masukkan tahun usulan" placeholder="Masukkan tahun usulan" required />
              </div>
            </div>
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.label for="problem" title="Permasalahan" />
                <x-ui.form.textarea id="problem" class="ckeditor" type="text" name="problem" size="md" rows="5" maxlength="255" aria-placeholder="Masukkan permasalahan usulan kelitbangan" placeholder="Masukkan permasalahan usulan kelitbangan" value="" message="" required />

              </div>
            </div>
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.label for="aim" title="Tujuan " />
                <x-ui.form.textarea id="aim" class="ckeditor" type="text" name="aim" size="md" rows="5" maxlength="255" aria-placeholder="Masukkan tujuan usulan kelitbangan" placeholder="Masukkan tujuan usulan kelitbangan" value="" message="" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="output" title="Out Put " />
                <x-ui.form.textarea id="output" class="ckeditor" type="text" name="output" size="md" rows="5" maxlength="255" aria-placeholder="Masukkan out put usulan kelitbangan" placeholder="Masukkan out put usulan kelitbangan" value="" message="" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="outcome" title="Out Come" />
                <x-ui.form.textarea id="outcome" class="ckeditor" type="text" name="outcome" size="md" rows="5" maxlength="255" aria-placeholder="Masukkan outcome usulan kelitbangan" placeholder="Masukkan out put usulan kelitbangan" value="" message="" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6 mb-4">
              <div class="relative">
                <x-ui.form.label for="estimated_budget" title="Perkiraan Anggaran (Rp.)" />
                <x-ui.form.input id="estimated_budget" type="number" name="estimated_budget" size="md" parent="" aria-placeholder="Masukkan perkiraan anggaran" placeholder="Masukkan perkiraan anggaran" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6 mb-4">
              <div class="relative">
                <x-ui.form.label for="status" title="Statu Usulan" />

                <select id="status" class="bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg font-inter text-sm capitalize tracking-tight outline-none py-3.5 px-4 w-full appearance-none duration-300 ease-in-out" name="status">
                  <option value="draft">Draft</option>
                  <option value="canceled">Canceled</option>
                </select>
              </div>
            </div>

            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan Data" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-app-layout>