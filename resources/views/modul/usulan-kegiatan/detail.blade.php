<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Usulan Kegiatan') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('usulanKegiatan.') }}">{{ __('Usulan Kegiatan') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Detail') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-start justify-between pb-4 mb-2">
      <div class="flex flex-col items-start">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">{{ $activityProposal->type_proposal_title }}</h3>
        <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight mb-1">{{ $activityProposal->type_proposal->name }}</p>
      </div>
      @if ($activityProposal->status!='canceled' && $activityProposal->status!='process')
      <a href="javascript:void(0);" onclick="return detailUsulanDelete({{ $activityProposal->id }})">
        <x-ui.form.button background="danger" variant="default" size="base" rounded="base" title="Hapus" icon-l="heroicon-s-trash" icon-type="fill" type="button" />
      </a>
      @endif
      @if ($activityProposal->status=='process')
      <a href="{{ route('kegiatanKelitbangan.redirect',$activityProposal->id) }}">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Lihat Kegiatan" type="button" />
      </a>
      @endif
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Whoops!</strong> There were some problems with your input.</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class=" pt-8">
        <dl class="font-inter">
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Nama Perangkat Daerah
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $activityProposal->user->name }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Permasalahan
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {!! $activityProposal->problem !!}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Tujuan
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {!! $activityProposal->aim !!}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Output
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {!! $activityProposal->output !!}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Outcome
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {!! $activityProposal->outcome !!}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Tahun Usulan
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $activityProposal->proposed_year }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Perkiraan Anggaran
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              Rp.{{ number_format($activityProposal->estimated_budget) }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex items-center text-black text-sm font-semibold">
              Status & Proses Selanjutnya
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-3.5 sm:mt-0">
              <span class="hidden sm:flex sm:items-center sm:mr-3">:</span>
              <div class="flex border border-gray-200 rounded-lg overflow-y-hidden overflow-x-auto py-3.5 px-3.5 w-max max-w-full">
                <div class="flex items-center">
                  @if ($activityProposal->status=='draft')
                  <x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="{{ ucfirst($activityProposal->status) }}" type="button" />
                  @elseif ($activityProposal->status=='canceled')
                  <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="{{ ucfirst($activityProposal->status) }}" type="button" />
                  @else
                  <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="{{ ucfirst($activityProposal->status) }}" type="button" />
                  @endif
                </div>
                @if ($activityProposal->status!='canceled' && $activityProposal->status!='process')
                <div class="flex ml-3">
                  <div class="mr-2.5">
                    <a class=" whitespace-nowrap" href="{{ route('usulanKegiatan.edit',$activityProposal->id) }}">
                      <x-ui.form.button background="dark" variant="default" size="base" rounded="base" title="Perbarui Usulan" type="button" />
                    </a>
                  </div>
                  <a class=" whitespace-nowrap" href="{{ route('kegiatanKelitbangan.create',$activityProposal->id) }}">
                    <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Buat Usulan Kegiatan" type="button" />
                  </a>
                </div>
                @endif
              </div>
            </dd>
          </div>
        </dl>
      </div>
    </div>
  </div>

  <!-- Detail Alert Delete (Ususlan Kegiatan) -->
  <script>
    let detailUsulanDelete = (getId) => {
      swal({
        title: "Apakah Anda sudah yakin?",
        text: "Setelah dihapus, Anda tidak dapat memulihkan Usulan Kegiatan ini!",
        icon: "warning",
        buttons: ["Batal", "Hapus"],
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          swal("File anda telah berhasil dihapus!", {
            icon: "success",
            button: {
              text: "Oke",
            },
          });

          // $.ajax({
          //   type: 'POST',
          //   url: 'url',
          //   data: data,
          //   success: (data) => {
          //     swal("File anda telah berhasil dihapus!", {
          //       icon: "success",
          //       button: {
          //         text: "Oke",
          //       },
          //     });
          //   },
          //   error: (data) => {
          //     swal("File anda tidak berhasil dihapus! ada masalah pada server", {
          //       icon: "warning",
          //       button: {
          //         text: "Oke",
          //       },
          //     });
          //   }
          // });
        }
      });
    };
  </script>
</x-app-layout>