<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Kegiatan Kelitbangan') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('kegiatanKelitbangan.') }}">{{ __('Kegiatan Kelitbangan') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Detail') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-start justify-between pb-4 mb-2">
      <div class="flex flex-col items-start">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">{{ $rndActivity->activity_proposal->type_proposal_title }}</h3>
        <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight mb-1">{{ $rndActivity->activity_proposal->type_proposal->name }}</p>
      </div>
    </div>
    <div class="flex flex-col relative">
      <div class="pt-8">
        <dl class="font-inter mb-8">
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Bidang Urusan
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $rndActivity->sector->name }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Program
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $rndActivity->program }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 pb-2 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Jenis Pelaksanaan
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-2 pb-4 sm:pb-2 px-0">
            <dt class="flex text-black text-sm font-normal">
              Swakelola
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $rndActivity->self_management }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-0 pb-4 px-0">
            <dt class="flex text-black text-sm font-normal">
              Penyedia(sebutkan nama CV)
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $rndActivity->provider }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex text-black text-sm font-semibold">
              Tahun Kegiatan
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-2 sm:mt-0">
              <span class="hidden sm:flex sm:mr-3">:</span>
              {{ $rndActivity->activity_year }}
            </dd>
          </div>
          <div class="sm:grid sm:grid-cols-3 py-4 px-0">
            <dt class="flex items-center text-black text-sm font-semibold">
              Status & Proses Selanjutnya
              <span class="flex sm:hidden ml-2 mr-2.5">:</span>
            </dt>
            <dd class="flex sm:col-span-2 text-sm text-gray-800 font-normal mt-3.5 sm:mt-0">
              <span class="hidden sm:flex sm:items-center sm:mr-3">:</span>
              <div class="flex border border-gray-200 rounded-lg overflow-y-hidden overflow-x-auto py-3.5 px-3.5 w-max max-w-full">
                <div class="flex items-center">
                  @if ($rndActivity->status=='draft')
                  <x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="{{ ucfirst($rndActivity->status) }}" type="button" />
                  @elseif ($rndActivity->status=='canceled')
                  <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="{{ ucfirst($rndActivity->status) }}" type="button" />
                  @else
                  <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="{{ ucfirst($rndActivity->status) }}" type="button" />
                  @endif
                </div>
                @if ($rndActivity->status!='canceled' && $rndActivity->status!='process')
                <div class="flex ml-3">
                  <div class="mr-2.5">
                    <a class=" whitespace-nowrap" href="{{ route('kegiatanKelitbangan.edit',$rndActivity->id) }}">
                      <x-ui.form.button background="dark" variant="default" size="base" rounded="base" title="Perbarui Kegiatan" icon-l="heroicon-s-pencil-alt" icon-type="fill" type="button" />
                    </a>
                  </div>
                  <a class=" whitespace-nowrap" href="{{ route('dataPenelitian.create',['data-penelitian',$rndActivity->activity_proposal_id]) }}">
                    <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Buat Penelitian" icon-l="heroicon-s-document-text" icon-type="fill" type="button" />
                  </a>
                </div>
                @endif
              </div>
            </dd>
          </div>
        </dl>
        <div class="tabel-detail-kelitbangan flex flex-col overflow-y-auto overflow-x-auto pr-4 h-[max-content] max-h-[435px]">
          <table id="activityTables0" class="activityTables bg-white border-collapse text-black mb-4 last:mb-0 w-full">
            <thead class="bg-lightgreen-1 border-collapse border-t border-b border-gray-200 text-green font-inter text-sm font-semibold capitalize tracking-tight whitespace-nowrap">
              <tr>
                <td scope="col" class="p-4" width="30%">Kegiatan Kelitbangan</td>
                <td scope="col" class="p-4">{{ $rndActivity->activity->name }}</>
                </td>
              </tr>
            </thead>
            <tbody id="subActivity0" class="border-b border-gray-200 font-inter text-xs font-medium tracking-tight">
              <tr class="subActivity0">
                <td class="p-4">Sub Kegiatan</td>
                <td class="p-4">
                  {{ $rndActivity->activity->activity_sub->name }}
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</x-app-layout>