<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Kegiatan Kelitbangan') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('kegiatanKelitbangan.') }}">{{ __('Kegiatan Kelitbangan') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Edit') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-start justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Edit Data : </h3>
      <div class="flex flex-col items-end">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">{{ $rndActivity->activity_proposal->type_proposal_title }}</h3>
        <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight mb-1">{{ $rndActivity->activity_proposal->type_proposal->name }}</p>
      </div>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('kegiatanKelitbangan.update') }}">
          @csrf

          <x-ui.form.input size="md" parent="" id="id" type="hidden" name="id" value="{{ $rndActivity->id }}" />

          <div class="flex flex-col w-full">
            <div class="grid grid-cols-12 gap-7 select-none">
              <div class="col-span-12 xl:col-span-6">
                <div class="grid grid-cols-12 gap-5 mb-4 xl:mb-0">
                  <div class="col-span-12 sm:col-span-6 xl:col-span-12">
                    <div class="relative">
                      <x-ui.form.label for="sector_id" title="Bidang Urusan" />
                      <select id="sector_id" class="custom-select custom-select2 rounded-lg shadow-none" name="sector_id" required>
                        <option>Pilih Bidang Urusan : </option>
                        @foreach ($sectors as $sector)
                        @if($rndActivity->sector_id==$sector->id)
                        <option value="{{ $sector->id }}" selected="selected">{{ $sector->name }}</option>

                        @else
                        <option value="{{ $sector->id }}">{{ $sector->name }}</option>

                        @endif
                        @endforeach
                      </select>
                      {{-- <x-ui.form.input id="business_field" type="text" name="business_field" size="md" parent="" aria-placeholder="Masukkan bidang urusan kegiatan" placeholder="Masukkan bidang urusan kegiatan" value="{{ $rndActivity->business_field }}" /> --}}
                    </div>
                  </div>
                  <div class="col-span-12 sm:col-span-6 xl:col-span-12">
                    <div class="relative">
                      <x-ui.form.label for="program" title="Program Kegiatan" />
                      <x-ui.form.input id="program" type="text" name="program" size="md" parent="" aria-placeholder="Masukkan program kegiatan kelitbangan" placeholder="Masukkan program kegiatan kelitbangan" value="{{ $rndActivity->program }}" />
                    </div>
                  </div>
                  <div class="col-span-12 sm:col-span-6 xl:col-span-12">

                    <div class="relative mb-4">
                      <x-ui.form.label for="text-form-control" title="Jenis Pelaksanaan" />
                      @if($rndActivity->type_implementation=="swakelola")
                      <div class="inline-flex flex-wrap">
                        <div class="relative mr-5">
                          <x-ui.form.radio id="radioInline1" click="checkCB()" checked="checked" name="type_implementation" value="swakelola" for="radio-inline1" title="Swakelola" />
                        </div>
                        <div class="relative">
                          <x-ui.form.radio id="radioInline2" click="checkCB()" name="type_implementation" value="penyedia" for="radio-inline2" title="Penyedia" />
                        </div>
                      </div>
                      @else
                      <div class="inline-flex flex-wrap">
                        <div class="relative mr-5">
                          <x-ui.form.radio id="radioInline1" click="checkCB()" name="type_implementation" value="swakelola" for="radio-inline1" title="Swakelola" />
                        </div>
                        <div class="relative">
                          <x-ui.form.radio id="radioInline2" click="checkCB()" checked="checked" name="type_implementation" value="penyedia" for="radio-inline2" title="Penyedia" />
                        </div>
                      </div>
                      @endif
                    </div>
                  </div>
                  <div id="swakelola" class="col-span-12 sm:col-span-6 xl:col-span-12">
                    <div class="relative">
                      <x-ui.form.label for="self_management" title="Swakelola" />
                      <x-ui.form.input id="self_management" type="text" name="self_management" size="md" parent="" aria-placeholder="Masukkan swakelola kegiatan" placeholder="Masukkan swakelola kegiatan" value="{{ $rndActivity->self_management }}" />
                    </div>
                  </div>
                  <div id="penyedia" class="col-span-12 sm:col-span-6 xl:col-span-12">
                    <div class="relative">
                      <x-ui.form.label for="provider" title="Penyedia (Sebutkan nama CV) " />
                      <x-ui.form.input id="provider" type="text" name="provider" size="md" parent="" aria-placeholder="Masukkan penyedia (Sebutkan nama CV) " placeholder="Masukkan penyedia (Sebutkan nama CV) " value="{{ $rndActivity->provider }}" />
                    </div>
                  </div>
                  <div class="col-span-12 sm:col-span-6 xl:col-span-12">
                    <div class="relative">
                      <x-ui.form.label for="activity_year" title="Tahun Kegiatan" />
                      <x-ui.form.input id="activity_year" type="number" name="activity_year" size="md" parent="" aria-placeholder="Masukkan tahun kegiatan" placeholder="Masukkan tahun kegiatan" value="{{ $rndActivity->activity_year }}" />
                    </div>
                  </div>
                </div>
              </div>
              <div class="activity-groups col-span-12 xl:col-span-6 bg-gray-100 bg-opacity-20 sm:border sm:border-gray-300 sm:rounded-lg overflow-y-auto overflow-x-hidden sm:py-[18px] pr-[18px] sm:px-[18px] h-[fit-content] max-h-[497px]">
                <div id="activityGroups" class="grid grid-cols-12 gap-5">
                  <div class="col-span-12 mb-3">
                    <div class="flex items-center xl:items-start justify-between relative">
                      <span>Kegiatan Kelitbangan</span>
                    </div>
                  </div>
                  <div class="col-span-12 mb-4 last:mb-0">
                    <div class="relative">
                      <x-ui.form.label for="activity_master_id" title="Kegiatan" />
                      <select id="activity_master_id" class="custom-select activty-data  rounded-lg shadow-none" name="activity_master_id">
                        <option value="{{ $rndActivity->activity->activity_master_id }}">{{ $rndActivity->activity->name }}</option>
                      </select>
                    </div>
                  </div>

                  <div class="col-span-12 mb-4 last:mb-0">
                    <div class="relative">
                      <x-ui.form.label for="activity_sub_master_id" title="Sub Kegiatan" />
                      <select id="activity_sub_master_id" class="custom-select activty-sub-data rounded-lg shadow-none " name="activity_sub_master_id" style="width:100%">
                        <option value="{{ $rndActivity->activity->activity_sub->activity_sub_master_id }}">{{ $rndActivity->activity->activity_sub->name }}</option>

                      </select>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <div class="col-span-12 mt-6">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan Data" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- JavaScript (Edit - Kegiatan Kelitbangan) -->
  <script>
    let checkCB = () => {
      var radios = document.getElementsByName('type_implementation');

      for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          if (radios[i].value != "penyedia") {
            $("#swakelola").show();
            $("#penyedia").hide();
          } else {
            $("#swakelola").hide();
            $("#penyedia").show();
          }

          break;
        }
      }
    }

    checkCB();

    $(() => {
      $('.activty-data').select2({
        ajax: {
          url: '{{ url("json/activity ? checked = ".$rndActivity->activity_master_id) }}',
          dataType: 'json'
        }
      }).on('select2:select', (e) => {
        var data = e.params.data;
        $('.activty-sub-data').val(null).trigger('change');
        getSub(data)
      });
    });

    let getSub = (data) => {
      $('.activty-sub-data').select2({
        ajax: {
          url: '{{ url("json/subactivity") }}/' + data.id + '?checked={{ $rndActivity->activity_master_id }}',
          dataType: 'json'
        }
      })
    }
  </script>
</x-app-layout>