@if (count($rnd_results)==0)
              <tr class="border-b border-gray-200">
                <td class="p-4 text-center" colspan="6">
                  Data {{ __('Usulan Kegiatan') }} tidak ditemukan
                </td>
              </tr>
              @endif
              @foreach ($rnd_results as $rnd_result)
                
              <tr class="border-b border-gray-200">
                <td class="p-4">
                  <a href="{{ route('rekomendasi-hasil-kelitbangan.edit',$rnd_result->id) }}" class="text-gray-500 underline">
                    {{  $rnd_result->activity_sub->name }}
                  </a>
                </td>
                <td class="p-4">
                    <a href="{{ route('kegiatanKelitbangan.detail',$rnd_result->activity_proposal->rnd_activity->id ) }}" class="text-gray-500 underline">
                        <?= $rnd_result->activity_proposal->type_proposal_title ?>
                    </a>
                </td>
                <td class="p-4">{{  $rnd_result->activity_proposal->study_abstract->year }}</td>
                <td class="p-4">{{  $rnd_result->recommendation_description }}</td>
                <td class="p-4">{{  $rnd_result->user->name }}</td>
                <td class="p-4">{{  $rnd_result->general_strategy }}</td>

               
                
              </tr>
              @endforeach