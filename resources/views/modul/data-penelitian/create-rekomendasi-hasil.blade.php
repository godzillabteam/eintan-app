<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Rekomendasi Hasil Kelitbangan') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('rekomendasi-hasil-kelitbangan.index') }}">{{ __('Rekomendasi Hasil') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Tambah') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-start justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Tambah Data : </h3>
      <div class="flex flex-col items-end">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">{{ $activityProposal->type_proposal_title }}</h3>
        <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight mb-1">{{ $activityProposal->type_proposal->name }} ( {{ $rndActivity->activity_year }} )</p>
      </div>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('dataPenelitian.store_hasil') }}">
          @csrf

          <div class="grid grid-cols-12 gap-7 select-none">
            <div class="col-span-12 xl:col-span-6">
              @if($activityProposal)
              <div class="relative">
                <x-ui.form.label for="text-form-control" title="Judul Kegiatan : {{ $activityProposal->type_proposal_title }}" />
                <span class="text-gray-400 font-inter text-base font-normal tracking-tight">
                  {{ $activityProposal->type_proposal->name }} ( {{ $rndActivity->activity_year }} )
                </span>
              </div>
              <x-ui.form.input id="activity_proposal_id" type="hidden" name="activity_proposal_id" size="md" parent="" value="{{ $activityProposal->id }}" />
              @else
              <div class="relative mb-4">
                <x-ui.form.label for="text-form-control" title="Usulan Kegiatan" />
                <select id="activity_proposal_id" class="bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg font-inter text-sm capitalize tracking-tight outline-none py-3.5 px-4 w-full appearance-none duration-300 ease-in-out" name="activity_proposal_id" required>
                  <option selected>Pilih Usulan Kegiatan : </option>
                  @foreach ($activityProposals as $activityProposal)
                  <option value="{{ $activityProposal->id }}">{{ $activityProposal->type_proposal_title }}</option>
                  @endforeach
                </select>
              </div>
              @endif
            </div>
            <div class="col-span-12">
              <table id="activityTables0" class="bg-white table border-collapse text-black mb-2 w-full activityTables">
                <thead class="bg-lightgreen-1 border-collapse border-t border-b border-gray-200 text-green font-inter text-xs font-semibold capitalize tracking-tight">
                  <tr>
                    <td scope="col" class="p-4" width="10%">Kegiatan</td>
                    <td scope="col" class="p-4" width="30%">
                      {{ $rndActivity->activity->name }}
                    </td>
                  </tr>
                </thead>
                <tbody id="subActivity0" class="font-inter text-xs font-medium tracking-tight">
                  <tr class="border-b border-gray-200 subActivity0">
                    <td class="p-4">Sub Kegiatan</td>
                    <td class="p-4">{{ $rndActivity->activity->activity_sub->name }}</td>
                  </tr>
                  <tr class="border-b border-gray-200 subActivity0">
                    <td class="p-4">Uraian Rekomendasi</td>
                    <td class="p-4">
                      <x-ui.form.textarea id="recommendation_description-{{ $rndActivity->activity->activity_sub->id }}" type="text" name="recommendation_description[{{ $rndActivity->activity->activity_sub->id }}]" size="md" rows="5" maxlength="255" aria-placeholder="Uraian Rekomendasi" placeholder="Uraian Rekomendasi" value="" message="" required />
                    </td>
                  </tr>
                  <tr class="border-b border-gray-200 subActivity0">
                    <td class="p-4">Penerima Rekomendasi</td>

                    <td class="p-4">
                      <select id="user_id" class="custom-select custom-select2 rounded-lg shadow-none" name="user_id[{{ $rndActivity->activity->activity_sub->id }}]" required>
                        <option selected>Pilih Penerima Rekomendasi : </option>
                        @foreach ($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                      </select>
                    </td>
                  </tr>
                  <tr class="subActivity0">
                    <td class="p-4">Strategi Umum</td>
                    <td class="p-4">
                      <x-ui.form.input id="general_strategy-{{ $rndActivity->activity->activity_sub->id }}" type="text" name="general_strategy[{{ $rndActivity->activity->activity_sub->id }}]" size="md" parent="" aria-placeholder="Strategi Umum" placeholder="Strategi Umum" required />
                    </td>
                  </tr>
                </tbody>
              </table>
            </div>

            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan Data" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-app-layout>