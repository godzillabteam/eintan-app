<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Abstraksi Kajian') }}
      </h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">
              {{ __('Abstraksi Kajian') }}
            </li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">
        {{ __('Data Abstraksi Kajian') }}
      </h3>
      <a href="{{ route('kegiatanKelitbangan.create') }}">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Tambah Data" type="button" />
      </a>
    </div>
    <div class="flex flex-col relative w-full">

      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      @endif

      <div class="pt-8 overflow-hidden">
        <table id="tableAbstraksiKajian" class="table-data whitespace-nowrap xl:whitespace-normal">
          <thead>
            <tr>
              <td scope="col">Judul Kegiatan</td>
              <td scope="col" width="0%">Tahun</td>
              <td scope="col">Pelaksana</td>
              <td scope="col">Abstraksi</td>
              <td scope="col" width="0%">Link</td>
              <td scope="col" width="0%"></td>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- JavaScript (Abstraksi Kajian) -->
  <script>
    $(() => {
      $('#tableAbstraksiKajian').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ url("data-penelitian/abstraksi-kajian/data") }}',
        columns: [{
            "data": "id",
            "className": "first text-left"
          },
          {
            "data": "year"
          },
          {
            "data": "sector.name"
          },
          {
            "data": "abstraction"
          },
          {
            "data": "link"
          },
          {
            "data": "id",
            "className": "last text-left"
          },
        ],
        order: [
          [0, 'asc']
        ],
        "columnDefs": [{
            "targets": 0,
            "data": "id",
            "render": (data, type, row, meta) => {
              return '<a class="text-gray-500 hover:text-primary focus:text-primary underline duration-300 ease-in-out" href="{{ url("data-penelitian/abstraksi-kajian") }}/' +
                data + '/edit">' + row.activity_proposal.type_proposal_title + '</a>';
            }
          },
          {
            "targets": 4,
            orderable: false,
            "data": "link",
            "render": (data, type, row, meta) => {
              return '<a class="text-gray-500 hover:text-primary focus:text-primary underline duration-300 ease-in-out" href="' +
                data + '" target="_blank">' + data + '</a>';
            }
          },
          {
            "targets": 5,
            "data": "id",
            "render": (data, type, row, meta) => {
              var abstraksiEdit = "{{ url('data-penelitian/abstraksi-kajian') }}/" + data + "/edit";

              return `
                <x-ui.form.dropdown align="drop-left">
                  <x-slot name="trigger">
                    <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                      <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                    </button> 
                  </x-slot>

                  <x-slot name="content">
                    <div class="py-2.5 px-5 pr-8">
                      <x-ui.form.dropdown-link href="` + abstraksiEdit + `">
                        <x-heroicon-s-document-text class="fill-current mr-2 h-[18px] w-[18px]" />
                        {{ __('Detail Abtraksi') }}
                      </x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link href="` + row.link + `" target="_blank">
                        <x-heroicon-o-link class="stroke-current mr-2 h-[18px] w-[18px]" />
                        {{ __('E-Journal') }}
                      </x-ui.form.dropdown-link>
                    </div>
                  </x-slot>
                </x-ui.form.dropdown>
              `;
            }
          },
        ]
      });
    });
  </script>
</x-app-layout>