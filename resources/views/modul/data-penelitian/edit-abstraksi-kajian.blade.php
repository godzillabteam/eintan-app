<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Abstraksi Kajian') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('abstraksi-kajian.index') }}">{{ __('Abstraksi Kajian') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Edit') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-start justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Edit Data : </h3>
      <div class="flex flex-col items-end">
        <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">{{ $study_abstract->activity_proposal->type_proposal_title }}</h3>
        <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight mb-1">{{ $study_abstract->activity_proposal->type_proposal->name }}</p>
      </div>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('dataPenelitian.store') }}">
          @csrf

          <x-ui.form.input id="activity_proposal_id" type="hidden" name="activity_proposal_id" size="md" parent="" value="{{ $study_abstract->activity_proposal->id }}" />

          <div class="grid grid-cols-12 gap-7 select-none">
            <div class="col-span-12 xl:col-span-6">
              <div class="relative mb-4">
                <x-ui.form.label for="executor" title="Pelaksana" />
                {{-- <x-ui.form.input id="executor" type="text" name="executor" size="md" parent="" aria-placeholder="Pelaksana" placeholder="Pelaksana"  value="{{ $study_abstract->executor }}"/> --}}
                <select id="sector_id" class="custom-select custom-select2 rounded-lg shadow-none" name="sector_id" required>
                  <option>Pilih Pelaksana: </option>
                  @foreach ($sectors as $sector)
                  @if($study_abstract->sector_id==$sector->id)
                  <option value="{{ $sector->id }}" selected="selected">{{ $sector->name }}</option>
                  @else
                  <option value="{{ $sector->id }}">{{ $sector->name }}</option>
                  @endif
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-span-12 xl:col-span-6">
              <div class="relative">
                <x-ui.form.label for="year" title="Tahun Rekomendasi" />
                <x-ui.form.input id="year" type="number" name="year" size="md" parent="" aria-placeholder="Tahun Rekomendasi" placeholder="Tahun Rekomendasi" value="{{ $study_abstract->year }}" />
              </div>
            </div>
            <div class="col-span-12 xl:col-span-6">
              <div class="relative">
                <x-ui.form.label for="abstraction" title="Abstraksi" />
                <x-ui.form.textarea id="abstraction" type="text" name="abstraction" size="md" rows="5" maxlength="255" aria-placeholder="Abstraksi" placeholder="Abstraksi" value="" message="{{ $study_abstract->abstraction }}" />
              </div>
            </div>
            <div class="col-span-12 xl:col-span-6">
              <div class="relative">
                <x-ui.form.label for="link" title="Link E-Journal" />
                <x-ui.form.input id="link" type="url" name="link" size="md" parent="" aria-placeholder="Link E-Journal" placeholder="Link E-Journal" value="{{ $study_abstract->link }}" />
              </div>
            </div>

            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan Data" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</x-app-layout>