<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Rekomendasi Hasil Kelitbangan') }}
      </h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">
              {{ __('Rekomendasi Hasil') }}
            </li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">
        {{ __('Data Rekomendasi Hasil') }}
      </h3>
      <a href="{{ route('kegiatanKelitbangan.create') }}">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Tambah Data" type="button" />
      </a>
    </div>
    <div class="flex flex-col relative w-full">

      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      @endif

      <div class="pt-8 overflow-hidden">
        <table id="tableHasilRekomendasi" class="table-data whitespace-nowrap xl:whitespace-normal">
          <thead>
            <tr>
              <td scope="col">Kegiatan/Sub</td>
              <td scope="col">Judul</td>
              <td scope="col" width="0%">Tahun</td>
              <td scope="col">Uraian</td>
              <td scope="col">Strategi</td>
              <td scope="col" width="0%">Tindak Lanjut</td>
              <td scope="col" width="0%"></td>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- JavaScript (Rekomendasi Hasil) -->
  <script>
    $(() => {
      $('#tableHasilRekomendasi').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ url("data-penelitian/rekomendasi-hasil-kelitbangan/data") }}',
        columns: [{
            "data": "id",
            "className": "first text-left"
          },
          {
            "data": "activity_proposal.id"
          },
          {
            "data": "activity_proposal.study_abstract.year"
          },
          {
            "data": "recommendation_description"
          },
          {
            "data": "general_strategy"
          },
          {
            "data": "rtl.id"
          },
          {
            "data": "rtl.id",
            "className": "last text-left"
          },
        ],
        order: [
          [0, 'asc']
        ],
        "columnDefs": [{
            "targets": 0,
            "render": (data, type, row, meta) => {
              return '<a class="text-gray-500 hover:text-primary focus:text-primary underline" href="{{ url("data-penelitian/rekomendasi-hasil-kelitbangan") }}/' + data + '/edit">' + row.activity_sub.name + '</a>';
            }
          },
          {
            "targets": 1,
            "data": "activity_proposal.id",
            "render": (data, type, row, meta) => {
              return '<a class="text-gray-500 hover:text-primary focus:text-primary underline" href="{{ url("kegiatan-kelitbangan") }}/redirect/' + data + '" >' + row.activity_proposal.type_proposal_title + '</a>';
            }
          },
          {
            "targets": 5,
            orderable: false,
            "data": "id",
            "render": (data, type, row, meta) => {
              if (data) {
                if (row['rtl']['recommendation_implementation'] == 'ditindaklanjuti') {
                  return `
                    <div class="whitespace-nowrap">
                      <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="Ditindak Lanjuti" type="button" />
                    </div>
                  `;
                } else {
                  return `
                    <div class="whitespace-nowrap">
                      <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="Tidak ditindak lanjuti" type="button" />
                    </div>
                  `;
                }
              } else {
                return `
                  <div class="whitespace-nowrap">
                    <x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="Belum dibuat" type="button" />
                  </div>
                `;
              }
            }
          },
          {
            "targets": 6,
            "data": "id",
            "render": (data, type, row, meta) => {
              if (data) {
                var hasilRekomendasiEdit = "{{ url('rtl') }}/" + row.id + "/edit";

                return `
                  <x-ui.form.dropdown align="drop-left">
                    <x-slot name="trigger">
                      <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                        <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                      </button> 
                    </x-slot>

                    <x-slot name="content">
                      <div class="py-2.5 px-5 pr-8">
                        <x-ui.form.dropdown-link href="` + hasilRekomendasiEdit + `">
                          <x-heroicon-s-document-text class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Rencana Tindak Lanjut') }}
                        </x-ui.form.dropdown-link>
                      </div>
                    </x-slot>
                  </x-ui.form.dropdown>
                `;
              } else {
                var hasilRekomendasiEdit = "{{ url('data-penelitian/rekomendasi-hasil-kelitbangan') }}/" + row.id + "/edit";
                var hasilRekomendasiDetail = "{{ url('data-penelitian/rekomendasi-hasil-kelitbangan') }}/" + row.id + "/detail";

                return `
                  <x-ui.form.dropdown align="drop-left">
                    <x-slot name="trigger">
                      <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                        <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                      </button> 
                    </x-slot>

                    <x-slot name="content">
                      <div class="py-2.5 px-5 pr-8">
                        <x-ui.form.dropdown-link href="` + hasilRekomendasiEdit + `">
                          <x-heroicon-s-document-text class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Rencana Tindak Lanjut') }}
                        </x-ui.form.dropdown-link>
                        @if(Auth::user()->role->name!='perangkat')
                        <x-ui.form.dropdown-link href="` + hasilRekomendasiDetail + `">
                          <x-heroicon-s-pencil class="fill-current mr-2 h-[18px] w-[18px]" />
                          {{ __('Ubah Rekomendasi hasil') }}
                        </x-ui.form.dropdown-link>
                        @endif
                      </div>
                    </x-slot>
                  </x-ui.form.dropdown>
                `;
              }
            }
          },
        ]
      });
    });
  </script>
</x-app-layout>