<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Survei') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('survey.index') }}">{{ __('Survei') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Edit') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Edit Data : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ __('Survei') }}</p>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('survey.update',$survey->id) }}">
          @csrf
          @method('PUT')

          <div class="grid grid-cols-12 gap-5 select-none">
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="is_active" title="Status" />
                <select id="is_active" class="custom-select custom-select2 rounded-lg shadow-none" name="is_active" required>
                  <option selected>Pilih Status Survei : </option>
                  <option value="1" @if ($survey->is_active=='1')
                    selected="selected"
                    @endif>Aktif</option>
                  <option value="0" @if ($survey->is_active=='0')
                    selected="selected"
                    @endif>Tidak Aktif</option>
                </select>
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="title" title="Judul Survey" />
                <x-ui.form.input id="title" type="text" name="title" size="md" parent="" maxlength="120" aria-placeholder="Masukan judul survey" placeholder="Masukan judul survey" value="{{ $survey->title }}" required />
              </div>
            </div>
            <div class="col-span-12 mt-8 mb-6">
              <div class="relative">
                <x-ui.form.label for="question" title="Pertanyaan" />
                <table id="tableDetailEditSurvey" class="table-data whitespace-nowrap xl:whitespace-normal mt-5">
                  <thead>
                    <tr>
                      <td scope="col" class="first text-left" width="0%">No</td>
                      <td scope="col" width="0%">Kategori</td>
                      <td scope="col">Pertanyaan</td>
                      <td scope="col" class="last" width="2%"></td>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach ($questions as $question)
                    <tr>
                      <td class="first text-center">{{ $question->order }}.</td>
                      <td>
                        <div class="flex whitespace-nowrap">
                          {{ $question->category }}
                        </div>
                      </td>
                      <td>{{ $question->question }}</td>
                      <td>
                        @if(in_array($question->id,$questionId))
                        <x-ui.form.checkbox id="question_id{{ $question->id }}" name="question_id[]" value="{{ $question->id }}" title="" checked="checked" />
                        @else
                        <x-ui.form.checkbox id="question_id{{ $question->id }}" name="question_id[]" value="{{ $question->id }}" title="" />
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>

            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <!-- JavaScript (Edit Survey) -->
    <script>
      $(() => {
        $('#tableDetailCreateSurvey').DataTable({
          processing: true,
          scrollX: true,
          paging: false,
          order: [
            [0, 'asc']
          ],
          "columnDefs": [{
            "targets": [0, 2],
            orderable: false,
          }],
        });
      });
    </script>
</x-app-layout>