<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Hasil Saran & Masukan') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('user-input.create') }}">{{ __('Saran & Masukan') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">
              {{ __('Hasil') }}
            </li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Hasil dari survei : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ $survey->title }}</p>
    </div>
    <div class="flex flex-col relative">
      <div class="pt-8">
        <table id="tableHasilSurvei" class="table-data whitespace-nowrap xl:whitespace-normal mt-6">
          <thead>
            <tr>
              <td scope="col" class="first text-center" width="0%">No</td>
              <td scope="col" width="10%">Kategori</td>
              <td scope="col">Pertanyaan</td>
              <td scope="col" class="last text-center">Rata - Rata</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($questions as $question)
            <tr>
              <td scope=" col" class="text-center">{{ $question->question->order }}.</td>
              <td scope="col">
                <div class="flex whitespace-nowrap">
                  {{ $question->question->category }}
                </div>
              </td>
              <td scope="col" class="">
                <div class="flex whitespace-nowrap sm:whitespace-normal">
                  {{ $question->question->question }}
                </div>
              </td>
              <td class="last text-center">
                {{ round($question->score,2) }}
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="mt-14">
          <div class="flex items-center justify-between pb-4 mb-2">
            <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Responden</h3>
          </div>
          <table id="tableRespondenHasilSurvei" class="table-data whitespace-nowrap xl:whitespace-normal mt-6">
            <thead>
              <tr>
                <td scope="col">Nama Responden</td>
                <td scope="col" width="32%">Penerima Manfaat dari balitbangda</td>
              </tr>
            </thead>
            <tbody>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <!-- JavaScript (Hasil Saran&Masukkan) -->
  <script>
    $(() => {
      $('#tableHasilSurvei').DataTable({
        processing: true,
        scrollX: true,
        paging: false,
        order: [
          [0, 'asc']
        ],
        "columnDefs": [{
          "targets": [0, 3],
          orderable: false,
        }],
      });

      $('#tableRespondenHasilSurvei').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ url("user-input/data/".$survey->id) }}',
        columns: [{
            "data": "user.name",
            "className": "first text-left"
          },
          {
            "data": "benefit_recipients",
            "className": "text-right sm:text-left"
          },
        ],
        order: [
          [0, 'asc']
        ],
        "columnDefs": [{
          "targets": 1,
          orderable: false,
          "render": (data, type, row, meta) => {
            if (data == '1') {
              return `
                <div class="whitespace-nowrap">
                  <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="Ya" type="button" />
                </div>
              `;
            } else {
              return `
                <div class="whitespace-nowrap">
                  <x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="Tidak" type="button" />
                </div>
              `;
            }
          }
        }, ]
      });
    });
  </script>
</x-app-layout>