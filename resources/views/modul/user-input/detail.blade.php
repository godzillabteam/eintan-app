<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ $survey->title }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">
              {{ __('Saran & Masukan') }}
            </li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Nama Responden : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ Auth::user()->name }} ({{ Auth::user()->role->name }})</p>
    </div>
    <div class="flex flex-col relative">

      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      <br>
      @endif

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <div class="bg-blue-100 rounded-lg text-blue-600 font-inter tracking-tight relative py-4 pl-5 pr-10 mb-6 duration-300 ease-in-out" role="alert">
          <h3 class="text-base font-medium mb-5">Skala Jawaban Skor</h3>
          <ul class="flex flex-col list-none pl-0">
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">1</h3>
              <p class="">Sangat Tidak Baik/Kurang Baik</p>
            </li>
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">2</h3>
              <p class="">Cukup Baik</p>
            </li>
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">3</h3>
              <p class="">Baik</p>
            </li>
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">4</h3>
              <p class="">Sangat Baik</p>
            </li>
          </ul>
        </div>

        <div class="flex items-center relative mb-4">
          <x-ui.form.label for="text-form-control" title="Penerima Manfaat dari balitbangda :" />
          <div class="ml-3 mb-1.5">
            <strong>@if($user_input->benefit_recipients==1) Ya @else Tidak @endif</strong>
          </div>
        </div>

        <table id="tableDetailSaranMasukkan" class="table-data whitespace-nowrap xl:whitespace-normal mt-6">
          <thead>
            <tr>
              <td scope="col" class="first text-center" width="0%">No</td>
              <td scope="col" width="10%">Kategori</td>
              <td scope="col">Pertanyaan</td>
              <td scope="col" class="text-center" width="0%">Skor</td>
            </tr>
          </thead>
          <tbody>
            @foreach ($questions as $question)
            <tr>
              <td scope=" col" class="text-center">{{ $question->question->order }}.</td>
              <td scope="col">
                <div class="flex whitespace-nowrap">
                  {{ $question->question->category }}
                </div>
              </td>
              <td scope="col" class="">
                <div class="flex whitespace-nowrap sm:whitespace-normal">
                  {{ $question->question->question }}
                </div>
              </td>
              <td class="text-center">
                @foreach ($user_input->user_input_question as $user_input_question)
                @if($user_input_question->question_id==$question->question->id) {{ $user_input_question->score }} @endif
                @endforeach
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- JavaScript (Detail Saran&Masukkan) -->
  <script>
    $(() => {
      $('#tableDetailSaranMasukkan').DataTable({
        processing: true,
        scrollX: true,
        paging: false,
        order: [
          [0, 'asc']
        ],
      });
    });
  </script>
</x-app-layout>