<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ $survey->title }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">
              {{ __('Saran & Masukan') }}
            </li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Nama Responden : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ Auth::user()->name }} ({{ Auth::user()->role->name }})</p>
    </div>
    <div class="flex flex-col relative">

      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      <br>
      @endif

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <div class="bg-blue-100 rounded-lg text-blue-600 font-inter tracking-tight relative py-4 pl-5 pr-10 mb-6 duration-300 ease-in-out" role="alert">
          <h3 class="text-base font-medium mb-5">Skala Jawaban Skor</h3>
          <ul class="flex flex-col list-none pl-0">
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">1</h3>
              <p class="">Sangat Tidak Baik/Kurang Baik</p>
            </li>
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">2</h3>
              <p class="">Cukup Baik</p>
            </li>
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">3</h3>
              <p class="">Baik</p>
            </li>
            <li class="flex items-center text-sm font-normal mb-3 sm:mb-1 last:mb-0">
              <h3 class="mr-4">4</h3>
              <p class="">Sangat Baik</p>
            </li>
          </ul>
        </div>

        <form action="{{ route('user-input.store') }}" method="POST" enctype="multipart/form-data">
          <input type="hidden" name="survey_id" id="" value={{ $survey->id }}>
          @csrf

          <div class="relative mb-8">
            <x-ui.form.label for="benefit_recipients" title="Penerima Manfaat dari balitbangda : " />
            <div class="inline-flex flex-wrap">
              <div class="relative mr-5">
                <x-ui.form.radio id="radioInline1" checked="checked" name="benefit_recipients" value="true" for="radio-inline1" title="Ya" />
              </div>
              <div class="relative">
                <x-ui.form.radio id="radioInline2" name="benefit_recipients" value="false" for="radio-inline2" title="Tidak" />
              </div>
            </div>
          </div>

          <table id="tableCreateSaranMasukkan" class="table-data whitespace-nowrap xl:whitespace-normal mt-6">
            <thead>
              <tr>
                <td scope="col" class="first text-center" width="0%" rowspan="2">No</td>
                <td scope="col" width="10%" rowspan="2">Kategori</td>
                <td scope="col" rowspan="2">Pertanyaan</td>
                <td scope="col" class="text-center" colspan="4">Jawaban Skor</td>
              </tr>
              <tr>
                <td scope="col" class="last text-center" width="0%">4</td>
                <td scope="col" class="last text-center" width="0%">3</td>
                <td scope="col" class="last text-center" width="0%">2</td>
                <td scope="col" class="text-center" width="0%">1</td>
              </tr>
            </thead>
            <tbody>
              @foreach ($questions as $question)
              <tr>
                <td scope="col" class="text-center">{{ $question->question->order }}.</td>
                <td scope="col">
                  <div class="flex whitespace-nowrap">
                    {{ $question->question->category }}
                  </div>
                </td>
                <td scope="col" class="">{{ $question->question->question }}</td>
                <td class="text-center">
                  <x-ui.form.radio id="radioInline{{ $question->question->id }}" name="qustion_id[{{$question->question->id}}]" value="4" title="4" />
                </td>
                <td class="text-center">
                  <x-ui.form.radio id="radioInline{{ $question->question->id }}" name="qustion_id[{{$question->question->id}}]" value="3" title="3" />
                </td>
                <td class="text-center">
                  <x-ui.form.radio id="radioInline{{ $question->question->id }}" name="qustion_id[{{$question->question->id}}]" value="2" title="2" />
                </td>
                <td class="text-center">
                  <x-ui.form.radio id="radioInline{{ $question->question->id }}" name="qustion_id[{{$question->question->id}}]" value="1" title="1" />
                </td>
              </tr>
              @endforeach
            </tbody>
          </table>

          <div class="relative mt-8">
            <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- JavaScript (Create Saran&Masukkan) -->
  <script>
    $(() => {
      $('#tableCreateSaranMasukkan').DataTable({
        processing: true,
        scrollX: true,
        paging: false,
        order: [
          [0, 'asc']
        ],
      });
    });
  </script>
</x-app-layout>