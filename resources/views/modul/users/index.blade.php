<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Pengguna Aplikasi') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Pengguna Aplikasi') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Data Pengguna</h3>
      <a href="{{ route('user.create') }}">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Tambah Data" type="button" />
      </a>
    </div>
    <div class="flex flex-col relative w-full">
      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      @endif

      <div class="pt-8 overflow-hidden">
        <table id="myTables" class="table-data whitespace-nowrap lg:whitespace-normal">
          <thead>
            <tr>
              <td scope="col" width="22%">Name</td>
              <td scope="col">Email</td>
              <td scope="col" width="18%">No.Telp</td>
              <td scope="col" width="8%">Role</td>
              <td scope="col" width="2%"></td>
            </tr>
          </thead>
          <tbody>
            <!-- Isi Tabel -->
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- JavaScript (Pengguna Aplikasi) -->
  <script>
    $(() => {
      $('#myTables').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ url("master/user/data") }}',
        columns: [{
            "data": "name",
            "className": "first text-left"
          },
          {
            "data": "email"
          },
          {
            "data": "phone"
          },
          {
            "data": "role.name"
          },
          {
            "data": "id",
            "className": "last"
          },
        ],
        "columnDefs": [{
            "targets": 0,
            "data": "activity_proposal.type_proposal_title",
            "render": (data, type, row, meta) => {
              var pengAplikasi_Edit = "{{ url('master/user') }}" + "/" + row.id + "/edit";

              return '<a class="text-gray-600 hover:text-primary underline duration-300 ease-in-out" href="' + pengAplikasi_Edit + '">' + data + '</a>';
            }
          },
          {
            "targets": 3,
            orderable: false,
            "data": "role.name",
            "render": (data, type, row, meta) => {
              if (data == 'bidang') {
                return `<x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="${ data }" type="button" />`
              } else if (data == 'admin') {
                return `<x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="${ data }" type="button" />`
              } else {
                return `<x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="${ data }" type="button" />`
              }
            }
          },
          {
            "targets": 4,
            "data": "id",
            "render": (data, type, row, meta) => {
              var pengAplikasi_Edit = "{{ url('master/user') }}" + "/" + row.id + "/edit";
              var pengAplikasi_Delete = "{{ url('master/user') }}" + "/" + row.id + "/delete";

              return `
                <x-ui.form.dropdown align="drop-left">
                  <x-slot name="trigger">
                    <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                      <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                    </button> 
                  </x-slot>

                  <x-slot name="content">
                    <div class="py-2.5 px-5 pr-8">
                      <x-ui.form.dropdown-link href="` + pengAplikasi_Edit + `">
                        <x-heroicon-s-pencil-alt class="fill-current mr-2 h-[18px] w-[18px]" />
                        {{ __('Perbarui') }}
                      </x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link class="text-danger hover:text-danger" href="javascript:void(0);" onclick="return deleteUserAplikasi('${pengAplikasi_Delete}')">
                        <x-heroicon-s-trash class="fill-current mr-2 h-[18px] w-[18px]" />
                        {{ __('Hapus') }}
                      </x-ui.form.dropdown-link>
                    </div>
                  </x-slot>
                </x-ui.form.dropdown>
              `;
            }
          },
        ],
      });
    });

    // Alert Delete {{ url("master/user/") }}/delete
    let deleteUserAplikasi = (link) => {
      swal({
        title: "Apakah Anda sudah yakin?",
        text: "Setelah dihapus, Anda tidak dapat memulihkan file ini!",
        icon: "warning",
        buttons: ["Batal", "Hapus"],
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          // swal("File anda telah berhasil dihapus!", {
          //   icon: "success",
          //   button: {
          //     text: "Oke",
          //   },
          // });

          $.ajax({
            type: 'GET',
            url: link,
            // data: data,
            success: (data) => {
              swal("Data anda telah berhasil dihapus!", {
                icon: "success",
                button: {
                  text: "Oke",
                },
              });

              setTimeout(() => {
                location.reload()
              }, 2000);
            },
            error: (data) => {
              swal("Data anda tidak berhasil dihapus! ada masalah pada server", {
                icon: "warning",
                button: {
                  text: "Oke",
                },
              });
            }
          });
        }
      });
    };
  </script>
</x-app-layout>