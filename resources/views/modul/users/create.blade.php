<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Pengguna Aplikasi') }}</h4>
    </div>
    <div class="flex items-center">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex items-center text-gray-200 font-light leading-5">
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="w-min">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('user.index') }}">{{ __('Pengguna Aplikasi') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Tambah') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Tambah Data : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ __('Pengguna Aplikasi') }}</p>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('user.store') }}">
          @csrf

          <div class="grid grid-cols-12 gap-5 select-none">
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="role_id" title="Peran" />
                <select id="role_id" class="bg-white border border-gray-300 focus:border-primary focus:ring-0 placeholder-gray-400 rounded-lg font-inter text-sm capitalize tracking-tight outline-none py-3.5 px-4 w-full appearance-none duration-300 ease-in-out" name="role_id" required>
                  @foreach ($roles as $role)
                  <option value="{{ $role->id }}">{{ $role->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="name" title="Nama" />
                <x-ui.form.input id="name" type="text" name="name" size="md" parent="" maxlength="120" aria-placeholder="Masukan nama pengguna aplikasi" placeholder="Masukan nama pengguna aplikasi" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="email" title="Email" />
                <x-ui.form.input id="email" type="email" name="email" size="md" parent="" maxlength="120" aria-placeholder="Masukan email yang masih aktif" placeholder="Masukan email yang masih aktif" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="phone" title="No. Telepon" />
                <x-ui.form.input id="phone" type="number" name="phone" size="md" parent="" maxlength="13" aria-placeholder="Masukan no.telp yang bisa dihubungi" placeholder="Masukan no.telp yang bisa dihubungi" required />
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="password" title="Password" />

                <div class="text-gray-300 focus-within:text-primary relative">
                  <x-ui.form.input id="password" type="password" name="password" size="md" parent="icons-r-md" maxlength="120" aria-placeholder="Masukan password pengguna aplikasi" placeholder="Masukan password pengguna aplikasi" required />
                  <span class="toggle-passwrod bg-transparent flex items-center justify-center absolute top-0 right-0 bottom-0 w-14" data-toggle="#password" data-size="md">
                    <x-heroicon-s-eye-off class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
                  </span>
                </div>
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="password_confirmation" title="Password Confirm" />

                <div class="text-gray-300 focus-within:text-primary relative">
                  <x-ui.form.input id="password_confirmation" type="password" name="password_confirmation" size="md" parent="icons-r-md" maxlength="120" aria-placeholder="Ulangi password pengguna aplikasi" placeholder="Ulangi password pengguna aplikasi" required />
                  <span class="toggle-passwrod bg-transparent flex items-center justify-center absolute top-0 right-0 bottom-0 w-14" data-toggle="#password_confirmation" data-size="md">
                    <x-heroicon-s-eye-off class="fill-current h-[22px] w-[22px] duration-300 ease-in-out" />
                  </span>
                </div>
              </div>
            </div>
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.label for="address" title="Alamat" />
                <x-ui.form.textarea id="address" type="text" name="address" size="md" rows="5" maxlength="255" aria-placeholder="Massukan alamat tempat tinggal anda" placeholder="Massukan alamat tempat tinggal anda" value="" message="" required />
              </div>
            </div>
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
</x-app-layout>