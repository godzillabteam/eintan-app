<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Pertanyaan') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('question.index') }}">{{ __('Pertanyaan') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Tambah') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Tambah Data : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ __('Pertanyaan') }}</p>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('question.store') }}">
          @csrf

          <div class="grid grid-cols-12 gap-5 select-none">
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="category" title="Kategori Pertanyaan" />
                <select id="category" class="custom-select custom-select2 rounded-lg shadow-none" name="category" required>
                  <option selected>Pilih Kategori Pertanyaan : </option>
                  <option value="Layanan Umum">Layanan Umum</option>
                  <option value="Layanan Teknis">Layanan Teknis</option>
                  <option value="Kemampuan SDM Petugas">Kemampuan SDM Petugas</option>
                </select>
              </div>
            </div>
            <div class="col-span-12 sm:col-span-6">
              <div class="relative">
                <x-ui.form.label for="order" title="Nomor Urut Pertanyaan" />
                <x-ui.form.input id="order" type="number" name="order" size="md" parent="" maxlength="120" aria-placeholder="Masukan urutan pertanyaan" placeholder="Masukan urutan pertanyaan" required />
              </div>
            </div>

            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.label for="question" title="Pertanyaan" />
                <x-ui.form.textarea id="question" type="text" name="question" size="md" rows="5" maxlength="255" aria-placeholder="Masukan Pertanyaan" placeholder="Masukan Pertanyaan" value="" message="" required />
              </div>
            </div>
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
</x-app-layout>