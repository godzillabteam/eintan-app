<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Pertanyaan') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Pertanyaan') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">{{ __('Data Pertanyaan') }}</h3>
      <a href="{{ route('question.create') }}">
        <x-ui.form.button background="primary" variant="default" size="base" rounded="base" title="Tambah Data" type="button" />
      </a>
    </div>
    <div class="flex flex-col relative w-full">
      @if ($message = Session::get('success'))
      <div class="bg-green-100 rounded-lg leading-normal text-green-700 font-inter tracking-tight relative py-3 pl-4 pr-10 duration-300 ease-in-out" role="alert">
        <p>{{ $message }}</p>
      </div>
      @endif

      <div class="pt-8 overflow-hidden">
        <table id="tableQuestion" class="table-data whitespace-nowrap xl:whitespace-normal">
          <thead>
            <tr>
              <td scope="col" width="0%">No</td>
              <td scope="col">Pertanyaan</td>
              <td scope="col" width="0%">Kategori</td>
              <td scope="col" width="0%"></td>
            </tr>
          </thead>
          <tbody>

          </tbody>
        </table>
      </div>
    </div>
  </div>

  <!-- JavaScript (Pertanyaan) -->
  <script>
    $(() => {
      $('#tableQuestion').DataTable({
        processing: true,
        serverSide: true,
        scrollX: true,
        ajax: '{{ url("master/question/data") }}',
        columns: [{
            "data": "order",
            "className": "first text-center"
          },
          {
            "data": "question"
          },
          {
            "data": "category"
          },
          {
            "data": "id",
            "className": "last"
          },
        ],
        order: [
          [1, 'asc']
        ],
        "columnDefs": [{
            "targets": 0,
            orderable: false,
          },
          {
            "targets": 2,
            orderable: false,
            "render": (data, type, row, meta) => {
              if (data == 'Layanan Umum') {
                return `
                  <div class="flex whitespace-nowrap">
                    <x-ui.form.button background="status-secondary" variant="status" size="base" rounded="pill" title="${ data }" type="button" />
                  </div>
                `;
              } else if (data == 'Layanan Teknis') {
                return `
                  <div class="flex whitespace-nowrap">
                    <x-ui.form.button background="status-danger" variant="status" size="base" rounded="pill" title="${ data }" type="button" />
                  </div>
                `;
              } else {
                return `
                  <div class="flex whitespace-nowrap">
                    <x-ui.form.button background="status-success" variant="status" size="base" rounded="pill" title="${ data }" type="button" />
                  </div>
                `;
              }
            }
          },
          {
            "targets": 3,
            "data": "id",
            "render": (data, type, row, meta) => {
              var questionEdit = "{{ url('master/question') }}" + "/" + data + "/edit";
              var questionDelete = "{{ url('master/question') }}/" + data + "/delete";

              return `
                <x-ui.form.dropdown align="drop-left">
                  <x-slot name="trigger">
                    <button class="hover:text-green flex flex-col bg-transparent border-0 outline-none py-[9px] px-0 ml-3 mr-3 w-auto" type="button">
                      <x-heroicon-o-dots-horizontal class="fill-current h-5 w-5 duration-300 ease-in-out" />
                    </button> 
                  </x-slot>

                  <x-slot name="content">
                    <div class="py-2.5 px-5 pr-8">
                      <x-ui.form.dropdown-link href="` + questionEdit + `">
                        <x-heroicon-s-pencil-alt class="fill-current mr-2 h-[18px] w-[18px]" />
                        {{ __('Perbarui') }}
                      </x-ui.form.dropdown-link>
                      <x-ui.form.dropdown-link class="text-danger hover:text-danger focus:text-danger" href="javascript:void(0);" onclick="return questionDelete('` + questionDelete + `')">
                        <x-heroicon-s-trash class="fill-current mr-2 h-[18px] w-[18px]" />
                        {{ __('Hapus') }}
                      </x-ui.form.dropdown-link>
                    </div>
                  </x-slot>
                </x-ui.form.dropdown>
              `;
            }
          },
        ]
      });
    });

    // Alert Delete (Pertanyaan)
    let questionDelete = (getURL) => {
      swal({
        title: "Apakah Anda sudah yakin?",
        text: "Setelah dihapus, Anda tidak dapat memulihkan Usulan Kegiatan ini!",
        icon: "warning",
        buttons: ["Batal", "Hapus"],
        dangerMode: true,
      }).then((willDelete) => {
        if (willDelete) {
          // swal("File anda telah berhasil dihapus!", {
          //   icon: "success",
          //   button: {
          //     text: "Oke",
          //   },
          // });

          $.ajax({
            type: 'GET',
            url: getURL,
            // data: data,
            success: (data) => {
              swal("File anda telah berhasil dihapus!", {
                icon: "success",
                button: {
                  text: "Oke",
                },
              });
              setTimeout(() => {
                location.reload()
              }, 2000);
            },
            error: (data) => {
              swal("File anda tidak berhasil dihapus! ada masalah pada server", {
                icon: "warning",
                button: {
                  text: "Oke",
                },
              });
            }
          });
        }
      });
    };
  </script>
</x-app-layout>