<x-app-layout>
  <x-slot name="header">
    <div class="mb-6 sm:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Rencana Tindak Lanjut') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Rencana Tindak Lanjut') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Proses : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ __('Rencana Tindak Lanjut') }}</p>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('rtl.store') }}" enctype="multipart/form-data">
          @csrf

          <input type="hidden" name="rnd_result_id" value="{{ $rnd_result->id }}" id="">

          <div class="flex flex-col w-full">
            <div class="relative mb-4">
              <x-ui.form.label for="uraian_rekomendasi" title="Uraian Rekomendasi" />
              <strong>{{ $rnd_result->recommendation_description }}</strong>
            </div>
            <div class="relative mb-4">
              <x-ui.form.label for="tahun_rekomendasi" title="Tahun Rekomendasi" />
              <strong>{{ $rnd_result->activity_proposal->study_abstract->year }}</strong>
            </div>
            <div class="relative mb-4">
              <x-ui.form.label for="kegiatan_sub" title="Kegiatan/Sub Kegiatan" />
              <strong>{{ $rnd_result->activity_sub->name }}</strong>
            </div>
            <div class="relative mb-4">
              <x-ui.form.label for="pelaksanaan_rekomendasi" title="Pelaksanaan Rekomendasi" />

              <div class="inline-flex flex-wrap mb-8">
                <div class="relative mr-5">
                  <x-ui.form.radio id="radioInline1" click="checkCB()" checked="checked" name="recommendation_implementation" value="tidak ditindaklanjuti" for="radio-inline1" title="Tidak ditindak lanjuti,  alasan ………." required />
                </div>
                <div class="relative">
                  <x-ui.form.radio id="radioInline2" click="checkCB()" name="recommendation_implementation" value="ditindaklanjuti" for="radio-inline2" title=" Ditindaklanjuti" required />
                </div>
              </div>

              <div class="relative mb-4" id="tidak-ditindaklanjuti">
                <x-ui.form.label for="text-form-control" title="Alasan " />
                <x-ui.form.textarea id="reason" type="text" name="reason" size="md" rows="5" maxlength="255" aria-placeholder="Alasan" placeholder="Alasan" value="" message=""  />
              </div>
            </div>

            <div id="ditindaklanjuti" class="mb-8">
              <div>
                <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Rencana Tindak lanjut</h3>
              </div>
              <div class="grid grid-cols-12 gap-7 select-none">
                <div class="col-span-12 xl:col-span-6">
                  <div class="relative">
                    <x-ui.form.label for="text-form-control" title="Nama Kegiatan" />
                    <x-ui.form.input id="activity" type="text" name="activity" size="md" parent="" value="{{ $rnd_result->activity_sub->name}}" aria-placeholder="Nama Kegiatan" placeholder="Nama Kegiatan" />
                  </div>
                </div>
                <div class="col-span-12 xl:col-span-6">
                  <div class="relative">
                    <x-ui.form.label for="text-form-control" title="Tahun " />
                    <x-ui.form.input id="year" type="text" name="year" size="md" parent="" aria-placeholder="Tahun " placeholder="Tahun " value="{{ $rnd_result->activity_proposal->study_abstract->year }}" />
                  </div>
                </div>
                <div class="col-span-12">
                  <div class="relative">
                    <x-ui.form.label for="text-form-control" title="Photo" />
                    <x-ui.form.input id="output" type="file" name="photo" size="md" parent="" />
                  </div>
                </div>
              </div>
            </div>

            <div class="relative">
              <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- JavaScript () -->
  <script>
    let checkCB = () => {
      var radios = document.getElementsByName('recommendation_implementation');

      for (var i = 0, length = radios.length; i < length; i++) {
        if (radios[i].checked) {
          if (radios[i].value != "tidak") {
            $("#ditindaklanjuti").show();
            $("#tidak-ditindaklanjuti").hide();
          } else {
            $("#ditindaklanjuti").hide();
            $("#tidak-ditindaklanjuti").show();
          }

          break;
        }
      }
    }

    checkCB();
  </script>
</x-app-layout>