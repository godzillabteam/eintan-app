<x-app-layout>
  <x-slot name="header">
    <div class="mb-4 lg:mb-0">
      <h4 class="text-black font-poppins text-2xl font-semibold tracking-tight antialiased">{{ __('Sub Aktivitas') }}</h4>
    </div>
    <div class="flex items-center justify-between">
      <div class="flex whitespace-nowrap sm:whitespace-[none] overflow-x-auto sm:overflow-hidden pb-3 sm:pb-0 xl:mr-6 w-full">
        <nav class="" aria-label="breadcrumb">
          <ul class="flex text-gray-200 font-light leading-none">
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('dashboard') }}">{{ __('Dashboard') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="">
              <a class="text-primary font-normal outline-none duration-300 ease-in-out" href="{{ route('activity-sub.index') }}">{{ __('Sub Aktivitas') }}</a>
            </li>
            <li class="mx-2">/</li>
            <li class="text-gray-400 font-normal outline-none duration-300 ease-in-out" aria-current="page">{{ __('Edit') }}</li>
          </ul>
        </nav>
      </div>
      @include('layouts.header.action')
    </div>
  </x-slot>

  <div class="bg-white flex flex-col rounded pt-5 px-[22px] pb-8">
    <div class="flex items-center justify-between pb-4 mb-2">
      <h3 class="text-black font-inter text-lg font-semibold capitalize tracking-tight">Edit Data : </h3>
      <p class="text-gray-400 font-inter text-base font-normal capitalize tracking-tight">{{ __('Sub Aktivitas') }}</p>
    </div>
    <div class="flex flex-col relative">

      @if ($errors->any())
      <div class="bg-red-100 rounded-lg text-red-600 font-inter tracking-tight relative py-4 pl-5 pr-10 duration-300 ease-in-out" role="alert">
        <h3 class="text-base font-medium mb-5"><strong>Ups!</strong>, Ada beberapa data yang belum dilengkapi...</h3>
        <ul class="flex flex-col list-none pl-0">
          @foreach ($errors->all() as $error)
          <li class="text-sm font-normal mb-3 sm:mb-0 last:mb-0">{{ $error }}</li>
          @endforeach
        </ul>
      </div>
      @endif

      <div class="pt-8">
        <form method="POST" action="{{ route('activity.update',$activity_sub_master->id) }}">
          @csrf
          @method('PUT')

          <div class="grid grid-cols-12 gap-5 select-none">
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.label for="text-form-control" title="Aktivitas" />
                <select id="activity_master_id" class="custom-select custom-select2  rounded-lg shadow-none" name="activity_master_id" required>
                  <option selected>Pilih Aktivitas : </option>
                  @foreach ($activityMasters as $activityMaster)
                  <option value="{{ $activityMaster->id }}" @if ($activityMaster->id==$activity_sub_master->activity_master_id)
                    selected="selected"
                    @endif>{{ $activityMaster->name }}</option>
                  @endforeach
                </select>
              </div>
            </div>
            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.label for="name" title="Sub Aktivitas" />
                <x-ui.form.textarea id="name" type="text" name="name" size="md" rows="5" maxlength="255" aria-placeholder="Masukan sub aktivitas kegiatan yang dilakukan" placeholder="Masukan sub aktivitas kegiatan yang dilakukan" message="{{ $activity_sub_master->name }}" required />
              </div>
            </div>

            <div class="col-span-12">
              <div class="relative">
                <x-ui.form.button background="primary" variant="default" size="md" rounded="base" title="Simpan" icon-l="heroicon-o-save" icon-type="fill" type="submit" />
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
</x-app-layout>