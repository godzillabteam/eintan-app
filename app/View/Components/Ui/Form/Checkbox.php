<?php

namespace App\View\Components\Ui\Form;

use Illuminate\View\Component;

class Checkbox extends Component
{
    public $id;
    public $name;
    public $value;

    public $for;
    public $title;

    public $click;
    public $change;

    public $checked;
    public $disabled;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($id, $name, $value, $for = '', $title = '', $click = '', $change = '', $checked = '', $disabled = '')
    {
        $this->id = $id;
        $this->name = $name;
        $this->value = $value;

        $this->for = $for;
        $this->title = $title;

        $this->click = $click;
        $this->change = $change;

        $this->checked = $checked;
        $this->disabled = $disabled;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.form.checkbox');
    }
}
