<?php

namespace App\View\Components\Ui\Form;

use Illuminate\View\Component;

class Textarea extends Component
{
    public $size;
    public $message;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($size, $message)
    {
        $this->size = $size;
        $this->message = $message;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.form.textarea');
    }
}
