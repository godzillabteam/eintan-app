<?php

namespace App\View\Components\Ui\Form;

use Illuminate\View\Component;

class Button extends Component
{
    public $background;
    public $variant;
    public $size;
    public $rounded;
    public $title;
    public $iconL;
    public $iconR;
    public $iconType;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($background, $variant, $size, $rounded, $title, $iconL = '', $iconR = '', $iconType = '')
    {
        $this->background = $background;
        $this->variant = $variant;
        $this->size = $size;
        $this->rounded = $rounded;
        $this->title = $title;
        $this->iconL = $iconL;
        $this->iconR = $iconR;
        $this->iconType = $iconType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.form.button');
    }
}
