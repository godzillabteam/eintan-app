<?php

namespace App\View\Components\Ui;

use Illuminate\View\Component;

class Icons extends Component
{
    public $icon;
    public $iconType;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($icon = '', $iconType = '')
    {
        $this->icon = $icon;
        $this->iconType = $iconType;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ui.icons');
    }
}
