<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Type_proposal;
use Livewire\WithPagination;

class TypeProposals extends Component
{

    use WithPagination;
    
    public $name, $type_proposals, $type_proposal_id;
    public $isOpen = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function render()
    {
        $this->type_proposals = Type_proposal::all();
        return view('livewire.type-proposals');

    }
/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function create()
    {
        $this->resetInputFields();
        $this->openModal();

    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function openModal()
    {
        $this->isOpen = true;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function closeModal()
    {
        $this->isOpen = false;
    }
  
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    private function resetInputFields(){
        $this->name = '';
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function store()
    {
        $this->validate([
            'name' => 'required',
        ]);
   
        Type_proposal::updateOrCreate(['id' => $this->type_proposal_id], [
            'name' => $this->name
        ]);
  
        session()->flash('message', 
            $this->type_proposal_id ? 'Type proposal Updated Successfully.' : 'Type proposal Created Successfully.');
  
        $this->closeModal();
        $this->resetInputFields();
    }
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function edit($id)
    {
        $student = Type_proposal::findOrFail($id);
        $this->type_proposal_id = $id;
        $this->name = $student->name;
    
        $this->openModal();
    }
     
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public function delete($id)
    {
        Type_proposal::find($id)->delete();
        session()->flash('message', 'Type proposal Deleted Successfully.');
    }
}