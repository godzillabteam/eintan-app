<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    
    public function handle(Request $request, Closure $next, ...$admin)
    {
    

        if(in_array(Auth::user()->role->name,$admin)) { // if the current role is Administrator
            return $next($request);
        } else {
            abort(403, "Cannot access to restricted page");
        }  

        

        abort(403, "Cannot access to restricted page");
        
    }
}
