<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Datatables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
       
        return view('modul/users.index');
    }


    public function data()
    {
        $users = User::with('role');
        return Datatables::of($users)->make();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = Role::get();

        $dataGet = [
            'roles' => $role
        ];

        return view('modul/users.create', $dataGet);
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email|unique:users',
            'phone' => 'max:13',
            'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
            'password_confirmation' => 'min:6'
        ]);

        $insert = [
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=> Hash::make($request->password),
            'phone'=>$request->phone,
            'address'=>$request->address,
            'role_id'=>$request->role_id
        ];

        $users = User::create($insert);
        return redirect()->route('user.index')
        ->with('success','Pengguna Aplikasi berhasil dibuat.');

        
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $role = Role::get();
        $user = User::findOrFail($id);

        $dataGet = [
            'roles' => $role,
            'user' => $user,
        ];

        return view('modul/users.edit', $dataGet);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $this->validate($request, [
            'name' => 'required|min:3|max:50',
            'email' => 'email|unique:users,email,'.$id.',id',
            'phone' => 'max:13',
        ]);


        if($request->password!=""){
            $this->validate($request, [
                'password' => 'min:6|required_with:password_confirmation|same:password_confirmation',
                'password_confirmation' => 'min:6'
            ]);

            $update = [
                'name'=>$request->name,
                'email'=>$request->email,
                'password'=> Hash::make($request->password),
                'phone'=>$request->phone,
                'address'=>$request->address,
                'role_id'=>$request->role_id
            ];

            $users = User::find($id)->update($update); 
        }else{
            $update = [
                'name'=>$request->name,
                'email'=>$request->email,
                'phone'=>$request->phone,
                'address'=>$request->address,
                'role_id'=>$request->role_id
            ];
    
            $users = User::find($id)->update($update); 
            
        }

        return redirect()->route('user.index')
            ->with('success','Pengguna Aplikasi berhasil diperbaharui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        // print_r($id);
        $user = User::findOrFail($id)->delete();

        return redirect()->route('user.index')
                        ->with('success','Pengguna aplikasi berhasil dihapus.');

    }
}
