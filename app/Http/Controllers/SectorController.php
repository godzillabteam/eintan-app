<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Sector;
use Yajra\DataTables\Datatables;



class SectorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul/sector.index');

        //
    }

    public function data()
    {
        //
        $sectors = Sector::query();
        return Datatables::of($sectors)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modul/sector.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Masukan Bidang wajib diisi.'
            ],
        );
        Sector::create($request->except('_token'));
        return redirect()->route('sector.index')
                        ->with('success','Bidang berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $sectors = Sector::findOrFail($id);

        $dataGet = [
            'sector' => $sectors,
        ];

        return view('modul/sector.edit', $dataGet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate(
            $request, 
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Masukan Bidang wajib diisi.'
            ],
        );
        Sector::find($id)->update($request->except('_token'));
        return redirect()->route('sector.index')
                        ->with('success','Bidang berhasil diperbarui.');
                        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $sectors = Sector::findOrFail($id)->delete();

        return redirect()->route('sector.index')
                        ->with('success','Bidang berhasil dihapus.');
    }
}
