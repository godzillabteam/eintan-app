<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rnd_result;
use App\Models\Activity_proposal;
use App\Models\Role;
use App\Models\Sector;
use Yajra\DataTables\Datatables;
use Auth;


class RekomendasiHasilKelitbanganController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $rnd_results = Rnd_result::with('activity_proposal')->latest()->paginate(10);

        $dataGet = [
            'rnd_results' => $rnd_results
        ];
        return view('modul/data-penelitian.index-rekomendasi-hasil-kelitbangan', $dataGet);
    }


    public function data()
    {
        $rnd_results = Rnd_result::with('activity_proposal','activity_proposal.study_abstract','user','activity_sub','rtl');

        if(Auth::user()->role->name=="perangkat"){
            $rnd_results->whereIn('user_id',explode(',',Auth::user()->id));
        }else{
            // if($request->user_id){
            //     $rnd_results->whereIn('user_id',explode(',',$request->user_id));
            // }
        }

        return Datatables::of($rnd_results)->make();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $activityProposals = Activity_proposal::where('status','draft')->latest()->get();

        $roles = Role::has('user')->where('name','perangkat')->first();
        $sectors = Sector::latest()->get();
        $rnd_results = Rnd_result::findOrFail($id);
        $dataGet = [
            'rnd_results' => $rnd_results,
            'activityProposals' => $activityProposals,
            'users' => $roles->user,
            'sectors'=>$sectors

        ];
        return view('modul/data-penelitian.edit-rekomendasi-hasil', $dataGet);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rnd_result = Rnd_result::find($id);

        $dataGet = [
            'rnd_result' => $rnd_result
        ];
        return view('modul/rtl.create', $dataGet);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
