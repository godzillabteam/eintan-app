<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Models\Question;
use App\Models\Survey;
use App\Models\Survey_question;


class SurveyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('modul/survey.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $questions = Question::get();
        $dataGet = [
            'questions' => $questions,
        ];

        //
        return view('modul/survey.create',$dataGet);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(
            $request, 
            [
                'title' => 'required',
            ],
            [
                'title.required' => 'Masukan Judul wajib diisi.',
            ],
        );

        $surveyData = [
            'is_active'=>$request->is_active,
            'title'=>$request->title
        ];
      
        if($request->is_active==1){
            Survey::where('is_active',1)->update(['is_active'=>0]);
        }


        $survey = Survey::create($surveyData);
        foreach($request->question_id as $question_id){
            $surveyQuestionData = [
                'survey_id'=>$survey->id,
                'question_id'=>$question_id
            ];
            $survey_question = Survey_question::create($surveyQuestionData);
        }

       
        // Survey::create($request->except('_token'));

        return redirect()->route('survey.index')
                        ->with('success','Survei berhasil dibuat.');
    }

    public function data()
    {
        //
        $surveys = Survey::query();
        return Datatables::of($surveys)->make();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $survey = Survey::findOrFail($id);

        $questions = Question::get();

        $questionId = [];
        foreach($survey->survey_question as $survey_question){
            $questionId[] = $survey_question->question_id;
        }

        $dataGet = [
            'survey' => $survey,
            'questions' => $questions,
            'questionId' => $questionId,
        ];

        return view('modul/survey.edit', $dataGet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        //
        $this->validate(
            $request, 
            [
                'title' => 'required',
            ],
            [
                'title.required' => 'Masukan Judul wajib diisi.',
            ],
        );

        $surveyData = [
            'is_active'=>$request->is_active,
            'title'=>$request->title
        ];
      
        if($request->is_active==1){
            Survey::where('is_active',1)->update(['is_active'=>0]);
        }


        $survey  = Survey::find($id)->update($surveyData);
        
        Survey_question::where('survey_id',$id)->delete();

        foreach($request->question_id as $question_id){
            $surveyQuestionData = [
                'survey_id'=>$id,
                'question_id'=>$question_id
            ];
            $survey_question = Survey_question::create($surveyQuestionData);
        }

       
        // Survey::create($request->except('_token'));

        return redirect()->route('survey.index')
                        ->with('success','Survei berhasil diperbarui.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user = Survey::findOrFail($id)->delete();

        return redirect()->route('survey.index')
                        ->with('success','Survei berhasil dihapus.');
    }
}
