<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Models\Rtl;
use App\Models\Rnd_result;
use Auth;

class RtlController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('modul/rtl.index');

    }

    public function data()
    {
        $rnd_results = Rtl::with(['rnd_result' => function ($query) {
            return $query->where('user_id',Auth::user()->id);
        },'rnd_result','rnd_result.activity_proposal','rnd_result.activity_sub']);
        
        // if(Auth::user()->role->name=="perangkat"){
            // $rnd_results->whereHas('rnd_result', function ($query) {
            //     return $query->where('user_id',Auth::user()->id);
            // });
        // }

        return Datatables::of($rnd_results)->make();
        # code...
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if($request->recommendation_implementation=="ditindaklanjuti"){
            $this->validate($request, [
                'activity' => 'required',
                'year' => 'required',
                'photo' => 'required',
            ]);

            $file = $request->file('photo');

            $tujuan_upload = 'storage/rtl/'.$request->rnd_result_id.'/';
            $files = $file->move($tujuan_upload,$file->getClientOriginalName());

            $insert = [
                'rnd_result_id'=>$request->rnd_result_id,
                'recommendation_implementation'=>$request->recommendation_implementation,
                'activity'=>$request->activity,
                'year'=>$request->year,
                'photo'=>$file->getClientOriginalName(),
                'reason'=>''
            ];

        }else{
            $insert = [
                'rnd_result_id'=>$request->rnd_result_id,
                'recommendation_implementation'=>$request->recommendation_implementation,
                'activity'=>'',
                'year'=>'',
                'photo'=>'',
                'reason'=>$request->reason,
            ];
        }

        $cek = Rtl::where('rnd_result_id',$request->rnd_result_id)->first();
        if($cek){
            $cek->update($insert);
        }else{
            Rtl::insert($insert);
        }


        return redirect()->route('rtl.index')
            ->with('success','Rencana Tindak Lanjut berhasil diperbaharui.');


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $rtl = Rtl::with('rnd_result','rnd_result.activity_proposal','rnd_result.activity_sub')->where('id',$id)->first();
        $rnd_results = Rnd_result::where('id',$rtl->rnd_result_id)->first();
        $dataGet = ['rtl'=>$rtl,'rnd_result'=>$rnd_results];
        return view('modul/rtl.edit',$dataGet);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
