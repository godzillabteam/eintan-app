<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use App\Models\Study_abstract;
use App\Models\Activity_proposal;
use App\Models\Sector;
use Yajra\DataTables\Datatables;


class AbstraksiKajianController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       
        return view('modul/data-penelitian.index-abstraksi-kajian');
    }


    public function data()
    {
        $study_abstract = Study_abstract::with('activity_proposal','sector');
        return Datatables::of($study_abstract)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        //
        $study_abstract = Study_abstract::find($id);
        $sectors = Sector::latest()->get();

        $dataGet = [
            'study_abstract' => $study_abstract,
            'sectors'=>$sectors
        ];
         return view('modul/data-penelitian.edit-abstraksi-kajian', $dataGet);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
