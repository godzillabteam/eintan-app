<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\User_input;
use App\Models\User_input_question;
use App\Models\Question;
use App\Models\Survey;
use App\Models\Survey_question;
use Illuminate\Support\Facades\Auth;
use DB;
use Yajra\DataTables\Datatables;


class UserInputController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id=null)
    {
        //
        if($id==null){
            $survey = Survey::where('is_active',1)->first();
        }else{
            $survey = Survey::findOrFail($id);
        }

        if(!$survey){
            return abort(404);     
        }

        $question = Survey_question::where('survey_questions.survey_id',$survey->id)
        ->leftJoin('user_input_questions', 'survey_questions.question_id', '=', 'user_input_questions.question_id')
        ->leftJoin('user_inputs', 'user_input_questions.user_input_id', '=', 'user_inputs.id')
        ->select(DB::raw('survey_questions.* , (
            SELECT
                AVG( `user_input_questions`.`score` )
            FROM
                `user_input_questions` 
                LEFT JOIN user_inputs ON user_inputs.id = user_input_questions.user_input_id 
            WHERE
                `survey_questions`.`question_id` = `user_input_questions`.`question_id` 
                AND user_inputs.survey_id = survey_questions.survey_id 

        ) as score'))
        ->groupBy('survey_questions.id')
        ->get();
        $dataGet = [
            'survey' => $survey,
            'questions' => $question,
        ];
        return view('modul/user-input.index', $dataGet);

    }

    public function data($id)
    {
        $userInputs = User_input::with('user')->where('survey_id',$id);
        return Datatables::of($userInputs)->make();

    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $survey = Survey::where('is_active',1)->first();

        if(!$survey){
            abort(404, "Cannot access to restricted page");
            exit();
        }

        $user_input = User_input::where('user_id',Auth::user()->id)->where('survey_id',$survey->id)->first();


        if(!$user_input){
            $dataGet = [
                'survey' => $survey,
                'questions' => $survey->survey_question,
            ];
            return view('modul/user-input.create', $dataGet);
        }else{
            $dataGet = [
                'survey' => $survey,
                'questions' => $survey->survey_question,
                'user_input' => $user_input,
            ];
            return view('modul/user-input.detail', $dataGet);
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $question = User_input::where( 'user_id',Auth::user()->id)->where('survey_id',$request->survey_id)->first();

        $userInput = [
            'survey_id'=>$request->survey_id,
            'user_id'=>Auth::user()->id,
            'benefit_recipients'=> ($request->benefit_recipients=='true')? 1 : 0
        ];

        if($question){
            $question->update($userInput);
        }else{
            $question = User_input::create($userInput);
        }

        $user_input_id = $question->id;

        foreach($request->qustion_id as $question_id => $score){

            $userInputQuestion = [
                'question_id'=>$question_id,
                'user_input_id'=>$user_input_id,
                'score'=>$score,
            ];
            $question_input = User_input_question::where('question_id',$question_id)->where('user_input_id',$user_input_id)->first();
            if($question_input){
                $question_input->update($userInputQuestion);
            }else{
                $question_input = User_input_question::create($userInputQuestion);
            }
        }

        return redirect()->route('user-input.create')
        ->with('success','Saran & Masukan berhasil diisi.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
