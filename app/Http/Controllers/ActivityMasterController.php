<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity_master;
use Yajra\DataTables\Datatables;



class ActivityMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul/activity.index');

        //
    }

    public function data()
    {
        //
        $activity_masters = Activity_master::query();
        return Datatables::of($activity_masters)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modul/activity.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Masukan Aktivitas wajib diisi.'
            ],
        );
        Activity_master::create($request->except('_token'));
        return redirect()->route('activity.index')
                        ->with('success','Aktivitas berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $activity_master = Activity_master::findOrFail($id);

        $dataGet = [
            'activity_master' => $activity_master,
        ];

        return view('modul/activity.edit', $dataGet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate(
            $request, 
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Masukan Aktivitas wajib diisi.'
            ],
        );
        Activity_master::find($id)->update($request->except('_token'));
        return redirect()->route('activity.index')
                        ->with('success','Aktivitas berhasil diperbarui.');
                        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $activity_master = Activity_master::findOrFail($id)->delete();

        return redirect()->route('activity.index')
                        ->with('success','Aktivitas berhasil dihapus.');
    }
}
