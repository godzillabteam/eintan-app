<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\DataTables\Datatables;
use App\Models\Question;


class QuestionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul/question.index');

        //
    }

    public function data()
    {
        //
        $questions = Question::query();
        return Datatables::of($questions)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('modul/question.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'order' => 'required',
                'question' => 'required'
            ],
            [
                'order.required' => 'Masukan Nomor Urut wajib diisi.',
                'question.required' => 'Masukan Pertanyaan wajib diisi.'
            ],
        );
        Question::create($request->except('_token'));
        return redirect()->route('question.index')
                        ->with('success','Perntanyaan berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $question = Question::findOrFail($id);

        $dataGet = [
            'question' => $question,
        ];

        return view('modul/question.edit', $dataGet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate(
            $request, 
            [
                'order' => 'required',
                'question' => 'required'
            ],
            [
                'order.required' => 'Masukan Nomor Urut wajib diisi.',
                'question.required' => 'Masukan Pertanyaan wajib diisi.'
            ],
        );
        Question::find($id)->update($request->except('_token'));
        return redirect()->route('question.index')
                        ->with('success','Perntanyaan berhasil diperbarui.');
                        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $question = Question::findOrFail($id)->delete();

        return redirect()->route('question.index')
                        ->with('success','Pertanyaan berhasil dihapus.');
    }
}
