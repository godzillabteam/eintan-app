<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\Type_proposal;
use App\Models\Activity_proposal;
use App\Models\Activity_master;
use App\Models\Activity_sub_master;

use Yajra\DataTables\Datatables;
use DB;
use Auth;

class UsulanKegiatanController extends Controller
{
    //


    public function index(Request $request)
    {
        $role = Role::where('name','perangkat')->first();
        $data['users'] = User::where('role_id',$role->id)->get();
        $data['type_proposals'] = Type_proposal::get();
        $data['years'] = Activity_proposal::selectRaw("DISTINCT(proposed_year) as year")->get();

        
        $filter = "";
        if($request->filter_date_usulan){
            $data['date'] = $request->filter_date_usulan;
            if($filter=="") $filter .= '?date='.$request->filter_date_usulan;
            else $filter .= '&date='.$request->filter_date_usulan;
        }else{
            $data['date'] = "";
        }
        
        if($request->user_id){
            $data['user_id'] = $request->user_id;
            if($filter=="") $filter .= '?user_id='.implode(',',$request->user_id);
            else $filter .= '&user_id='.implode(',',$request->user_id);
        }else{
            $data['user_id'] = [];
        }

        if($request->proposed_year){
            $data['proposed_year'] = $request->proposed_year;
            if($filter=="") $filter .= '?proposed_year='.implode(',',$request->proposed_year);
            else $filter .= '&proposed_year='.implode(',',$request->proposed_year);
        }else{
            $data['proposed_year'] = [];
        }

        if($request->type_proposal_id){
            $data['type_proposal_id'] = $request->type_proposal_id;
            if($filter=="") $filter .= '?type_proposal_id='.implode(',',$request->type_proposal_id);
            else $filter .= '&type_proposal_id='.implode(',',$request->type_proposal_id);
        }else{
            $data['type_proposal_id'] = [];
        }

        $data['filter'] = $filter;

        return view('modul/usulan-kegiatan.index',$data);
    }

    public function data(Request $request)
    {

        $activityProposals = Activity_proposal::with('type_proposal','user');
        if($request->proposed_year){
            $activityProposals->whereIn('proposed_year',explode(',',$request->proposed_year));
        }

        if(Auth::user()->role->name=="perangkat"){
            $activityProposals->whereIn('user_id',explode(',',Auth::user()->id));
        }else{
            if($request->user_id){
                $activityProposals->whereIn('user_id',explode(',',$request->user_id));
            }
        }

        if($request->type_proposal_id){
            $activityProposals->whereIn('type_proposal_id',explode(',',$request->type_proposal_id));
        }

        return Datatables::of($activityProposals)->make();

    }

    public function jsonActivity(Request $request,$type,$id=null)
    {
        if($type=="activity"){
            $data = Activity_master::where('name', 'like', '%'.$request->q.'%')->get();
            $json = [];
            foreach($data as $rec){
                if($request->checked==$rec['id']){
                    $json[] = [
                        'id'=>$rec['id'],
                        'text'=>$rec['name'],
                        'selected'=>true
                    ];
                }else{
                    $json[] = [
                        'id'=>$rec['id'],
                        'text'=>$rec['name'],
                    ];
                }

            }
        }else{
            $data = Activity_sub_master::where('name', 'like', '%'.$request->q.'%')->where('activity_master_id',$id)->get();
            $json = [];
            foreach($data as $rec){
                if($request->checked==$rec['id']){
                    $json[] = [
                        'id'=>$rec['id'],
                        'text'=>$rec['name'],
                        'selected'=>true
                    ];
                }else{
                    $json[] = [
                        'id'=>$rec['id'],
                        'text'=>$rec['name'],
                    ];
                }
            }

        }

        $return['results']  = $json; 
        return response()->json($return);
    }

    public function create()
    {

        if(Auth::user()->role->name!='perangkat'){
            $roles = Role::with('user')->where('name','perangkat')->first();
            $user = $roles->user;
        }else{
            $user = User::where('id',Auth::user()->id)->get();
        }
        $typeProposal = Type_proposal::get();

        $dataGet = [
            'users' => $user,
            'typeProposals' => $typeProposal
        ];
        return view('modul/usulan-kegiatan.create', $dataGet);
    }


    public function store(Request $request)
    {

        $this->validate(
            $request, 
            [
                'type_proposal_title' => 'required',
                'problem' => 'required',
               
            ],
            [
                'type_proposal_title.required' => 'Masukan Judul wajib diisi.',
                'problem.required' => 'Masukan Permasalahan wajib diisi.',
                
            ],
        );
        Activity_proposal::create($request->all());
         
        return redirect()->route('usulanKegiatan.')
                        ->with('success','Usulan kegiatan berhasil dibuat.');
    }


    public function update(Request $request)
    {

        $this->validate(
            $request, 
            [
                'type_proposal_title' => 'required',
                'problem' => 'required'
            ],
            [
                'type_proposal_title.required' => 'Masukan Judul wajib diisi.',
                'problem.required' => 'Masukan Permasalahan wajib diisi.'
            ],
        );
        Activity_proposal::where('id',$request->id)->update($request->except('_token'));
         
        return redirect()->route('usulanKegiatan.')
                        ->with('success','Usulan kegiatan berhasil diperbaharui.');
    }

    public function delete($id)
    {
        $activityProposal = Activity_proposal::with('type_proposal')->find($id);
        if(!$activityProposal)  return redirect()->route('usulanKegiatan.')
        ->with('danger','Usulan kegiatan tidak ditemukan.');

        Activity_proposal::where('id',$id)->delete();
        return redirect()->route('usulanKegiatan.')
        ->with('success','Usulan kegiatan berhasil dihapus.');

    }

    

    public function edit($id)
    {
        $activityProposal = Activity_proposal::with('type_proposal')->find($id);
        $typeProposal = Type_proposal::get();

        if(Auth::user()->role->name!='perangkat'){
            $roles = Role::with('user')->where('name','perangkat')->first();
            $user = $roles->user;
        }else{
            $user = User::where('id',Auth::user()->id)->get();
        }

        $dataGet = [
            'activityProposal' => $activityProposal,
            'users' => $user,
            'typeProposals' => $typeProposal
        ];

        if($activityProposal->status=='canceled') return redirect()->route('usulanKegiatan.');
        return view('modul/usulan-kegiatan.edit', $dataGet);
    }

    public function detail($id)
    {
        $activityProposal = Activity_proposal::with('type_proposal')->find($id);

        $dataGet = [
            'activityProposal' => $activityProposal
        ];
        return view('modul/usulan-kegiatan.detail', $dataGet);
        
        # code...
    }
}