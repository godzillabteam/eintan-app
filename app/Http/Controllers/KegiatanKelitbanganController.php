<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Activity;
use App\Models\Activity_master;
use App\Models\Activity_sub;
use App\Models\Activity_sub_master;
use App\Models\Rnd_activity;
use App\Models\Activity_proposal;
use App\Models\Sector;
use Yajra\DataTables\Datatables;


class KegiatanKelitbanganController extends Controller
{
    //
    public function index()
    {
        # code...
        return view('modul/kegiatan-kelitbangan.index');
    }

    public function data()
    {
        $activityProposals = Rnd_activity::with('activity_proposal','sector');
        return Datatables::of($activityProposals)->make();

    }

    public function create($id=null)
    {
        if($id) $activityProposal = Activity_proposal::has('type_proposal')->find($id);
        else $activityProposal = null;
        $activityProposals = Activity_proposal::where('status','draft')->latest()->get();
        $sectors = Sector::latest()->get();

        $dataGet = [
            'activityProposal' => $activityProposal,
            'activityProposals' => $activityProposals,
            'sectors'=>$sectors
        ];

        return view('modul/kegiatan-kelitbangan.create', $dataGet);
    }

    public function edit($id)
    {
        $rndActivity = Rnd_activity::with('activity_proposal','activity_proposal.type_proposal','activity','activity.activity_sub')->findOrFail($id);
        $sectors = Sector::latest()->get();

        $dataGet = [
            'rndActivity' => $rndActivity,
            'sectors'=> $sectors
        ];
        return view('modul/kegiatan-kelitbangan.edit', $dataGet);
        
        # code...
    }

    public function update(Request $request)
    {

        //    print_r($request->all());
        $this->validate(
            $request, 
            [
                'sector_id' => 'required',
                'program' => 'required',
                // 'self_management' => 'required',
                // 'provider' => 'required',
                'activity_master_id' => 'required',
                'activity_sub_master_id' => 'required',
            ],
            [
                'sector_id.required' => 'Masukan Bidang Urusan wajib diisi.',
                'program.required' => 'Masukan Program wajib diisi.',
                // 'self_management.required' => 'Masukan Swakelola wajib diisi.',
                // 'provider.required' => 'Masukan Penyedia (sebutkan nama CV)  wajib diisi.',
                'activity_master_id.required' => 'Masukan Kegiatan wajib diisi.',
                'activity_sub_master_id.required' => 'Masukan Sub Kegiatan wajib diisi.',
            ],
        );

        $rndParams = $request->except('_token');
        // print_r($rndParams);

        $rndParams = $request->except('_token');
        if($rndParams['type_implementation']!='swakelola') $rndParams['self_management'] = '-';
        if($rndParams['type_implementation']!='penyedia') $rndParams['provider'] = '-';
        $rndActivity = Rnd_activity::find($request->id)->update($rndParams);


        $activity = Activity_master::find($request->activity_master_id);
        $activityParams = [
            'rnd_activity_id'=>$request->id,
            'activity_master_id'=>$activity->id,
            'name'=>$activity->name
        ];
        $activityInsert = Activity::where('rnd_activity_id',$request->id)->update($activityParams);

        $activityData = Activity::where('rnd_activity_id',$request->id)->first();


        $activity_sub = Activity_sub_master::find($request->activity_sub_master_id);
        $activitySubsParams = [
            'activity_id'=>$activityData->id,
            'activity_sub_master_id'=>$activity_sub->id,
            'name'=>$activity_sub->name
        ];
        $activitySubInsert = Activity_sub::where('activity_id',$activityData->id,)->update($activitySubsParams);

    
        $rndActivity = Rnd_activity::find($request->id);

       $update = Activity_proposal::where('id',$rndActivity->activity_proposal_id)->update(['status'=>'process']);
        return redirect()->route('kegiatanKelitbangan.')
                        ->with('success','Kegiatan kelitbangan berhasil diperbarui.');

  


    }

    public function detail($id)
    {

        $rndActivity = Rnd_activity::with('activity_proposal','activity','activity.activity_sub')->find($id);
       
        $dataGet = [
            'rndActivity' => $rndActivity
        ];
        return view('modul/kegiatan-kelitbangan.detail', $dataGet);

    }

    public function redirectForm($id)
    {
        $rndActivity = Rnd_activity::where(['activity_proposal_id'=>$id])->first();
        return redirect()->route('kegiatanKelitbangan.detail',$rndActivity->id);
    }

    public function store(Request $request)
    {

        $this->validate(
            $request, 
            [
                'sector_id' => 'required',
                'program' => 'required',
                // 'self_management' => 'required',
                // 'provider' => 'required',
                'activity_master_id' => 'required',
                'activity_sub_master_id' => 'required',
            ],
            [
                'sector_id.required' => 'Masukan Bidang Urusan wajib diisi.',
                'program.required' => 'Masukan Program wajib diisi.',
                // 'self_management.required' => 'Masukan Swakelola wajib diisi.',
                // 'provider.required' => 'Masukan Penyedia (sebutkan nama CV)  wajib diisi.',
                'activity_master_id.required' => 'Masukan Kegiatan wajib diisi.',
                'activity_sub_master_id.required' => 'Masukan Sub Kegiatan wajib diisi.',
            ],
        );

        
        $rndParams = $request->except('_token');
        if($rndParams['type_implementation']!='swakelola') $rndParams['self_management'] = '-';
        if($rndParams['type_implementation']!='penyedia') $rndParams['provider'] = '-';
        $rndActivity = Rnd_activity::create($rndParams);

        $activity = Activity_master::find($rndActivity->activity_master_id);
        $activityParams = [
            'rnd_activity_id'=>$rndActivity->id,
            'activity_master_id'=>$activity->id,
            'name'=>$activity->name
        ];
        $activityInsert = Activity::create($activityParams);

        $activity_sub = Activity_sub_master::find($rndActivity->activity_sub_master_id);
        $activityParams = [
            'activity_id'=>$activityInsert->id,
            'activity_sub_master_id'=>$activity_sub->id,
            'name'=>$activity_sub->name
        ];
        $activityInsert = Activity_sub::create($activityParams);

        Activity_proposal::find($request->activity_proposal_id)->update(['status'=>'process']);
         
        return redirect()->route('kegiatanKelitbangan.')
                        ->with('success','Kegiatan kelitbangan berhasil dibuat.');
    }
}
