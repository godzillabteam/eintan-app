<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity_sub_master;
use App\Models\Activity_master;
use Yajra\DataTables\Datatables;



class ActivitySubMasterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('modul/activity_sub.index');

        //
    }

    public function data()
    {
        //
        $activity_sub_masters = Activity_sub_master::with('activity_master');
        return Datatables::of($activity_sub_masters)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['activityMasters'] = Activity_master::latest()->get();
     
        return view('modul/activity_sub.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Masukan Aktivitas wajib diisi.'
            ],
        );
        Activity_sub_master::create($request->except('_token'));
        return redirect()->route('activity-sub.index')
                        ->with('success','Aktivitas berhasil dibuat.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $activity_sub_master = Activity_sub_master::findOrFail($id);
        $activityMasters = Activity_master::latest()->get();

        $dataGet = [
            'activity_sub_master' => $activity_sub_master,
            'activityMasters'=>$activityMasters
        ];

        return view('modul/activity_sub.edit', $dataGet);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate(
            $request, 
            [
                'name' => 'required'
            ],
            [
                'name.required' => 'Masukan Aktivitas wajib diisi.'
            ],
        );
        Activity_sub_master::find($id)->update($request->except('_token'));
        return redirect()->route('activity-sub.index')
                        ->with('success','Aktivitas berhasil diperbarui.');
                        

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $activity_sub_master = Activity_sub_master::findOrFail($id)->delete();

        return redirect()->route('activity-sub.index')
                        ->with('success','Pertanyaan berhasil dihapus.');
    }
}
