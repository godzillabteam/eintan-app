<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Activity;
use App\Models\Activity_sub;
use App\Models\Rnd_activity;
use App\Models\Activity_proposal;
use App\Models\Study_abstract;
use App\Models\Role;
use App\Models\Rnd_result;
use App\Models\Sector;

class DataPenelitianController extends Controller
{
    //

    public function index()
    {
        # code...
    }

    public function create($type='data-penelitian',$id=null)
    {
        if($id){
            $activityProposal = Activity_proposal::with('type_proposal')->find($id);
            $rndActivity = Rnd_activity::with('activity_proposal','activity_proposal.type_proposal','activity','activity.activity_sub')->where('activity_proposal_id',$id)->first();
        }else{
            $activityProposal = null;
            $rndActivity = null;
        } 

        $activityProposals = Activity_proposal::where('status','draft')->latest()->get();

        $roles = Role::has('user')->where('name','perangkat')->first();
        $sectors = Sector::latest()->get();
        
        $dataGet = [
            'activityProposal' => $activityProposal,
            'activityProposals' => $activityProposals,
            'rndActivity' => $rndActivity,
            'users' => $roles->user,
            'sectors'=>$sectors

        ];
        if($type=="data-penelitian"){
            $studyAbstract = Study_abstract::where('activity_proposal_id',$id)->first();
            if($studyAbstract) return redirect()->route('dataPenelitian.create',['rekomendasi-hasil',$activityProposal->id])
            ->with('success','Usulan kegiatan berhasil dibuat.');
            else return view('modul/data-penelitian.create-abstraksi-kajian', $dataGet);
        }else{
            return view('modul/data-penelitian.create-rekomendasi-hasil', $dataGet);
        }
        # code...
    }

    public function store(Request $request)
    {
        $this->validate(
            $request, 
            [
                'year' => 'required',
                'sector_id' => 'required',
                'abstraction' => 'required',
                'link' => 'required'
            ],
            [
                'year.required' => 'Masukan Tahun Rekomendasi wajib diisi.',
                'sector_id.required' => 'Masukan Pelaksana wajib diisi.',
                'abstraction.required' => 'Masukan Abstraksi wajib diisi.',
                'link.required' => 'Masukan Link E-Journal wajib diisi.'
            ],
        );
        
        // Rnd_activity::where('activity_proposal_id',$request->activity_proposal_id)->update(['status'=>'process']);

        $study_abstract = Study_abstract::where('activity_proposal_id',$request->activity_proposal_id)->first();
        if($study_abstract){
            Study_abstract::where('activity_proposal_id',$request->activity_proposal_id)->update($request->except('_token'));
            return redirect()->route('abstraksi-kajian.index')
                        ->with('success','Abstraksi Kajian berhasil diperbarui.');
        }else{
            Study_abstract::create($request->except('_token'));
            return redirect()->route('dataPenelitian.create',['rekomendasi-hasil',$request->activity_proposal_id])
                        ->with('success','Usulan kegiatan berhasil dibuat.');
        }

        
        // study_abstracts
    }

    public function store_hasil(Request $request)
    {
        $activity_proposal_id = $request->activity_proposal_id;
        foreach($request->recommendation_description as $subId => $recommendation_description){
            $rnd_result = Rnd_result::where('activity_proposal_id',$activity_proposal_id)->where('activity_sub_id',$subId)->first();
            $user_id = $request->user_id[$subId];
            $general_strategy = $request->general_strategy[$subId];
            $data = [
                'activity_proposal_id'=>$activity_proposal_id,
                'activity_sub_id'=>$subId,
                'recommendation_description'=>$recommendation_description,
                'user_id'=>$user_id,
                'general_strategy'=>$general_strategy
            ];
            if($rnd_result){
                Rnd_result::where('activity_proposal_id',$activity_proposal_id)->where('activity_sub_id',$subId)->update($data);
            }else{
                Rnd_result::create($data);
            }

        }

        Rnd_activity::where('activity_proposal_id',$request->activity_proposal_id)->update(['status'=>'process']);

        return redirect()->route('rekomendasi-hasil-kelitbangan.index')
        ->with('success',' Abstraksi Kajian & Rekomendasi Hasil Kelitbangan berhasil dibuat.');
    }
}
