<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rtl extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'rnd_result_id',
        'recommendation_implementation',
        'activity',
        'reason',
        'year',
        'photo'
    ];

    public function rnd_result()
    {
        return $this->belongsTo(Rnd_result::class);
    }




}
