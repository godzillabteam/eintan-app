<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type_proposal extends Model
{
    use HasFactory;

    protected $fillable = [
        'name'
    ];

    public function activity_proposal()
    {
        return $this->hasMany(Activity_proposal::class);

    }

}
