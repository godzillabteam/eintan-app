<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity_sub extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'activity_id',
        'activity_sub_master_id',
        'name',
        
    ];

    public function activity()
    {
        return $this->belongsTo(Activity::class);
    }

    public function rnd_result()
    {
        return $this->hasMany(Rnd_result::class);
    }


}
