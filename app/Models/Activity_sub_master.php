<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Activity_sub_master extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'activity_master_id',
        'name'
    ];

    public function activity_master()
    {
        return $this->belongsTo(Activity_master::class);
    }
}
