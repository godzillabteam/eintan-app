<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Survey extends Model
{
    use HasFactory;
    use SoftDeletes;


    protected $fillable = [
        'title',
        'is_active',
    ];

    public function survey_question()
    {
        return $this->hasMany(Survey_question::class);
    }
}
