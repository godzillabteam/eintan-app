<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Rnd_result extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'activity_proposal_id',
        'activity_sub_id',
        'recommendation_description',
        'user_id',
        'general_strategy'
    ];


    public function activity_proposal()
    {
        return $this->belongsTo(Activity_proposal::class);
    }

    public function activity_sub()
    {
        return $this->belongsTo(Activity_sub::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function rtl()
    {
        return $this->hasOne(Rtl::class);
    }

}
