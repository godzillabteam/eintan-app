<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Sector extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'name'
    ];

    public function rnd_activity()
    {
        return $this->hasMany(Rnd_Activity::class);
    }

    public function study_abstract()
    {
        return $this->hasMany(Study_abstract::class);
    }
}
