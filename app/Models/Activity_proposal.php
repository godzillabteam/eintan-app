<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Activity_proposal extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'type_proposal_id',
        'type_proposal_title',
        'problem',
        'aim',
        'output',
        'outcome',
        'proposed_year',
        'estimated_budget'
    ];

    public function type_proposal()
    {
        return $this->belongsTo(Type_proposal::class);
    }

    public function rnd_activity()
    {
        return $this->hasOne(Rnd_Activity::class);
    }

    public function study_abstract()
    {
        return $this->hasOne(Study_abstract::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);

    }

    
}
