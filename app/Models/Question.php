<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Question extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'question',
        'category',
        'order',
    ];

    public function user_input_question()
    {
        return $this->hasMany(User_input_question::class);
    }

    public function survey_question()
    {
        return $this->hasMany(Survey_question::class);
    }
}
