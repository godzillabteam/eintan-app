<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activity extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'rnd_activity_id',
        'activity_master_id',
        'name',
    ];

    public function activity_sub()
    {
        return $this->hasOne(Activity_sub::class);
    }

    public function rnd_activity()
    {
        return $this->belongsTo(Rnd_activity::class);
    }
}
