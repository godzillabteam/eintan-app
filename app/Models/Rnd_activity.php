<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Rnd_activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'activity_proposal_id',
        'sector_id',
        'program',
        'self_management',
        'provider',
        'activity_year',
        'status',
        'activity_master_id',
        'activity_sub_master_id',
        'type_implementation'
    ];

    public function activity()
    {
        return $this->hasOne(Activity::class);
    }

    public function activity_proposal()
    {
        return $this->belongsTo(Activity_proposal::class);
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }



}
