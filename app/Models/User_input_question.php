<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_input_question extends Model
{
    use HasFactory;

    protected $fillable = [
        'question_id',
        'user_input_id',
        'score',
    ];


    public function user_input()
    {
        return $this->belongsTo(User_input::class);
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }


}
