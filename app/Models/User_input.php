<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class User_input extends Model
{
    use HasFactory;

    protected $fillable = [
        'survey_id',
        'user_id',
        'benefit_recipients',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function user_input_question()
    {
        return $this->hasMany(User_input_question::class);
    }
}
