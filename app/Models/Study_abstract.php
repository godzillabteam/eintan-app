<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Study_abstract extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'activity_proposal_id',
        'year',
        'sector_id',
        'abstraction',
        'link'
    ];

    public function activity_proposal()
    {
        return $this->belongsTo(Activity_proposal::class);
    }

    public function sector()
    {
        return $this->belongsTo(Sector::class);
    }

}
